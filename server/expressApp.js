/**
 * Created by DugganJ on 13/10/2015.
 */
var express = require('express');
var q = require('q');
var path = require('path');
var contentManagerHelper = require('./contentManagerHelper');
var contentManagerService = require('./contentManagerService');

//express setup
var expressApp = express();
expressApp.use(express.static(path.join(__dirname, './untar')));
expressApp.use(express.static( path.join(__dirname, './ebookShell')));

expressApp.get('/listPackageDetails', function(req, res) {
     q.fcall(function () {
        var sections = contentManagerService.listPackageDetails(req.query.folderPath);
        res.send(sections);
    }).catch(function (err) {
         console.log(err);
         res.status(400).send(err);
    });
});

expressApp.get('/removeUntarContent', function(req, res) {
    q.fcall(function(){
        return contentManagerService.removeUntarContent();
    }).then(function () {
        console.log('sucessfully removed untar content');
        res.send();
    }).catch(function (err) {
        console.log(err);
        res.status(400).send(err);
    });
});

expressApp.get('/loadPackageContent', function(req, res) {
    var tarPath = path.join(req.query.folderPath, req.query.fileName);
    q.fcall(function() {
        return contentManagerHelper.untar(tarPath);
    }).then(function () {
            console.log('sucessfully untarred file');
            contentManagerHelper.changeConfigSync();
            res.send();
    }).catch(function (err) {
        console.log(err);
        res.status(400).send("Error loading tar: " + tarPath);
    });
});

var server = expressApp.listen(4567, function () {
    var host = server.address().address;
    var port = server.address().port;
    console.log('app listening at http://%s:%s', host, port);
});