{
  "assessmentTest": {
    "identifier": "id",
    "testPart": {
      "identifier": "player",
      "itemSessionControl": {
        "defaultPageDisplayMode": "doublePage",
        "autoSaveTimeInterval": "0",
        "showCoverPageAtRight": "false",
        "ignoreDisplayModeAtPageLevel": "false"
      },
      "assessmentSection": {
        "identifier": "screen",
        "visible": "true",
        "assessmentItemRef": [
        {
          "identifier": "player",
          "href": "10440"
        }
        ]
      }
    }
  }
}
