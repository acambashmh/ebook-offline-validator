var bTextCopied = false

MG.AnnotationBarOperator = function(){

    this.superClass = MG.BaseOperator.prototype;
    this.authorableShowroom = null;
    this.authorableShowroom_TA = null;
    this.zoomWrapper = null;
    this.toolSelected = true;
    this.imageSelected = false;
}

// extend super class
MG.AnnotationBarOperator.prototype = new MG.BaseOperator();
/**
 * This function defines the functionality of components to be executed.
 * At SCROLL_VIEW_CONTAINER Case, popup for annotation toolbar added. Popup will appear at least word of selection.
 */
MG.AnnotationBarOperator.prototype.attachComponent = function(objComp){

    if (objComp == null) 
        return;
    
    var objThis = this;
    var strType = objComp.element.attr(AppConst.ATTR_TYPE).toString();
    switch (strType) {

        case AppConst.NOTES_ANNOTATION_BTN:
            
            break;
        
        case AppConst.ANNOTATION_CONTAINER:
            
            $(objComp).bind("click", function(e){
                PopupManager.removePopup('annotation-bar');
                window.getSelection().removeAllRanges();
            });
            break;
            
        case AppConst.SCROLL_VIEW_CONTAINER:
            $(objComp).bind(objComp.events.PAGE_RENDER_COMPLETE, function(){
                var container2Ref = $(objComp.element[0]).find('[id="bookContentContainer2"]')[0];
                var arrPageComps = $(objComp.element).find('[data-role="pagecomp"]');
                var objPageRole = null;
                for (var i = 0; i < arrPageComps.length; i++) {
                    objPageRole = $(arrPageComps[i]).data("pagecomp");
                    
                    $(objPageRole).unbind(objPageRole.events.PAGE_LOADED).bind(objPageRole.events.PAGE_LOADED, function(e,pageBreakVal){
	                    $(container2Ref).find('[pagebreakvalue="'+pageBreakVal+'"]').find("img").each(function( index ) {
					
							if(EPubConfig.ImageAnnotation_isAvailable == false || ($(this).attr("alt") == undefined || $(this).attr("alt") == null) || $.trim($(this).attr("alt")) == "")
                                {
                                    return;
                                }
							
							var pagedata = e.currentTarget;
	                    	$(this).unbind('click').bind('click', function(){
                                $('#anno-sep').css('display', 'none');
                                $('.colorContainer').css('display', 'none');
	
								$(".annotation-inner").css({
					                'width': "auto"
					            });
					            
	                            $(popupbar).css({
	                                'width': 'auto',
									'float': 'none'
	                            });
	
	
	                            if (EPubConfig.pageView == 'doublePage' || EPubConfig.pageView == 'singlePage') {
	                                var containerParent = $(this);
	                                if ($(containerParent).attr('type') == "PageContainer") {
	                                    containerParent = $(containerParent);
	                                }
	                                else {
	                                    while ($(containerParent).parent().attr('id') != 'bookContentContainer') {
	                                        if ($(containerParent).parent().attr('type') == "PageContainer") {
	                                            containerParent = $(containerParent).parent();
	                                            break;
	                                        }
	                                        else {
	                                            containerParent = $(containerParent).parent();
	                                            
	                                            //hiding the mark-up tools pop-up because the selection was not done in required designated area;
					                            if(containerParent[0] == null)
					                            {
					                            	$("#annotation-bar").css("display", "none");
					                            	return;
					                            }
	                                        }
	                                    }
	                                }
	                                // console.log($(containerParent).attr('pagebreakvalue'),objComp.options);
	                            }
	                            
	                            objThis.imageSelected = true;
	                            var fnAncestoralParentWithObjectID = function(objTextContainer){
	                                var objAncestorWithObjectID = $(objTextContainer).closest('[objectid]')[0];
	                                return objAncestorWithObjectID;
	                            }
	                            
	                            //finding ancestoral parent with object id of container in which text selection started
	                            var objAncestorWithObjectIDOfImageContainer = fnAncestoralParentWithObjectID(this);
	                            
	                            if (objAncestorWithObjectIDOfImageContainer == null) 
	                                return;
	                            
	                            var objNearestPageContainer = $(objAncestorWithObjectIDOfImageContainer).parents('[type=PageContainer]')[0];
	                            objComp.options.pageIndexForAnnotation = $(objNearestPageContainer).attr('pagebreakvalue');
	                            
	                            var noteInfo = {
	                                ancestoralParentObjectID: $(objAncestorWithObjectIDOfImageContainer).attr("objectid"),
	                                pageNum: $(objNearestPageContainer).attr("pagebreakvalue")
	                            };
	                            
	                            $(objComp)[0].setInfoForSkickyNotes(noteInfo);
	                            
	                            $('#highlighter-anno').css('display', 'none');
	                            $('#copy-anno').css('display', 'none');
	                            $(popupbar).css({
	                                'width': 'auto'
	                            });
	                            
	                            //ensure that notes icon is not disabled
	                            $('#notes-anno').removeClass('ui-disabled')
	                            
	                            $($(objComp))[0].options.rangedata = "";
	                            var popupbar = "#annotation-bar";
	
	                            PopupManager.addPopup($(popupbar), objComp, {
	                                isModal: false,
	                                hasPointer: true,
	                                popupOverlayStyle: "popupMgrMaskStyleSemiTransparent",
	                                offsetScale: GlobalModel.currentScalePercent,
	                                isTapLayoutDisabled: false
	                            });
	                            
	                            var markupToolPosition;
	                            if ($("#bookContentContainer2").scrollTop() < $(this).offset().top + $("#bookContentContainer2").scrollTop()) {
	                                markupToolPosition = $(this).offset().top;
	                            }
	                            else {
	                                markupToolPosition = 55;
	                            }
	                            var gotoNextHeight = 0;
	                            if ($("#goToNextElement").css("display") == "block") 
	                                gotoNextHeight = 50;
	                            if ($(window).height() <= $(this).offset().top + 50 + gotoNextHeight) 
	                                markupToolPosition = $(window).height() - 60 - gotoNextHeight;
	                            $(popupbar).css({
	                                'top': markupToolPosition
	                            });
	                            var popupBarLeft;
	                            popupBarLeft = ((($(this).offset().left) + ($(this).width() * GlobalModel.currentScalePercent))) - 34;
	                            $(popupbar).css({
	                                'left': popupBarLeft,
	                                'width': 'auto'
	                            });
	
	                            var totalPadding = parseInt($(popupbar).css('padding-top')) + parseInt($(popupbar).css('padding-bottom'));
	                            var triangleArraowTopPos = $(".annotation-div").height() + totalPadding - 1;
	                            $(".anno-triangle").css('left', ($(".annotation-div").width() / 2) - 10);
	                            $(".anno-triangle").css('top', triangleArraowTopPos + 'px');
	                            objComp.options.Icontop = markupToolPosition + 15;
		                            
	                      	  });
                        }); 
                    });
                }
            });
            
            $(objComp).bind(objComp.events.SELECT_TEXT_END, function(){
            
                var popupbar = "#annotation-bar";
                var selectionrange = objComp.getRangeContent();
                var objCompRef = $(objComp);
				var matchFound=1;
                if($(PopupManager.objMaskDiv).css('display') == "block") {
                	return;
                }
                var popupOffSet = $(objCompRef)[0].options.nScalePercent;
                var verifyRange = false;
//                if (objThis.imageSelected == false) {
                    verifyRange = objComp.verifySelection(selectionrange);
//                }
                objThis.imageSelected = false;
                if (verifyRange) {
                	//if multiple pages are selected then donot create markup tool
					return;
                    $('#highlighter-anno').addClass('ui-disabled');
                    $('#highlighter-anno').removeClass('ui-enabled');
                    $('.highlighter-anno').addClass('ui-disabled');
                    $('.highlighter-anno').removeClass('ui-enabled');
                }
                else {
                    $('#highlighter-anno').removeClass('ui-disabled');
                    $('#highlighter-anno').addClass('ui-enabled');
                    $('.highlighter-anno').addClass('ui-enabled');
                    $('.highlighter-anno').removeClass('ui-disabled');
                    
                }
                
                
                if (selectionrange) {
                	//Added to fix annotation tool appearance issue for nothing selected.
                	var strSelectedTextCont = String($(selectionrange.cloneContents())[0].textContent);
                	var objReg = /[\s]*/gi;
                	strSelectedTextCont = strSelectedTextCont.replace(objReg, "");
                	if(strSelectedTextCont == "")
                	{
                		return;
                	}
                	
                    if ($(selectionrange.cloneContents())[0].textContent != " ") {
                        if (!selectionrange.collapsed) {

                               var txt = document.createElement('div');
                               txt.appendChild(selectionrange.cloneContents());

					              for (var annVerify = 0; annVerify < GlobalModel.annotations.length; annVerify++)
									{


										if(GlobalModel.annotations[annVerify].type=='highlight')
										{

										if(($(txt).find(GlobalModel.annotations[annVerify].selectionID).length != 0) || ($(objComp)[0].options.rangeObject) == GlobalModel.annotations[annVerify].rangeObject)
										{

										matchFound=0;
										var triggerID = GlobalModel.annotations[annVerify].selectionID.replace('.selection', '')
										$(document).trigger('highlightEdit', triggerID);
										break;
										}
										}
										else if(GlobalModel.annotations[annVerify].type=='stickynote')
										{
										if(($(txt).find('.noteselection_'+GlobalModel.annotations[annVerify].id).length != 0) || ($(objComp)[0].options.rangeObject) == GlobalModel.annotations[annVerify].rangeObject)
										{
										matchFound=0;
										$( '.noteselection_'+GlobalModel.annotations[annVerify].id ).trigger( "click" );
										//$(document).trigger('stickynoteEdit', GlobalModel.annotations[annVerify].id);										
										break;
										}
										}
										
																			
									}

					if(matchFound==1)
					{
                            PopupManager.addPopup($(popupbar), objComp, {
                                isModal: false,
                                //hasPointer: true,
                                popupOverlayStyle: "popupMgrMaskStyleSemiTransparent",
                                //offsetScale: GlobalModel.currentScalePercent,
                                isTapLayoutDisabled: false
                            
                            });
                            if (navigator.userAgent.match(/(android)/i))
                  				{
                  					if (EPubConfig.pageView == 'scroll' || EPubConfig.pageView == 'SCROLL')
                  					{
                  						$(popupbar).css('visibility','hidden');
	                  					setTimeout(function(){
	                  						objComp.setAnnotationPopup(popupbar);
										},500);	
                  					}
                  					else
                  					{
                  						objComp.setAnnotationPopup(popupbar);
                  					}	
                  				}
                  				else
                  				{
                  					objComp.setAnnotationPopup(popupbar);
                  				}
                            //objComp.setAnnotationPopup(popupbar);
							var sel = window.getSelection();
								
								if(ismobile){
	                    		}
	                    		else{
	                    			try{
	                    				sel.removeAllRanges();
	                    				sel.addRange(selectionrange);
	                    			}catch(e){
	                    				//console.log("error occured",e);
	                    				PopupManager.removePopup('annotation-bar');
	                    				setTimeout(function(){
	                    					sel.removeAllRanges();
	                    					sel.addRange(selectionrange);
	                    					PopupManager.addPopup($(popupbar), objComp, {
				                            	isModal: false,
				                            	//hasPointer: true,
				                            	popupOverlayStyle: "popupMgrMaskStyleSemiTransparent",
				                            	//offsetScale: GlobalModel.currentScalePercent,
				                            	isTapLayoutDisabled: false});
				                            	objComp.setAnnotationPopup(popupbar);
					                    	},500);
	                    			}
	                    			
								}
					}
                            
                        }
                    }
                }
            });
            
            break;
            
        case AppConst.STICKYNOTE_SECTION_PANEL:
            /* This event is triggered from stickynotepanelcomp.js.
         stickyNotesData is the text that will be displayed in the panel.*/
            //$(objComp.element).find('[id=annotationMyNotesTab]').removeClass('leftPanelTab').addClass('leftPanelTabActive');
            $(objComp).bind('addDataToStickyNoteSectionPanel', function(e, stickyNotesData){
                $($(objComp.element).find('[id=addedStickyNotes]')[0]).html(stickyNotesData);
                var stickyNoteTitleHeight;
                var prevDetailedAnnotationRef = null;
                
                $('.annotationData').unbind('click').bind('click', function(){
                	if(GlobalModel.hasDPError("notes"))
        				return;
                    switch ($(this).parent().attr('type')) {
                        case "highlightData":
                            var currentListId = ($(this).parent().attr('id').replace('highlightData_', ''));
                            $(objComp.element).trigger('editHighlightData', currentListId);
                            break;
                        case "stickynoteData":
                            $("#stickyNoteLauncher").trigger('click');
                            PopupManager.removePopup();
                            var currentListId = ($(this).parent().attr('id').replace('stickynoteData_', ''));
                            $(objComp.element).trigger('editStickynoteData', currentListId);
                            break;
                        default:
                    }
                });
                
                // Show hide button functionality
                
                
                $('.expandCollapseNoteList').unbind('click').bind('click', function(){
                    var objExpandCollapseList = this;
                    
                    //var hideOtherDetails = $(this).find('[class=expandCollapseNoteList]');
                    //console.log(hideOtherDetails);
                    //$(hideOtherDetails).trigger('click');
                    if ($(this).hasClass('showDetail')) {
                        if (prevDetailedAnnotationRef != null) {
                            $(prevDetailedAnnotationRef).trigger('click');
                            prevDetailedAnnotationRef = null;
                        }
                        prevDetailedAnnotationRef = $(this)[0];
                        $(this).html(GlobalModel.localizationData["STICKY_NOTE_PANEL_HIDE_DETAILS_BUTTON"]);
                        $(this).removeClass('showDetail');
                        $($(this).parent().find("#stickynoteDataTitle")).hide();
                        $($(this).parent().find("#stickynoteDataTitle_detail")).show();
                        stickyNoteTitleHeight = $(this).parent().find('[class=dataTitle]').height();
                        $(this).parent().find('[class=dataTitle]').css({
                            'height': 'auto',
                            'overflow': 'auto',
                            'white-space': 'normal'
                        });
                        $($(this).parent().find("#editDeleteAnnotation")).append('<div class="editDeleteAnnotaionPanelContainer"><div class="annotaionEditDiv"></div><div class="annotaionDeleteDiv"></div></div>');
                    }
                    else {
                        prevDetailedAnnotationRef = null;
                        $($(this).parent().find("#stickynoteDataTitle")).show();
                        $($(this).parent().find("#stickynoteDataTitle_detail")).hide();
                        $(this).parent().find('[class=editDeleteAnnotaionPanelContainer]').remove()
                        $(this).html(GlobalModel.localizationData["STICKY_NOTE_PANEL_SHOW_DETAILS_BUTTON"]);
                        $(this).addClass('showDetail');
                        $(this).parent().find('[class=dataTitle]').css({
                            'height': stickyNoteTitleHeight,
                            'overflow': 'hidden',
                            'white-space': 'nowrap'
                        });
                    }
                    
                    objThis.doFunctionCall(AppConst.STICKYNOTE_SECTION_PANEL, 'updateViewSize');
                    
                    
                    $(this).parent().find('[class=annotaionEditDiv]').unbind('click').bind('click', function(){
                    	if(GlobalModel.hasDPError("notes"))
            				return;
                        $("#stickyNoteLauncher").trigger('click');
                        PopupManager.removePopup();
                        switch ($(this).parent().parent().parent().attr('type')) {
                            case "highlightData":
                                var currentListId = ($(this).parent().parent().parent().attr('id').replace('highlightData_', ''));
                                $(objComp.element).trigger('editHighlightData', currentListId);
                                break;
                            case "stickynoteData":
                                var currentListId = ($(this).parent().parent().parent().attr('id').replace('stickynoteData_', ''));
                                $(objComp.element).trigger('editStickynoteData', currentListId);
                                break;
                            default:
                        }
                    //var currentListId = ($(this).parent().parent().attr('id').replace('stickynoteData_', ''));
                    
                    });
                    
                    $(this).parent().find('[class=annotaionDeleteDiv]').unbind('click').bind('click', function(){
                    	if(GlobalModel.hasDPError("notes"))
            				return;
                        PopupManager.removePopup();
                        
                        switch ($(this).parent().parent().parent().attr('type')) {
                            case "highlightData":
                                // console.log("highlightData");
                                var currentListId = ($(this).parent().parent().parent().attr('id').replace('highlightData_', ''));
                                $(objComp.element).trigger('deleteHighlightData', currentListId);
                                break;
                            case "stickynoteData":
                                var currentListId = ($(this).parent().parent().parent().attr('id').replace('stickynoteData_', ''));
                                $(objComp.element).trigger('deleteStickynoteData', currentListId);
                                break;
                            default:
                        }
                    });
                    
                });
            });
           
            
            $(objComp).bind('addDataToQuestionAnswerSectionPanel', function(e){
                objThis.doFunctionCall(AppConst.QUES_ANS_PANEL, 'createQuestionPanelList',objComp);
            });
            break;
        case AppConst.QUES_ANS_PANEL: 
            break;
            
        default:
            ////console.error('### ERROR: ['+ strType +'] -- Either "type" is not defined or Invalid operator is assigned');
            return false;
            break;
    }
    
    return MG.BaseOperator.prototype.attachComponent.call(this, objComp);
}


MG.AnnotationBarOperator.prototype.getTextToCopy = function(){
    bTextCopied = true;
    
    if (window.getSelection) {
        if (window.getSelection().empty) { // Chrome
            window.getSelection().empty();
        }
        else 
            if (window.getSelection().removeAllRanges) { // Firefox
                window.getSelection().removeAllRanges();
            }
    }
    else 
        if (document.selection) { // IE?
            document.selection.empty();
        }
    
    PopupManager.removePopup();
    
    return this.doFunctionCall(AppConst.SCROLL_VIEW_CONTAINER, "getTextToCopy");
}
