( function( $, undefined ) {
$.widget( "magic.annotationbar", $.magic.magicwidget, 
	{
		options: 
		{
			
		},
		member://member variables
		{
			
		},
		_create: function()
		{
		
			// it seems that jqmData('options)persist the data-options values available in _create() only so storing and merging it here first then in _init 
			//setting it to dom elements options object
			$.extend(this.options, this.element.jqmData('options'));

			
		},
		_init:function()
		{
			this.element.css("display", "none");
			var objRef = this;
			$(document).bind(AppConst.CLOSE_ANNOTATION, function(e){
				objRef.element.css("display", "none");
			})
		},
		showHidePanel:function()
		{
			if(this.element.css("display") == "none")
			{
				this.element.css("display", "block");
			}
			else
			{
				$(document).trigger(AppConst.CLOSE_ANNOTATION);
			}
		},
		
		destroy: function()
		{
			
			//$.Widget.prototype.destroy.call(this);
			
		}
	})
})( jQuery );
	
	