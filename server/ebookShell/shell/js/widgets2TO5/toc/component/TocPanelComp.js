/**
 * @author sumanta.mishra
 */
var tocScroll = null;
var objToc = $.widget( "magic.tocPanelComp", $.magic.tabViewComp, 
{
	events:
	{
		TOC_RENDER_COMPLETE: "tocrendercomplete",
		TOC_LINK_ITEM_CLICK: "toclinkitemclick"
	},
	options: 
	{
		src: null,
		strList:"",
		level:0,
		curriculumXml :"",
		glossaryData: null,
		tocData: null
	},
	member:
	{
		pageLinkList: null,
		nClickedLinkIndex: -1,
		objAutomaticMenu: null,
		currentSelectedTOCNode: null,
		navItemsList: []
	},
	_create: function()
	{
		$.extend(this.options, this.element.jqmData('options'));
		
		//Setting localized place holder text in Go to page text field
		var tocTopbarRef = ($(this.element[0]).find('[id=tocTopbar]')[0]);
		var tocGotoInputBoxRef = $($(tocTopbarRef)[0]).find('[id=tocGotoInputBox]')[0];
		$(tocGotoInputBoxRef).attr("placeholder", GlobalModel.localizationData["TOC_GOTO_PAGE_PLACEHOLDER_TEXT"]);
		/* Start : Modlog_23052013 Aastha for MREAD-310 OBB || configurable goto page functionality */
		var tocGotoButtonRef = $($(tocTopbarRef)[0]).find('[id=tocGotoBtn]')[0];
		if(!EPubConfig.Goto_isAvailable) {
			$(tocGotoInputBoxRef).css('display','none');
			$(tocGotoButtonRef).css('display','none');
			
		}
		/* End : Modlog_23052013 Aastha for MREAD-310 OBB || configurable goto page functionality */
	},
	_init: function()
	{
	    var self = this;
		$.magic.tabViewComp.prototype._init.call(this);	
		
		var objPathManager = getPathManager();
		var strSrcPath = EPubConfig.tocFileName;
		
        var strUri = objPathManager.getTOCPath();
		self.loadTocContent(strUri);
		
		$(document).bind("updateIScroll",function(){
        	if( $( '#tocMainPanel' ).css('display') == 'block' )
        	{
        		if($(HotkeyManager.currentActiveElement).find("[levelid]").length || $(HotkeyManager.currentActiveElement).attr('levelid'))
        		{
        			var Index = $(".focusglow").attr("id");
					if((Number($("#"+Index).offset().top - 157) + parseInt($("#"+Index).css("height"))) > parseInt($("#curTree").height()) || Number($("#"+Index).offset().top - 157)<0)
						tocScroll.scrollToElement("#"+Index,0);
        		}
        	}
        });
		
		$(window).resize(function(){
			self.updateViewSize();
		});
		$(objThis.element).unbind("tabDisplayReset").bind("tabDisplayReset", function() {
			$("#curTree").find('[hotkeyindex]').attr("hotkeyindex", "-1");
			$("#tocGotoInputBox").attr("hotkeyindex", "-1");
			$("#tocGotoBtn").attr("hotkeyindex", "-1");
		});
		
		$(objThis.element).unbind("tabDisplayChanged panelOpened").bind("tabDisplayChanged panelOpened", function() {
			$("#curTree").find('[hotkeyindex]').attr("hotkeyindex", "-1");
			$("#tocGotoInputBox").attr("hotkeyindex", "-1");
			$("#tocGotoBtn").attr("hotkeyindex", "-1");
			objThis.member.objAutomaticMenu.updateHotKeyIndex();
			HotkeyManager.tocPanelHandler();
		});
	},
	
	updateViewSize: function(Index){
		var objThis = this;
		if(tocScroll)
		{
			setTimeout(function(){
				$("#curTree").height(window.height - 101);
				tocScroll.refresh();
				if(Index)
				{
					currentNavId = Index;
					$(document).trigger("updateHotkeyIndex")
					/*if((Number($("#"+Index).offset().top - 157) + parseInt($("#"+Index).css("height"))) > parseInt($("#curTree").height()) || Number($("#"+Index).offset().top - 157)<0)
						tocScroll.scrollToElement("#"+Index,0);*/
				}
				//adjust the width of the tree if scrollbar is visible
				if(ismobile == null)
				{
					//Handling the click event on scroll bar of iscroll calling the function of MyScroll.js
					iScrollClickHandler($(objThis.element).find("[class=myScrollbarV]"),tocScroll,"curTree");
					
				}
			},400);
		}
	},
	
	loadTocContent: function(strURL)
	{
        var objThis = this;
        var data = GlobalModel.tocXHTMLData;
        objThis.options.tocData = data;
        setTimeout(function(){objThis.onTocDataLoaded(data);},0)
	},
	
	onTocDataLoaded: function(data)
	{
		var objThis = this;
		var strXML;
		var hiddenStrXML;
		
		if(getInternetExplorerVersion() == -1 && window.ActiveXObject)
		{
			var xmlObject = new ActiveXObject("Microsoft.XMLDOM");
			xmlObject.async = false;
			strXML = data;
		}
		else
		{
			strXML = new XMLSerializer().serializeToString(data);
		}
		
		//for i tag
                    objReg = /\<i\>/gi;
                    strXML = strXML.replace(objReg, "&lt;i&gt;");
                    objReg = /\<\/i\>/gi;
                    strXML = strXML.replace(objReg, "&lt;/i&gt;");
                    
                    //for u tag
                    objReg = /\<u\>/gi;
                    strXML = strXML.replace(objReg, "&lt;u&gt;");
                    objReg = /\<\/u\>/gi;
                    strXML = strXML.replace(objReg, "&lt;/u&gt;");
                    
                    //for b tag
                    objReg = /\<b\>/gi;
                    strXML = strXML.replace(objReg, "&lt;b&gt;");
                    objReg = /\<\/b\>/gi;
                    strXML = strXML.replace(objReg, "&lt;/b&gt;");
                    
                    //for em tag
                    objReg = /\<em\>/gi;
                    strXML = strXML.replace(objReg, "&lt;em&gt;");
                    objReg = /\<\/em\>/gi;
                    strXML = strXML.replace(objReg, "&lt;/em&gt;");
		
		strXML = strXML.split("</nav>")[0] + "</nav>";
		
		var temp = strXML.split(/<nav[^>]*?>/);
		
		//ensuring that TOC has nav tag
		if(temp[1] == null)
			throw new Error("TOC Malformed: <nav>..</nav> tag missing")
			
		strXML = "<nav>" + temp[1];
		hiddenStrXML = strXML;
		
		var objReg = /\<ol\>/gi;
		hiddenStrXML = hiddenStrXML.replace(objReg, "");
		objReg = /\<ol hidden=""\>/gi;
		hiddenStrXML = hiddenStrXML.replace(objReg, "");
		objReg = /\<\/ol\>/gi;
		hiddenStrXML = hiddenStrXML.replace(objReg, "");
		var objTree = $.xml2json(hiddenStrXML);
		var objTocComData = objTree;
		var xmlDoc = $.parseXML( strXML )
		
		var iNumChildren = $(xmlDoc).find("ol").length;
		
		if(iNumChildren == 0)
			throw new Error("TOC: No Pages found")
		
		var iNumHiddenOLsRemoved = 0;
		var mdsDataArr = [];
		var strMDSAttr = "";
		if( EPubConfig.legacyTOC == false )
		{	
			$(xmlDoc).find("li").each(function(){
				var obj = new Object();
		        obj.pagename = $($(this).children()[0]).attr("href");
		        if($(this).attr('data-mds'))
		        {
		        	strMDSAttr = $(this).attr('data-mds');
		        	var objAttrReg = /[\s]*/gi;
		        	strMDSAttr = strMDSAttr.replace(objAttrReg, "")
		        	obj.data = strMDSAttr.split(",");
		        	obj.mds_isAvailable = true;
		        	obj.mdsAttr = strMDSAttr;
		        }
		       
		        mdsDataArr.push(obj);
		        
		    });
		    
		    var mdsArrLength = mdsDataArr.length;
		    var pageLength = GlobalModel.ePubGlobalSettings.Page.length;
		    var objMDSData = {};
		    for(i = 0; i< mdsArrLength; i++)
			{
				if(mdsDataArr[i].data)
				{
					var mdsDataLength = mdsDataArr[i].data.length;
						for(k = 0; k< pageLength; k++)
						{
							if(GlobalModel.ePubGlobalSettings.Page[k].PageName == mdsDataArr[i].pagename && mdsDataArr[i].mds_isAvailable)
							{
								objMDSData = {};
								for(j = 0; j< mdsDataLength; j++)
								{
									objMDSData[mdsDataArr[i].data[j].split("-")[0]] = mdsDataArr[i].data[j].split("-")[1];
									GlobalModel.ePubGlobalSettings.Page[k].mds_isAvailable = true;
								}
								GlobalModel.ePubGlobalSettings.Page[k].mdsData = objMDSData;
								GlobalModel.ePubGlobalSettings.Page[k].mdsAttr = mdsDataArr[i].mdsAttr;
								break;
							}
						}
					
				}
			}
		}
		$(xmlDoc).find("ol").each(function(){
	        if($(this).children().length == 0)
	        {
	        	throw new Error("TOC Malformed: Empty OL encountered.")
	        }
	        
	        if($(this).attr('hidden') == "")
	        {
	        	iNumHiddenOLsRemoved++;
	        	$(this).remove();
	        }
	    });
	    
	    var bAllPagesInBookAreHidden = false;
	    if(iNumHiddenOLsRemoved == iNumChildren)
	    {
	    	bAllPagesInBookAreHidden = true;
	    }
	    
	    if(getInternetExplorerVersion() == -1 && window.ActiveXObject)
		{
			strXML = xmlDoc.xml;
		}
		else
		{
			strXML = new XMLSerializer().serializeToString(xmlDoc);
		}
		
		var objReg = /\<ol\>/gi;
		strXML = strXML.replace(objReg, "");
		objReg = /\<\/ol\>/gi;
		strXML = strXML.replace(objReg, "");
		objTree = $.xml2json(strXML);
		var objTocData = objTree;
		
		objThis.createTree(objTocComData,objTocData, bAllPagesInBookAreHidden);
		
		tocScroll = new iScroll('curTree', { scrollbarClass: 'myScrollbar' });
		
		this.updateViewSize();
		
	},
	
	createTree: function(objTocComData,objTocData, bAllPagesInBookAreHidden)
	{
		var objThis = this;
		var i = 0;
		var objAutomaticMenu = new TreeUIHelper();
        objAutomaticMenu.addCustomEvent(objAutomaticMenu.events.UPDATE_TREE_VIEW_SIZE, objThis.updateViewSize, objThis);
		
		objAutomaticMenu.init(objTocComData,objTocData, bAllPagesInBookAreHidden);
		this.member.objAutomaticMenu = objAutomaticMenu;
		this.member.pageLinkList = objAutomaticMenu.linkList;
		
		var cnt = 0;
		var nDepth = -1;
		for(i = 0; i< 7; i++)
		{
			//console.log('EPubConfig["L" + x] : '+EPubConfig["L" + x] );
			if(EPubConfig["L" + i] == true)
			{
				cnt++;
				if(EPubConfig["L" + i+"_label"])
				{
					if(EPubConfig.scrollNavType == EPubConfig["L" + i+"_label"])
					{
						nDepth = cnt;
						break;
					}
				}
			}
		}
		
		if(EPubConfig.scrollNavType == 'Page') {
			var arrNavElements = $("ul#tree").find('li');
			for(i = 0; i < this.member.pageLinkList.length; i++)
			{
				objThis.member.navItemsList.push(i);
			}
		} else {
			if(nDepth != -1)
			{
				var arrNavElements = $.find('[levelId='+nDepth+']');
				for(i = 0; i < arrNavElements.length; i++)
				{
					objThis.member.navItemsList.push(eval($(arrNavElements[i]).attr('linkIndex')));
				}
			}	
		}
		
		//overriding the Total Page Count to ensure that correct page count (as per TOC) is used.
		EPubConfig.totalPages = this.member.pageLinkList.length;
		
		GlobalModel.navPageArr = objThis.member.navItemsList;
		/*if(EPubConfig.L0 == "true") {
			EPubConfig.L0_value = "depth"+cnt;
			cnt++
		}
		if(EPubConfig.L1 == "true") {
			EPubConfig.L1_value = "depth"+cnt;
			cnt++
		}
		if(EPubConfig.L2 == "true") {
			EPubConfig.L2_value = "depth"+cnt;
			cnt++
		}
		if(EPubConfig.L3 == "true") {
			EPubConfig.L3_value = "depth"+cnt;
			cnt++
		}
		if(EPubConfig.L4 == "true") {
			EPubConfig.L4_value = "depth"+cnt;
			cnt++
		}
		if(EPubConfig.L5 == "true") {
			EPubConfig.L5_value = "depth"+cnt;
			cnt++
		}
		if(EPubConfig.L6 == "true") {
			EPubConfig.L6_value = "depth"+cnt;
			cnt++
		}
		
		console.log("=========",EPubConfig.L0_value,"=========",EPubConfig.L1_value,"=========",EPubConfig.L2_value,"=========",EPubConfig.L3_value,"=========",EPubConfig.L4_value,"=========",EPubConfig.L5_value,"=========",EPubConfig.L6_value);*/
		//console.log(this.member.pageLinkList);
		$(this).trigger(this.events.TOC_RENDER_COMPLETE, this.data);
		
		var arrElements = $(this.element).find('[isactivelink=true]');
		
		var clickHandler = function( evt ){
			objThis.member.nClickedLinkIndex = parseInt($(target).attr("linkindex"));
			$(objThis).trigger(objThis.events.TOC_LINK_ITEM_CLICK, objThis.data);
		}
		
		var target = null;		
		for(var i = 0; i < arrElements.length; i++)
		{
			if( isWinRT )
			{
				var msGesture = new MSGesture();
				msGesture.target = $(arrElements[i])[0];
				$(arrElements[i])[0].addEventListener("MSGestureTap", clickHandler);
				$(arrElements[i])[0].addEventListener(WinRTPointerEventType, function(evt){
					if (evt.type == "pointerdown" || (evt.type == "MSPointerDown"))
				    {
				        msGesture.addPointer(evt.pointerId);
				    }
					target = this;
				});
			}
			else
			{
				$(arrElements[i]).bind("click", function( event1 ){
					target = this;
					clickHandler( event1 );
				});
			}
			
		}
	},
	
	getPageLinkList: function()
	{
		return this.member.pageLinkList;
	},
	
	getClickedLinkIndex: function()
	{
		if(EPubConfig.pageView == 'doublePage'){
            this.member.nClickedLinkIndex = this.member.nClickedLinkIndex + 1;
        }
        return this.member.nClickedLinkIndex;
	},
	
	getPopupPanel: function()
	{
		//return $(this.element).find('[id="glossaryTextPanel"]');
	},
	
	onPanelOpen: function()
	{
		//return $(this.element).find('[id="glossaryTextPanel"]');
		this.updateViewSize(this.member.currentSelectedTOCNode);
	},
	onPanelClose: function()
	{
        var self = this;
		//resetting the value of Go to page input field
		var tocTopbarRef = ($(this.element[0]).find('[id=tocTopbar]')[0]);
		var tocGotoInputBoxRef = $($(tocTopbarRef)[0]).find('[id=tocGotoInputBox]')[0];
		
		$(tocGotoInputBoxRef).val("");
        // This block will reset the stickynote panel when the panel is closed.
        if(this.member.currentSelectedTOCNode == null){
            var iPageIndex = parseInt($($('[type=PageContainer]')[GlobalModel.currentPageIndex]).attr('pagesequence'));
            this.member.currentSelectedTOCNode = "nav"+ (iPageIndex);
        }
        this.member.objAutomaticMenu.collapseAll();
        this.member.objAutomaticMenu.setTOCView(this.member.currentSelectedTOCNode);
	},
	destroy: function()
	{
		$.widget.prototype.destroy.call( this );		
	},
	
	/**
	  * This function is called when the user scrolls the page or through deep linking to set the TOC to the desired level
	  * @param 	(int) page index
	  * @return void
	  */
	openTocForPage: function(nIndex)
          {
                    var objThis = this;
                    var currCnt = (nIndex+1);
                    var pageName = $($('[type=PageContainer]')[nIndex]).attr('pagename');
                    try {
                              if($(objThis.options.tocData).find("a[href='"+pageName+".xhtml']:last").parent().parent().attr("hidden") == undefined) {
                                        var navId = $(objThis.options.tocData).find("a[href='"+pageName+".xhtml']:last").parent().attr('id');
                              } else {
                                        var navId = objThis.getNavIdFromSibling($(objThis.options.tocData).find("a[href='"+pageName+".xhtml']"))
                              }         
                              if(typeof navId == 'undefined') {
                                        if($(objThis.options.tocData).find("a[href='"+pageName+".xhtml']:last").parent().parent().attr("hidden") == undefined) {
                                                  var navId = $(objThis.options.tocData).find("a[href='"+pageName+".xhtml']:last").parent().attr('id');
                                        } else {
                                                  var navId = objThis.getNavIdFromSibling($(objThis.options.tocData).find("a[href='"+pageName+".xhtml']"))
                                        }
                                        if(typeof navId == 'undefined') {
                                                  if($(objThis.options.tocData).find("a[href='"+pageName+".xhtml']:last").parent().parent().attr("hidden") == undefined) {
                                                            var navId = $(objThis.options.tocData).find("a[href='"+pageName+".xhtml']:last").parent().attr('id');
                                                  } else {
                                                            var navId = objThis.getNavIdFromSibling($(objThis.options.tocData).find("a[href='"+pageName+".xhtml']"))
                                                  }
                                                  if(typeof navId == 'undefined') {
                                                            if($(objThis.options.tocData).find("a[href='"+pageName+".xhtml']:last").parent().parent().attr("hidden") == undefined) {
                                                                      var navId = $(objThis.options.tocData).find("a[href='"+pageName+".xhtml']:last").parent().attr('id');
                                                            } else {
                                                                      var navId = objThis.getNavIdFromSibling($(objThis.options.tocData).find("a[href='"+pageName+".xhtml']"))
                                                            }
                                                  }
                                        }
                              }
                              
                    } catch(e) {
                              var navId = "nav" + (nIndex+1);
                    }
                    currentNavId = navId;
                    objThis.member.currentSelectedTOCNode = navId;
                    objThis.member.objAutomaticMenu.collapseAll();
                    objThis.member.objAutomaticMenu.setTOCView(this.member.currentSelectedTOCNode);
          },
          
          getNavIdFromSibling: function(elem) {
                    var elemLength = $(elem).length;
                    if(elemLength > 0) {
                              for(var i=elemLength; i > 0; i--) {
                                        if($($(elem)[i-1]).parent().parent().attr("hidden") == undefined) {
                                                  return $($(elem)[i-1]).parent().attr('id');
                                        }
                              }
                    }
                    return this.getNavIdFromParent(elem);
                    
          },
          
          getNavIdFromParent: function(elem) {
                    var elem = $(elem).parent().parent().parent().children("a")[0];
                    var elemLength = $(elem).length;
                    if(elemLength > 0) {
                              for(var i=elemLength; i > 0; i--) {
                                        if($($(elem)[i-1]).parent().parent().attr("hidden") == undefined) {
                                                  return $($(elem)[i-1]).parent().attr('id');
                                        }
                              }
                    }
                    
          }
});

