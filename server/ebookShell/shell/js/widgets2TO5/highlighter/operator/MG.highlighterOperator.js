/**
 * @author Harsh Chawla
 * This class handles the functionality of highlighter
 */
MG.highlighterOperator = function(){
    this.highlightcontainerRef = "";
    this.highlightRef = "";
    this.scrollviewRef = "";
    this.rangedata = "";
    this.currentselectioncolor = "selectioncolor3";
    this.currentselection = "";
    this.newHighlight = true;
    this.currentAnnotation;
    this.currentsaved = "";
    this.deletefromPanel = true;
    this.currentIDtoDelete = "";
    this.isOpenHighlight = false;
}

MG.highlighterOperator.prototype = new MG.BaseOperator();

MG.highlighterOperator.prototype.attachComponent = function(objComp){

    if (objComp == null) 
        return;
    
    var objThis = this, strType = objComp.element.attr(AppConst.ATTR_TYPE).toString();
    var isDeleteNote = false;
    
    switch (strType) {
    
    
        case AppConst.HIGHLIGHTER_COMP:
            
            break;
            
        // This will define all the functionality of highlight popup.    
        case AppConst.HIGHLIGHT_CONTAINER:
            //ensuring that the default selection color comes from config.txt;
            // if the default color does not exists in list of highlight colors, use the first color in the list
            
            $(document).bind("highlightEdit", function(e, editID){
                PopupManager.removePopup();
                $($(objThis.deleteConfirmationPanel).find('[id=dltConfirmationTxt]')[0]).html(GlobalModel.localizationData["HIGHLIGHT_DELETE_CONFIRMATION_TEXT"]);
                objThis.openDeleteConfirmationPanel($(".selection" + editID));
                objThis.deletefromPanel = false;
            });
            
            
            var iSelectionColor = 1;
            
            for (var i = 0; i < EPubConfig.Highlight_colors.length; i++) {
                if (EPubConfig.Highlight_colors[i] == EPubConfig.Highlights_default_color) {
                    iSelectionColor = i + 1;
                    break;
                }
            }
            
            objThis.currentselectioncolor = "selectioncolor" + iSelectionColor;
            
            objThis.highlightcontainerRef = objComp;
            
            var stickynoteClsBtnRef = $(objComp.element).find('[type=closeButton]')[0];
            var saveBtnRef = $(objComp.element).find('[type=saveBtn]')[0];
            var deleteBtnRef = $(objComp.element).find('[type=deleteBtn]')[0];
            
            
            
            // This will define all the functionality of close button of highlight popup.    
            $(stickynoteClsBtnRef).unbind('click').bind('click', function(){
                PopupManager.removePopup();
                if (objThis.newHighlight == true) {
                    objThis.currentselection = ".selection" + GlobalModel.totalStickyNotes;
                    $(objThis.currentselection).removeClass(objThis.currentselectioncolor);
                    $(objThis.currentselection).removeClass("selectioncolor0");
                    $(objThis.currentselection).removeClass("selection" + GlobalModel.totalStickyNotes);
                }
                else {
                    $(objThis.currentselection).removeClass(objThis.currentselectioncolor);
                    $(objThis.currentselection).removeClass("selectioncolor0");
                    $(objThis.currentselection).addClass(objThis.currentsaved);
                    objThis.newHighlight = true;
                }
                if (ismobile)
                	objThis.scrollviewRef.options.rangedata = "";
                else	
                	$(objThis.scrollviewRef).trigger("select_text_end"); // this event is handled in annotationbaroperator.
                
            });
            
            
            // This will define all the functionality of color blocks in highlight popup.    
            var colorBtnRef = $(objComp.element).find('[type=color]');
            
            if (ismobile) {
                var eventType = 'vclick';
            }
            else {
                var eventType = 'click';
            }
            
            var clickHandler = function(){
            	if(GlobalModel.hasDPError("highlight"))
            		return;
                var matchFound = objThis.findHighlightMatch();
                
                if (matchFound != 0) {
                	if (navigator.userAgent.match(/(android)/i)) 
		            {
		            	var startRange = document.createRange();
			            var rangeData = objThis.scrollviewRef.options.rangedata;
			            startRange.setStart(rangeData.startContainer, rangeData.startOffset);
			            var $start = $("<span/>");
			            startRange.insertNode($start[0]);
			            $start.remove();
		            }
                    objThis.newHighlight = true;

                    if (objThis.newHighlight == true) {
                        rangy.init();
                        var cssClassApplier = rangy.createCssClassApplier("selection" + GlobalModel.totalStickyNotes);
                        var cssClassAppliercolor = rangy.createCssClassApplier(objThis.currentselectioncolor);
                        window.getSelection().removeAllRanges();
                        window.getSelection().addRange(objThis.scrollviewRef.options.rangedata);
                        cssClassApplier.toggleSelection();
                        cssClassAppliercolor.toggleSelection();
                        
                        objThis.currentselection = ".selection" + GlobalModel.totalStickyNotes;
                    }
                    else {
                        $(objThis.currentselection).removeClass(objThis.currentselectioncolor + "_saved");
                        
                    }
                    
                    
                    $(objThis.currentselection).removeClass("selectioncolor0");
                    $(objThis.currentselection).removeClass(objThis.currentselectioncolor);
                    
                    $(objThis.currentselection).addClass("selection" + this.id);
                    objThis.currentselectioncolor = "selection" + this.id;
                    
                    objThis.saveHighlight();
                }
                else {
                    if (window.getSelection) {
                        if (window.getSelection().empty) { // Chrome
                            window.getSelection().empty();
                        }
                        else 
                            if (window.getSelection().removeAllRanges) { // Firefox
                                window.getSelection().removeAllRanges();
                            }
                    }
                    else 
                        if (document.selection) { // IE?
                            document.selection.empty();
                        }
                    $(objThis.highlightcontainerRef.element).find('[type=saveBtn]').removeClass('ui-disabled');
                    try {
                              $(objThis.highlightcontainerRef.element).find('[type=saveBtn]').data("buttonComp").enable();
                    } catch(e) {}
                    
                    if (objThis.newHighlight == true) {
                        objThis.currentselection = ".selection" + GlobalModel.totalStickyNotes;
                    }
                    else {
                        $(objThis.currentselection).removeClass($(objThis.currentselection).attr("class").split(" ")[1]);
                        
                    }
                    
                    
                    $(objThis.currentselection).removeClass("selectioncolor0");
                    $(objThis.currentselection).removeClass(objThis.currentselectioncolor);
                    
                    $(objThis.currentselection).addClass("selection" + this.id);
                    objThis.currentselectioncolor = "selection" + this.id;
                }
                
            }
            
            if( isWinRT )
            {
				for( var i = 0; i < $(colorBtnRef).length; i++ )
				{
			    	$(colorBtnRef)[i].addEventListener(WinRTPointerEventType, clickHandler);
				}
			    
            }
            else
            {
            	 $(colorBtnRef).unbind(eventType).bind(eventType, clickHandler);
            }
                        
            // This will define all the functionality of delete button in highlight popup.    
            
            $(deleteBtnRef).unbind('click').bind('click', function(){
                PopupManager.removePopup();
                $($(objThis.deleteConfirmationPanel).find('[id=dltConfirmationTxt]')[0]).html(GlobalModel.localizationData["HIGHLIGHT_DELETE_CONFIRMATION_TEXT"]);
                objThis.openDeleteConfirmationPanel(this);
                objThis.deletefromPanel = false;
                
            });
            
            // This will define all the functionality of save button in highlight popup.    
            
            $(saveBtnRef).unbind('click').bind('click', function(e){
            
                objThis.saveHighlight();
            });
            
            
            break;
            
        // This will define all the functionality of Delete confirmation popup for highlight.    
        
        case AppConst.DLT_CONFIRM_PANEL:
            
            objThis.deleteConfirmationPanel = $(objComp.element);
            
            var dltConfirmSuccessRef = ($(objComp.element).find('[id=dltConfirmationSuccess]')[0]);
            var dltConfirmFailureRef = ($(objComp.element).find('[id=dltConfirmationFailure]')[0]);
            
            $(dltConfirmSuccessRef).unbind('click').bind('click', function(){
            

                $(objThis.currentselection).unbind('click');
                if( ( GlobalModel.annotations[objThis.currentAnnotation].annotationID != undefined ) && ( GlobalModel.annotations[objThis.currentAnnotation].annotationID != 0 ))
                {
	                ServiceManager.Annotations.remove(GlobalModel.annotations[objThis.currentAnnotation].annotationID, 2);
                }
               
                (GlobalModel.annotations).splice(objThis.currentAnnotation, 1);
                
				$(objThis.currentselection).removeClass(objThis.currentselectioncolor);
				$(objThis.currentselection).removeClass(objThis.currentselectioncolor+"_saved");
				$(objThis.currentselection).removeClass(objThis.currentselection.replace('.', ''));

                
                $("#highlightData_" + objThis.currentIDtoDelete).remove();
                $("#NotesPanel").data('annotationPanelComp').member.notePanelScroll.refresh()
                PopupManager.removePopup();
                objThis.newHighlight = true;
                objThis.currentAnnotation = undefined;
                objThis.scrollviewRef.options.rangedata = "";
            });
            
            $(dltConfirmFailureRef).unbind('click').bind('click', function(){
            
                PopupManager.removePopup();
                if (window.getSelection) {
	                if (window.getSelection().empty) {// Chrome
	                    window.getSelection().empty();
	                }
	                else 
	                    if (window.getSelection().removeAllRanges) {// Firefox
	                        window.getSelection().removeAllRanges();
	                    }
	            }
	            else 
	                if (document.selection) {// IE?
	                    document.selection.empty();
	                }
            	objThis.scrollviewRef.options.rangedata = "";
            });
            
            break;
            
        case AppConst.SCROLL_VIEW_CONTAINER:
            objThis.scrollviewRef = objComp;
            
            $(objComp).bind(objComp.events.PAGE_RENDER_COMPLETE, function(){
                objThis.scrollViewRenderCompleteHandler(objComp);
            });
            
            break;
            
        // This will define all the functionality of highlight button in annotation bar popup.    
        
        case AppConst.HIGHLIGHT_ANNOTATION_BTN:
            
            //            objThis.highlightBtn = objComp;
            if (isArray(EPubConfig.Highlight_colors) == false) {
                throw new Error("List of highlight colors is not an array");
            }
            
            $(objComp.element).unbind('click').bind('click', function(e){
                if(GlobalModel.hasDPError("highlight"))
            		return;
                if (ismobile)
                {
                	document.onselectionchange = null;
                	$("#annotation-bar").css("display","none");
	            	$("body").append('<input type="text" id="inputToRemove" style="width:0;height:0"/>')
		    		$("#inputToRemove").focus();
		    		$("#inputToRemove").blur();
		    		$("#inputToRemove").remove();	
                }
                	            
                $($(objThis.highlightcontainerRef.element).find('[type=deleteBtn]')[0]).removeClass('ui-enabled');
                $($(objThis.highlightcontainerRef.element).find('[type=deleteBtn]')[0]).addClass('ui-disabled');
                $($(objThis.highlightcontainerRef.element).find('[type=saveBtn]')[0]).removeClass('ui-disabled');
                $($(objThis.highlightcontainerRef.element).find('[type=saveBtn]')[0]).addClass('ui-enabled');
                
                try {
                          $($(objThis.highlightcontainerRef.element).find('[type=deleteBtn]')[0]).data("buttonComp").disable();
                          $($(objThis.highlightcontainerRef.element).find('[type=saveBtn]')[0]).data('buttonComp').enable();
                } catch(e){}
                
                $(".colorButtonSelected").removeClass('colorButtonSelected').addClass('colorButton');
                
                objThis.rangedata = objThis.scrollviewRef.options.rangedata;
                //As it is commented in 6to12
                /*$(window).bind('resize', function(){
                    var highlightPop = $("[type='highlightContainer']");
                    if (highlightPop.css('display') == 'block') {
                        highlightPop.hide();
                        objThis.updateHighlightPopPos();
                    }
                })*/
                objThis.updateHighlightPopPos();
            });
            break;
            
        case AppConst.NOTE_SECTION_PANEL:
            
            $(objComp.element).bind('editHighlightData', function(e, currentListId){
               if(e) {
                	e.stopPropagation();
                }
                $(objThis.highlightcontainerRef.element).find('[type=saveBtn]').addClass('ui-disabled');
                try {
                          $(objThis.highlightcontainerRef.element).find('[type=saveBtn]').data('buttonComp').disable();
                } catch(e) {}
                var pageIndex;
                var pageNumValue;
                var rangedata;
                objThis.newHighlight = false;
                $($(objThis.highlightcontainerRef.element).find('[type=deleteBtn]')[0]).removeClass('ui-disabled');
                try {
                          $($(objThis.highlightcontainerRef.element).find('[type=deleteBtn]')[0]).data('buttonComp').enable();
                } catch(e) {}
                $($(objThis.highlightcontainerRef.element).find('[type=deleteBtn]')[0]).addClass('ui-enabled');
                objPopupPanel = objThis.highlightcontainerRef.element;
                
                for (var i = 0; i < GlobalModel.annotations.length; i++) {
                    if (GlobalModel.annotations[i]) {
                        if (GlobalModel.annotations[i].type == "highlight") {
                            if (GlobalModel.annotations[i].id == parseInt(currentListId)) {
                            
                                pageIndex = GlobalModel.annotations[i].pageNumber;
                                objThis.scrollviewRef.options.rangedata = GlobalModel.annotations[i].range;
                                objThis.currentselection = GlobalModel.annotations[i].selectionID;
                                objThis.currentsaved = GlobalModel.annotations[i].selectioncolor + "_saved";
                                objThis.currentAnnotation = i;
                            }
                        }
                    }
                }
                
                
                $(objThis.highlightcontainerRef.element).find('[type=saveBtn]').addClass('ui-disabled');
                try {
                          $(objThis.highlightcontainerRef.element).find('[type=saveBtn]').data('buttonComp').disable();
                } catch(e) {}
                objThis.newHighlight = false;
                $($(objThis.highlightcontainerRef.element).find('[type=deleteBtn]')[0]).removeClass('ui-disabled');
                try {
                          $($(objThis.highlightcontainerRef.element).find('[type=deleteBtn]')[0]).data('buttonComp').enable();
                } catch(e) {}
                $($(objThis.highlightcontainerRef.element).find('[type=deleteBtn]')[0]).addClass('ui-enabled');
                objPopupPanel = objThis.highlightcontainerRef.element;

                var pageNumValue = GlobalModel.pageBrkValueArr.indexOf(String(pageIndex));
                var scrollviewCompRefnce = $("#bookContentContainer").data('scrollviewcomp');
                var curPageIndex = scrollviewCompRefnce.member.curPageElementsPgBrk.indexOf(String(pageIndex));
                var curselectionTop = 0;
                if($(objThis.currentselection).length)
                {
					curselectionTop = $(objThis.currentselection).offset().top;
					if(!($($(objThis.currentselection)[0]).offset().top > 0 && $($(objThis.currentselection)[0]).offset().top < ($(window).height()-20*GlobalModel.currentScalePercent) && $($(objThis.currentselection)[0]).offset().left > $("#bookContentContainer").css('margin-left').slice(0,-2) && $($(objThis.currentselection)[0]).offset().left < ($(window).width()-30)))
					{
						objThis.doFunctionCall(AppConst.SCROLL_VIEW_CONTAINER, 'resetZoom');            		
					} 
                }
                else
                {
                	objThis.doFunctionCall(AppConst.SCROLL_VIEW_CONTAINER, 'resetZoom'); 
                }
                if(GlobalModel.currentPageIndex == curPageIndex)
                {
                    if (EPubConfig.pageView.toUpperCase() == 'SCROLL')
					{
						var scrollBy = $($(objThis.currentselection)[0]).offset().top + $("#bookContentContainer2").scrollTop();
                    	$("#bookContentContainer2").scrollTop(scrollBy - 100);
					}
				}
				else
				{
               		objThis.isOpenHighlight = true;
               		GlobalModel.currentPageIndex=curPageIndex;
               		if(curselectionTop < $(window).height() && EPubConfig.pageView.toUpperCase() == 'SCROLL' && curselectionTop)
					{
						var scrollBy = $($(objThis.currentselection)[0]).offset().top + $("#bookContentContainer2").scrollTop();
						$("#bookContentContainer2").scrollTop(scrollBy - 100);
					}
					else
						objThis.doFunctionCall(AppConst.SCROLL_VIEW_CONTAINER, 'showPageAtIndex', pageNumValue);
				}
                window.getSelection().removeAllRanges();
                
                if (!ismobile) {
                    window.getSelection().addRange(objThis.scrollviewRef.options.rangedata);
                }
                
            });
            
            $(objComp.element).bind('deleteHighlightData', function(e, currentListId){
				e.stopPropagation();
				if(event) {
					event.stopPropagation();
				}
                var pageIndex;
                var rangedata;
                $($(objThis.highlightcontainerRef.element).find('[type=deleteBtn]')[0]).removeClass('ui-disabled');
                try {
                          $($(objThis.highlightcontainerRef.element).find('[type=deleteBtn]')[0]).data('buttonComp').enable();
                } catch(e) {}
                $($(objThis.highlightcontainerRef.element).find('[type=deleteBtn]')[0]).addClass('ui-enabled');
                
                for (var i = 0; i < GlobalModel.annotations.length; i++) {
                    if (GlobalModel.annotations[i]) {
                        if (GlobalModel.annotations[i].type == "highlight") {
                            if (GlobalModel.annotations[i].id == parseInt(currentListId)) {
                            
                                pageIndex = GlobalModel.annotations[i].pageNumber;
                                objThis.scrollviewRef.options.rangedata = GlobalModel.annotations[i].range;
                                objThis.currentselectioncolor = GlobalModel.annotations[i].selectioncolor;
                                //                              objThis.currentselection = ".selection"+GlobalModel.annotations[i].id;
                                objThis.currentselection = GlobalModel.annotations[i].selectionID;
                                objThis.currentsaved = GlobalModel.annotations[i].selectioncolor + "_saved";
                                objThis.currentAnnotation = i;
                                objThis.currentIDtoDelete = GlobalModel.annotations[i].id;
                            }
                        }
                    }
                }
                
                var rangeData = objThis.scrollviewRef.options.rangedata;
                
                objThis.deletefromPanel = true;
                
                setTimeout(function(){
                    $($(objThis.deleteConfirmationPanel).find('[id=dltConfirmationTxt]')[0]).html(GlobalModel.localizationData["HIGHLIGHT_DELETE_CONFIRMATION_TEXT"]);
                    objThis.openDeleteConfirmationPanel(this);
                }, 100);
                objThis.scrollviewRef.options.rangedata = "";
            });
            
            break;
            
        default:
            break;
            
    }
    
    return MG.BaseOperator.prototype.attachComponent.call(this, objComp);
    
    
}

/**
 * This function will open the deleteconfirmation panel
 */
MG.highlighterOperator.prototype.openDeleteConfirmationPanel = function(launcher){
    var objThis = this;
    objThis.currentselection = "." + $(launcher).attr("class").split(" ")[0];
        
    for (var counter = 0; counter < GlobalModel.annotations.length; counter++) {
        if (GlobalModel.annotations[counter].type == 'highlight') {
        
            if (GlobalModel.annotations[counter].selectionID == "." + $(launcher).attr("class").split(" ")[0]) {
                objThis.currentselectioncolor = GlobalModel.annotations[counter].selectioncolor;
                objThis.currentAnnotation = counter;
            }
        }
    }
    PopupManager.addPopup($(objThis.deleteConfirmationPanel), $(objThis.currentselection), {
        isModal: true,
        isTapLayoutDisabled: true,
        popupOverlayStyle: "popupMgrMaskStyleSemiTransparent",
        isCentered: false
    
    });
    
    if( isWinRT )
    {
    	setTimeout(function(){window.getSelection().removeAllRanges();},0);//Fix to remove selection that appear on the buttons of the panel.
    }
    
    if($(objThis.deleteConfirmationPanel).offset().left < $("#bookContentContainer").css('margin-left').slice(0,-2))
    {
    	$(objThis.deleteConfirmationPanel).css("left",$("#bookContentContainer").css('margin-left').slice(0,-2)+"px");
    }
    if(($(objThis.deleteConfirmationPanel).offset().left + $(objThis.deleteConfirmationPanel).width()) > $(window).width() - 75)
    {
    	$(objThis.deleteConfirmationPanel).css("left",$(window).width() - $(objThis.deleteConfirmationPanel).width() - 75+"px");
    }
    if($(objThis.deleteConfirmationPanel).offset().top < 0)
    {
    	$(objThis.deleteConfirmationPanel).css("top","10px");
    } 
    if(($(objThis.deleteConfirmationPanel).offset().top + $(objThis.deleteConfirmationPanel).height()) > $(window).height() - 50)
    {
    	$(objThis.deleteConfirmationPanel).css("top",$(window).height() - $(objThis.deleteConfirmationPanel).height() - 50+"px");
    }
    
    if(EPubConfig.pageView.toUpperCase() == 'SCROLL')
    {
        if($(objThis.deleteConfirmationPanel).offset().top < $(objThis.currentselection).offset().top)
            $(objThis.deleteConfirmationPanel).css('margin-top',"-5px");
        else
            $(objThis.deleteConfirmationPanel).css('margin-top',"8px");
    }
    else
            $(objThis.deleteConfirmationPanel).css('margin-top',"-5px");
    
     if (event) {
  		 event.stopPropagation();
  	 }
}


MG.highlighterOperator.prototype.saveHighlight = function(){
    var objThis = this;
    var currentListId;
    var highlightId;
    if (window.getSelection) {
        if (window.getSelection().empty) { // Chrome
            window.getSelection().empty();
        }
        else 
            if (window.getSelection().removeAllRanges) { // Firefox
                window.getSelection().removeAllRanges();
            }
    }
    else 
        if (document.selection) { // IE?
            document.selection.empty();
        }
    
    if (this.newHighlight == true) {
        this.currentselection = ".selection" + GlobalModel.totalStickyNotes;;
        this.currentAnnotation = GlobalModel.annotations.length;
        currentListId = this.currentAnnotation;
        highlightId = GlobalModel.annotations.length;
    }
    else {
        currentListId = this.currentAnnotation;
        highlightId = this.currentselection.replace('.selection', '');
        
    }
    if (this.currentselectioncolor == "selectioncolor0") {
        for (var i = 0; i < EPubConfig.Highlight_colors.length; i++) {
            if (EPubConfig.Highlight_colors[i] == EPubConfig.Highlights_default_color) {
                break;
            }
        }
        this.currentselectioncolor = "selectioncolor" + (i + 1);
    }
    $(this.currentselection).addClass(this.currentselectioncolor);
    $(this.currentselection).removeClass(this.currentselectioncolor);
    $(this.currentselection).addClass(this.currentselectioncolor + "_saved");
    
    
    var container = document.createElement("span");
    container.appendChild(this.scrollviewRef.options.rangedata.cloneContents()).innerHTML;
    $(container).addClass($(this.scrollviewRef.options.rangedata.commonAncestorContainer.parentElement).attr('class'));
    highlightContent = container.outerHTML;
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var year = d.getFullYear();
    var dateOfCreation = (('' + month).length < 2 ? '0' : '') + month + '/' + (('' + day).length < 2 ? '0' : '') + day + '/' + year.toString().substr(2, 2);
    
    
    
    var arrObj = new Array();
    arrObj.title = highlightContent;
    arrObj.id = highlightId;
    arrObj.rangeObject = this.scrollviewRef.options.rangeObject;
    arrObj.date = dateOfCreation;
    arrObj.style = 0;
    arrObj.type = "highlight";
    arrObj.range = this.scrollviewRef.options.rangedata;
    arrObj.selectioncolor = this.currentselectioncolor;
    arrObj.selectionID = this.currentselection;
    arrObj.timeMilliseconds = d;
    arrObj.tagsLists = [];
    var yPos = this.scrollviewRef.options.Icontop + $("#bookContentContainer2").scrollTop();
    arrObj.top = yPos;
    arrObj.pageNumber = (this.scrollviewRef.options.pageIndexForAnnotation);
    
    
    GlobalModel.annotations[parseInt(currentListId)] = (arrObj);
    
    if (this.newHighlight == true) {
        ServiceManager.Annotations.create(arrObj, 2, function( data ){
																	//console.log(arrObj, "arrobj   ", GlobalModel.annotations, "GlobalModel.annotations  ", data, "data");
																	for( var i = 0; i < GlobalModel.annotations.length; i++ )
																	{
																		if( ( arrObj.id == GlobalModel.annotations[i].id ) && ( arrObj.type == GlobalModel.annotations[i].type) )
																		{
																			//alert("Here");
																			GlobalModel.annotations[i].annotationID = data.annotation_id;// approach might change
																			//alert(GlobalModel.annotations[i].annotation_id + "                         " + i);
																			break;
																		}
																	}
																	});
    }
    else {
		arrObj.annotationID =  GlobalModel.annotations[parseInt(currentListId)].annotationID;
		GlobalModel.annotations[parseInt(currentListId)] = (arrObj);
		if( ( arrObj.annotationID != undefined ) && ( arrObj.annotationID != 0 ))
		{
        ServiceManager.Annotations.update(2, arrObj);
    }
    }
    
    
    
    if (this.newHighlight == true) {
        $(".selection" + GlobalModel.totalStickyNotes).unbind('click').bind('click', function(e){
        	var _parentElement = $($(e.target)[0].parentElement) ;
        	if( ( _parentElement.attr("type") == "glossterm" ) || ( _parentElement.attr("type") == "footnote" ) || ( _parentElement.attr("data-link") == "internal-link" ) || ( _parentElement.attr("data-ajax") == "false" ) )
	    	{
	    		return;
	    	}
        	if(GlobalModel.hasDPError("highlight"))
            		return;
        	e.stopPropagation();
        /*	if($(e.currentTarget.parentNode).attr("type") == "footnote")
				return;*/
            PopupManager.removePopup();
            $($(objThis.deleteConfirmationPanel).find('[id=dltConfirmationTxt]')[0]).html(GlobalModel.localizationData["HIGHLIGHT_DELETE_CONFIRMATION_TEXT"]);
            
            
            objThis.currentAnnotation = $(this).attr("class").split(" ")[0].replace('selection', '');

            
            objThis.openDeleteConfirmationPanel(this);
            objThis.deletefromPanel = false;
        });
        
        
        GlobalModel.totalStickyNotes++;
    }
    
    PopupManager.removePopup();
    this.newHighlight = true;
    objThis.scrollviewRef.options.rangedata = "";
}

MG.highlighterOperator.prototype.updateHighlightPopPos = function(){
    var objThis = this;
     PageNavigationManager.removeSwipeListner(objThis);
    var highlightPop = $("[type='highlightContainer']");
    setTimeout(function(){
        objThis.findHighlightMatch();
        objThis.doFunctionCall(AppConst.HIGHLIGHT_ANNOTATION_BTN, 'onhighlightAnnotationBtnClick', highlightObj, matchFound);
        highlightPop.show();
    }, 0);
}

MG.highlighterOperator.prototype.findHighlightMatch = function(){
    var objThis = this;
    var highlightObj = {
        numberofHighlights: GlobalModel.totalStickyNotes,
        highlightContainer: objThis.highlightcontainerRef,
        rangeData: objThis.scrollviewRef.options.rangedata,
        defaultcolor: objThis.currentselectioncolor,
        topPos: objThis.scrollviewRef.options.toppos,
        leftPos: objThis.scrollviewRef.options.Iconleft,
    };
    var matchFound = 1;
    
    for (var annVerify = 0; annVerify < GlobalModel.annotations.length; annVerify++) {
    
        if ((GlobalModel.annotations[annVerify].type == 'highlight') && (GlobalModel.annotations[annVerify].range)) {
        
            var txt = document.createElement('div');
            txt.appendChild(highlightObj.rangeData.cloneContents());
            
            var container = document.createElement("span");
            container.appendChild(highlightObj.rangeData.cloneContents()).innerHTML;
            var highlightContent = container.outerHTML;
            
            var arrContent = GlobalModel.annotations[annVerify].title.replace(/<([^<>]+)>/g, "");
            var currContent = highlightContent.replace(/<([^<>]+)>/g, "");
            var arrRect = GlobalModel.annotations[annVerify].range.getBoundingClientRect();
            var currRect = highlightObj.rangeData.getBoundingClientRect();
            
            if ($(txt).find(GlobalModel.annotations[annVerify].selectionID).length != 0) {
                matchFound = 0;
                objThis.currentselection = GlobalModel.annotations[annVerify].selectionID;
                                
            }
            
        }
    }
    
    if (matchFound == 0) {
        $(objThis.currentselection).trigger("click");
        
        
    }
    
    return matchFound;
    
}
MG.highlighterOperator.prototype.scrollViewRenderCompleteHandler = function(objComp){
    var objThis = this;
    var arrPageComps = $(objComp.element).find('[data-role="pagecomp"]');
    var objPageRole = null;
    for (var i = 0; i < arrPageComps.length; i++) {
        objPageRole = $(arrPageComps[i]).data("pagecomp");
        
        
        
        $(objPageRole).bind("createHighlightOnPage", function(e)
		{
			objThis.reDrawHighlight();
			
        });
    }
}

MG.highlighterOperator.prototype.reDrawHighlight = function()
{
	var objThis = this;
	var scrollviewCompRefnce = $("#bookContentContainer").data('scrollviewcomp');
	for(var counter=0; counter<GlobalModel.annotations.length; counter++)
	{
		if((GlobalModel.annotations[counter].type=='highlight'))
		{
			var selectioncolor=GlobalModel.annotations[counter].selectioncolor;
			if(EPubConfig.pageView == 'doublePage')
			{
				if(scrollviewCompRefnce.member.curPageElementsPgBrk.indexOf(GlobalModel.annotations[counter].pageNumber) == GlobalModel.currentPageIndex || scrollviewCompRefnce.member.curPageElementsPgBrk.indexOf(GlobalModel.annotations[counter].pageNumber) == GlobalModel.currentPageIndex + 1)
				{
					objThis.createHighlight(GlobalModel.annotations[counter].selectionID, selectioncolor, GlobalModel.annotations[counter].rangeObject);
				}
			}
			else
			{
				if(scrollviewCompRefnce.member.curPageElementsPgBrk.indexOf(GlobalModel.annotations[counter].pageNumber) == GlobalModel.currentPageIndex)
				{
					objThis.createHighlight(GlobalModel.annotations[counter].selectionID, selectioncolor, GlobalModel.annotations[counter].rangeObject);
				}
			}	
		}
	}
	
}

MG.highlighterOperator.prototype.restoreSelection = function(containerEl, savedSel){
    var charIndex = 0, range = document.createRange();
    range.setStart(containerEl, 0);
    range.collapse(true);
    var nodeStack = [containerEl], node, foundStart = false, stop = false;
//    console.log(nodeStack);
    while (!stop && (node = nodeStack.pop())) {
        if (node.nodeType == 3) {
            var nextCharIndex = charIndex + node.length;
            
            if (!foundStart && savedSel.start >= charIndex && savedSel.start <= nextCharIndex) {
                range.setStart(node, savedSel.start - charIndex);
                foundStart = true;
                
            }
            if (foundStart && savedSel.end >= charIndex && savedSel.end <= nextCharIndex) {
                range.setEnd(node, savedSel.end - charIndex);
                
                stop = true;
            }
            charIndex = nextCharIndex;
        }
        else {
            var i = node.childNodes.length;
            while (i--) {
                nodeStack.push(node.childNodes[i]);
            }
        }
    }
    
    var sel = window.getSelection();
    sel.removeAllRanges();
    sel.addRange(range);
    return range;
}

MG.highlighterOperator.prototype.createHighlight = function(selectionID, selectioncolor, objRange){
console.log( selectionID, selectioncolor, objRange ,"this is createHighlight");
    var rangedata;
    var objThis = this;

  	setTimeout(function()
  	{

    var Obj = $.xml2json(objRange);
    
    var startC = $('#' + Obj.startContainer).find('#mainContent')[0];
    
    var startO = parseInt(Obj.startOffset);
    var endO = parseInt(Obj.endOffset);
    
    var savedSel = {
        start: startO,
        end: endO
    };
    if($(startC)[0] == undefined)
		return;
    rangedata = objThis.restoreSelection($(startC)[0], savedSel);
    
    rangy.init();
    var cssClassApplier = rangy.createCssClassApplier(selectionID.replace('.', ''));
    
    var cssClassAppliercolor = rangy.createCssClassApplier(selectioncolor + '_saved');
    
    
    window.getSelection().removeAllRanges();
    window.getSelection().addRange(rangedata);
    
    if($(startC).find(selectionID)[0] == undefined)
	{
		cssClassApplier.toggleSelection();
		cssClassAppliercolor.toggleSelection();
	}
    $(selectionID).unbind('click').bind('click', function(e){
    	var _parentElement = $($(e.target)[0].parentElement) ;
    	if( ( _parentElement.attr("type") == "glossterm" ) || ( _parentElement.attr("type") == "footnote" ) || ( _parentElement.attr("data-link") == "internal-link" ) || ( _parentElement.attr("data-ajax") == "false" ) )
    	{
    		return;
    	}
    	if(GlobalModel.hasDPError("highlight"))
    		return;
    	e.stopPropagation();
    	if($(e.currentTarget.parentNode).attr("type") == "footnote")
			return;
    	PopupManager.removePopup();
        $($(objThis.deleteConfirmationPanel).find('[id=dltConfirmationTxt]')[0]).html(GlobalModel.localizationData["HIGHLIGHT_DELETE_CONFIRMATION_TEXT"]);
        objThis.currentAnnotation = $(this).attr("class").split(" ")[0].replace('selection', '');
        objThis.openDeleteConfirmationPanel(this);
        objThis.deletefromPanel = false;
    });
    
    window.getSelection().removeAllRanges();
	if(objThis.currentAnnotation != undefined)
	{
		if($(GlobalModel.annotations[objThis.currentAnnotation].selectionID).attr('class'))
			objThis.openHighlighterPopup();
	}
	}, 0);
}

MG.highlighterOperator.prototype.openHighlighterPopup = function(){
	var objThis = this;
	setTimeout(function()
	{
		if(objThis.isOpenHighlight)
		{
			
			objThis.currentselection = GlobalModel.annotations[objThis.currentAnnotation].selectionID;
			objThis.isOpenHighlight = false;
			if (EPubConfig.pageView.toUpperCase() == 'SCROLL')
			{
				var scrollBy = $($(objThis.currentselection)[0]).offset().top + $("#bookContentContainer2").scrollTop();
            	$("#bookContentContainer2").scrollTop(scrollBy - 100);
			}
		}
	},0);
}

MG.highlighterOperator.prototype.getElementsByXPath = function(doc, xpath){
    var nodes = [];
    
    try {
        var result = doc.evaluate(xpath, doc, null, XPathResult.ANY_TYPE, null);
        
        for (var item = result.iterateNext(); item; item = result.iterateNext()) {
            nodes.push(item);
            
        }
    } 
    catch (exc) {
        // Invalid xpath expressions make their way here sometimes.  If that happens,
        // we still want to return an empty set without an exception.
    }
    return nodes;
}
