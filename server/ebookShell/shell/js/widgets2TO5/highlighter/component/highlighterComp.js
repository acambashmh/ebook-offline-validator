/**
 * @author Harsh Chawla
 * This class handles the functionality of highlighter
 */

(function($, undefined) {
	$.widget("magic.highlighterComp", $.magic.magicwidget, {

		options : {
			objSuper : null
		},
		_create : function() {
			
			if(typeof EPubConfig.Highlight_colors === 'string' || $.isNumeric(EPubConfig.Highlight_colors) || EPubConfig.Highlight_colors == true || EPubConfig.Highlight_colors == false)
        	{
        		console.error("List of highlight colors is not an array");
        		throw new Error("List of highlight colors is not an array");
        	}
        	
			//CSS added based on Config settings for highlight colors
			
			var maxHighlightColors = 5;
			
			$('head').append("<style>.selectioncolor0{background:" + EPubConfig.Highlights_default_color + ";}</style>");
			for (var i = 0; i < EPubConfig.Highlight_colors.length; i++) {
				$('head').append("<style>.selectioncolor" + (i + 1) + "{background:" + EPubConfig.Page_Highlight_colors[i] + ";}.selectioncolor" + (i + 1) + "_saved{cursor:pointer;background:" + EPubConfig.Page_Highlight_colors[i] + ";user-select: none;-webkit-user-select: none;-moz-user-select: none;}#color" + (i + 1) + "{background:url('" + strReaderPath +  "css/2TO5/images/main/iconSprite.png') " + EPubConfig.Highlight_colors[i] + ";background-position:-343px -75px;border-radius:100px;}#color" + (i + 1) + ":hover{background:url(strReaderPath +  'css/2TO5/images/main/iconSprite.png') " + EPubConfig.Highlight_colors[i] + ";background-position:-428px -75px;border-radius:100px;}#color" + (i + 1) + ":active{background:url(strReaderPath + 'css/2TO5/images/main/iconSprite.png') " + EPubConfig.Highlight_colors[i] + ";background-position:-485px -75px;border-radius:100px;}</style>");
				$('head').append("<style>.selectioncolor" + (i + 1) + "_icon{background: url('" + strReaderPath +  "css/2TO5/images/stickynote/highlight-pencil_panel.png') " + EPubConfig.Highlight_colors[i] + ";}</style>");
				
				$('.colorContainer').append('<div id="color' + (i + 1) + '" type="color" class="highlighter-anno ui-enabled" data-role="bookmarkComp" style="display: block;"></div>')
				
				//not allowing more than maxHighlightColors count to be added - as discussed with sumant
				if(i == (maxHighlightColors - 1))
				{
					break;
				}
			}
			
			$(".tagInputBox").attr("placeholder", GlobalModel.localizationData["TAG_PLACEHOLDER_TEXT"]);

		},
		getPanelViewComp : function() {
			return $('#stickyNotePanel');
		},

		modifyHighlightData : function(arrObj) {

			var totalHighlights = arrObj.totalHighlights;
			var highlightModifiedId = arrObj.id;
			var Highlightvalue = arrObj.title;
			var d = new Date();
			var stickynoteTitle = Highlightvalue;
			for (var i = 0; i < totalHighlights; i++) {
				if (GlobalModel.annotations[i]) {
					if (GlobalModel.annotations[i].id == highlightModifiedId) {
						GlobalModel.annotations[i].title = Highlightvalue;
						GlobalModel.annotations[i].timeMilliseconds = d;

					}
				}

			}

			var arrObj = new Array();
			arrObj.title = HighlightTitle;
			arrObj.id = totalHighlights;
			//arrObj.timeMilliseconds = d;
			return arrObj;

		},
	});
})(jQuery);

