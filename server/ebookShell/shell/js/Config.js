/**
 * @author sumanta.mishra
 */

EPubConfig = {
	pageWidth: 820,
	pageHeight: 1171,
	
	totalPages: 14,
	pageNamePrefix: "page",
	pageNameExtension: ".xhtml",
	
	tocFileName: "toc.xhtml",
	indexXMLPath: "index.xml",
	standardsXMLPath: "standards.json",
	standardsDescriptionXMLPath: "Florida_Common_Core_Standards_ELA_6-12.json",
	baseContentPath: "content/en",
	toolsXMLPath: "tools.json",
	
	bookTitle: "ePubPOC",
	contentFolderName: "OPS",
	smilFolderName: "smil",
	intractivityPath:"data/questionAnswer",
	ePubGlobalSettingsFile: "config.txt",
	
	programBrandingTitle: "Program Branding",
    programBrandingSrc: "content/image/brandName.png",
    logoImgDivSrc: "content/image/HMH_logo.png",
    logoImgDiv2Src: "content/image/brandBGImage.png",
	
	cssFolderName: "css",
	cssFileNames: [
		"template.css"
	],
	
	imageFolderName: "images",
	audioFolderName: "audio",
	fontsFolderName: "fonts",
	//labelsLangPath: "",
	//labelsFileNames: [],
	ReaderType: "",
	
	//glossary configurations
	glossaryXMLPath: "content/en/ePubPOC/OPS/data/glossary.xml",
	glossaryType: "in-context",
	FULL_GLOSSARY_PATH: "http://www.google.com",
	MY_NOTEBOOK_PATH: "http://www.google.com",

	isPageOrderInSequence: false, //false if page names come from TOC; true if page names are in sequence i.e. page0001.xhtml
	 
	/*
	 * Below variables help is communicating with external services. These specify the hierarchy structure of current book
	 */
	
	LevelIdentifier_L0: "Grade",
	
	//Level 1 represents volume
	LevelIdentifier_L1: "Volume",
	
	//Level 2 represents unit
	LevelIdentifier_L2: "Unit",
	
	//Level 3 represents chapter/lesson
	LevelIdentifier_L3: "Lesson",
	
	//Level 4 represents section
	LevelIdentifier_L4: "Section",
	
	//Level 5 represents page
	LevelIdentifier_L5: "Page",
	
	//Level 6 represents page
	LevelIdentifier_L6: "Page",
	
	L0: "false",
    L0_label: "Grade",
    L0_value: "",
    L1: "false",
    L1_label: "Volume",
    L1_value: "",
    L2: true,
    L2_label: "Collection",
    L2_value: "",
    L3: true,
    L3_label: "Lesson",
    L3_value: "",
    L4: true,
    L4_label: "Section",
    L4_value: "",
    L5: true,
    L5_label: "Page",
    L5_value: "",
    L6: "false",	
    L6_label: "",
    L6_value: "",

	//TOCStartLevel specifies which level the TOC starts from.
	TOCStartLevel: "L2",
	
	//	Unit/Lesson/Section/Page 	
	//If no value is given or variable is not defined Navigation links would not be available 
	scrollNavType : 'Page',		
	
	//for theme CSS & themehover CSS
	Theme: "2TO5",
	
	//Page layout and navigation configurations
	//Page_Layout_Type: "Fixed",
	Page_Navigation_Type: "SCROLL",
	
	//Annotation configurations;
	//Annotation_Live_Data_Persistence: "",
	//Annotation_Service_URL: "",
	
	//URL to fetch resource list from
	//Resources_Service_URL: "",
	
	//notes configurations
	Notes_Per_Page: 5,
	//Teacher_Notes_Per_Page: 0,
	
	//Help configuration
	//Help_Type: "",
	//Help_URL: "",
	
	//Search Configuration
	//Search_URL: "",
	
	//Highlight configuration
	Highlights_default_color: "#f6d855",
	Highlight_colors: ["#40e6d9", "#31b21f", "#f6d855", "#ee5ee9"],
	
	//Carousel/Thumbnail configuration
	//Thumbnail_Path: "",
	// Thumbnail_Width: 0,
	// Thumbnail_Height: 0,
	
	//Read aloud configuration
	Text_Highlight: 'ON',
	// Read_Aloud_level: "",
	
	//language to be used for localization;
	LocalizationLanguage: "en",
	defaultLocalizationLanguage: "en",
	
	//Markup_Tool_default_Options: ["Notes", "Highlight"],
	//Markup_default_Color: "#FF00FF",

	
	//Required For HMH to configure required functionalities
	Glossary_isAvailable: true,
	Annotations_isAvailable: true,
	Markup_Tool_isAvailable: true,
	Resources_isAvailable: true,
	Notes_isAvailable: true,
	Highlights_isAvailable: true,
	Help_isAvailable: true,
	BookMark_isAvailable: true,
	//Tools_isAvailable: false,
	Zoom_isAvailable: true,
	Index_isAvailable: true,
	Search_isAvailable: true,
	//Carousel_isAvailable: false,
	Read_Aloud_isAvailable: true,
	//TextSizeController_isAvailable: false,
	TOC_isAvailable: true,
	AudioPlayer_isAvailable: true,
	Copy_isAvailable: false,
	
	//to be used by reader (internal is available list)
	ContentsPanel_isAvailable: true,
	
	//this is mapping of Component Names and their availability property
	Feature_isAvaliblePropertyMapping: {
		"STICKY_NOTES": "Notes_isAvailable",
		"BOOKMARK": "BookMark_isAvailable",
		"GLOSSARY": "Glossary_isAvailable",
		"HELP": "Help_isAvailable",
		"HIGHLIGHTER": "Highlights_isAvailable",
		"COPY": "Copy_isAvailable",
		"RESOURCES_PANEL": "Resources_isAvailable",
		"ZOOM": "Zoom_isAvailable",
		"AUDIO_PLAYER": "AudioPlayer_isAvailable",
		"TABEL_OF_CONTENTS": "TOC_isAvailable",
		"SEARCH_PANEL" : "Search_isAvailable",
		"INDEX_PANEL" : "Index_isAvailable",
		"ANNOTATION_PANEL": "Annotations_isAvailable",
		"READ_ALOUD":"Read_Aloud_isAvailable",
		"CONTENTS_PANEL":"ContentsPanel_isAvailable"
	},

	//List of components in side panel. Values of Component (key) is the div ID of the respective component
	SidePanelComponents: {
		"AUDIO_PLAYER" : "audioLauncherBtn",
		"BOOKMARK": "bookmarkSectionLauncher",
		"HELP" : "helpLauncher",
		"RESOURCES_PANEL": "btnResourcePanel",
		"STICKY_NOTES": "stickyNoteLauncher", 
		"CONTENTS_PANEL": "btnTocLauncher",
		"ZOOM": "zoomBtn",
		"ANNOTATION_PANEL": "annotationPanel"
	},
	
	TabbedPanels: {
		"CONTENTS_PANEL": {
			"TABEL_OF_CONTENTS": "tocContents",
			"SEARCH_PANEL" : "tocSearch",
			"INDEX_PANEL" : "tocIndex"
		}
	},
	
	//List of components that are part of annotations and their container IDs
	AnnoToolNames: {
		"HIGHLIGHTER": "highlighter-anno",
		"STICKY_NOTES": "notes-anno"
	},
	
	//list of features that are being used in the application (Managed by using HMH settings)
	RequiredApplicationComponents: null,
	
	scaleToFit: true,
	
	// If value is true, swipe would be enabled otherwise swipe would be disabled
	swipeNavEnabled : false,
	
	navDataArr: '',
	pageView : 'singlePage',
	
	//new settings added on January 15, 2013 (mentioned this comment for documentation purpose)
	ContentId: "",			//32 character string representing the Book GUID or ISBN
	book_entitlement: "standard",
	maxCharactersInNotes: 1000,
	
	// Duration after which branding bar would auto-hide inspite of user's inactivity
	brandHideAfterDuration: '10000',
	brandHideInDuration: 400,
    
    // Added: Tuesday, 22/1/2013. This will specify the class level of the book.
	isForClass2To5: true
}