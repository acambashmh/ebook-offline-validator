function getInternetExplorerVersion() {

	var rv = -1;
	if (navigator.appName == 'Microsoft Internet Explorer') {

		var ua = navigator.userAgent;

		var re = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");

		if (re.exec(ua) != null)

			rv = parseFloat(RegExp.$1);
	}

	return rv;

}
/*
 Added following code to prevent Google prompts
 preventPrompt: true 
  
 */
function checkVersion() {
	var ver = getInternetExplorerVersion();

	if (ver > -1) {

		if (ver <= 9.0) {
			isIE8or9 = true;
			if (!IsGCFInstalled()) {
				$('body').css("display", "none");
				var i = confirm("Reader requires Google Chrome Frame plugin. Do you want to install?");
				if (i) {
					$.ajax({
						type : "GET",
						url : strReaderPath + "js/utils/CFInstall.min.js",
						processData : false,
						contentType : "plain/text",
						success : function(data) {

							//add file content to the application
							var oScript = document.createElement("script");
							oScript.language = "javascript";
							oScript.type = "text/javascript";
							oScript.text = data;
							$('#pg').append(oScript);

							$('#pg').append('<style>.chromeFrameInstallDefaultStyle{display:block;z-index: 200000;visibility: visible;width: 100%;height: 100%;margin-top: 0;margin-left: 0;top: 0;left: 0;}</style>');

							oScript = document.createElement("script");
							oScript.language = "javascript";
							oScript.type = "text/javascript";
							oScript.text = 'CFInstall.check({mode: "inline", preventPrompt: true, oninstall: function() {window.location = window.location;}});';
							$('#pg').append(oScript);
							
							$('body').css("display", "block");
						}
					});
				}
				else
				{
					document.write("")
				}
			}
		}
	}
}

function IsGCFInstalled() {

	try {

		var e = new ActiveXObject('ChromeTab.ChromeFrame');
		if (e) {
			return true;
		}
	} catch (e) {
		// console.log('ChromeFrame not available, error:', e.message);
	}
	return false;
}

var isIE8or9 = false;

if(strReaderPath == "")
{
	window.onload = checkVersion;
}
else
{
	checkVersion();
}
