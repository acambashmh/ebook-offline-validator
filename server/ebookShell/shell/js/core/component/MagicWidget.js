/**
 * Base class for all widgets used in the reader.
 */
(function($, undefined) {
	$.widget("magic.magicwidget", $.mobile.widget, {

		options : {
			scaleWidth : 0,
			scaleHeight : 0,
			notesX2 : 0,
			notesY2 : 0,
			notesX1 : 0,
			notesY1 : 0
		},
		member : {
			_x : 0,
			_y : 0,
			_width : 0,
			_height : 0,
			_bResizeable : false,
			_bMoveable : true
		},
		_create : function() {
			this.data.type = this.element.attr("type");
			var objClass = this;
			this.element.click(function() {
				objClass.onHandler()
			})
			this.element.taphold(function() {
				objClass.onTapHold()
			})
		},
		_init : function() {
			objThis = this;
			this.setLayout();
			this.setMovement();
			//Draggable movement
			$(window).bind("orientationchange", function() {
				objThis.setMovement();
			});
		},
		setMovement : function() {
			var objThis = this;
			var bMoveable = this.element.attr("moveable");
			if (eval(bMoveable)) {
				var objThis = this;
				objThis.setContainmentPos();
				$(this.element).draggable({
					delay : 1000,
					containment : [this.options.left, this.options.top, this.options.right, this.options.bottom],
					start : function(event, ui) {
						$(objThis).trigger('dragStart');
					},
					stop : function(event, ui) {
						$(objThis).trigger('dragStop');
					}
				});
			}

		},
		setContainmentPos : function() {
			/*
var onjCompProperty = $(this.element).stickyNoteComp("getCompSize");
			this.options.left = parseInt(onjCompProperty.left);
			this.options.top = parseInt(onjCompProperty.top);
			this.options.scaleInc = $('[data-role="zoomwrapper"]').data('zoomwrapper').member._nPercentScale;
			this.options.scaleWidth = $('[data-role="zoomwrapper"]').data('zoomwrapper').member._nScaledContentWidth;
			this.options.scaleHeight = $('[data-role="zoomwrapper"]').data('zoomwrapper').member._nScaledContentHeight;
			this.options.right = parseInt(this.options.scaleWidth) + parseInt($(this.element).parent().offset().left) - (onjCompProperty.width) * (this.options.scaleInc);
			this.options.bottom = parseInt(this.options.scaleHeight) + parseInt($(this.element).parent().offset().top) - (onjCompProperty.height) * (this.options.scaleInc);
			//console.log("right" , parseInt(this.options.scaleWidth) , parseInt($(this.element).parent().offset().left) , onjCompProperty.width);
			//console.log("bottom" , parseInt(this.options.scaleHeight) , parseInt($(this.element).parent().offset().top) , onjCompProperty.height);
*/
		},
		getRect : function() {
			var objThis = this, thisElement = this.element, thisMemVars = this.member;
			thisMemVars._x = Number(thisElement.offset().left);
			thisMemVars._y = Number(thisElement.offset().top);
			thisMemVars._width = Number(thisElement.width());
			thisMemVars._height = Number(thisElement.height());
		},
		// SET THE VARIABLE IN DOM ELEMENT FOR STORING UNIQUE INSTANCE
		setVar : function(strVar, obj) {
			$(this.element).data(strVar, obj);
		},
		// GET THE VARIABLE FROM DOM ELEMENT
		getVar : function(strVar) {
			return $(this.element).data(strVar);
		},
		onHandler : function(e) {

			$(this).trigger(this.events.CLICK, this.data);
		},
		onTapHold : function(e) {
			$(this).trigger('taphold', this.data);

		},
		setLayout : function() {
			var objLayoutOptions = this.element.jqmData('layout');
			try {
				var objLayout = new MG[objLayoutOptions.type]();
				objLayout.setOptions(objLayoutOptions);
				objLayout.setOwner(this.element);

				this.setVar("layout", objLayout);
			} catch(err) {
				//console.error("no layout is set");
			}
			//this.updateLayout();

		},
		getLayout : function() {
			return this.getVar("layout");
		},
		updateLayout : function() {
			/*try {
				var layout = this.getVar("layout");
				layout.updateLayout();
			} catch(err) {
				//console.error("object does not have any layout");
			}*/

		}
	})
})(jQuery);

