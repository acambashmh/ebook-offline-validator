/**
 * Author: Lakshay Ahuja
 * Date: 7/2/2013
 * Organisation: Magic Software Pvt Ltd
 */
var extraPagesAdded = 0;
var pageChangeToScrollView = false;
var currentPgOffset = 0;
var PageNavigationManager = {
    currentPageIndex : 0,
    currentPageBrkVal : 0,
    timeoutVariable : null,
    timeoutVariableForPageLoad : null,
    newPageIndex : 0
}

PageNavigationManager.addSwipeListner = function(objScrollElement, scrollViewCompRef) {
    var objThis = this;
    var objScrollviewComp = scrollViewCompRef;
    if (EPubConfig.swipeNavEnabled || isWinRT) {
    	
        $(objScrollviewComp.element).unbind("swipepageleft").bind("swipepageleft", function() {
        	
            var totalPagesOnCreation = scrollViewCompRef.member.totalPagesOnCreation;
            var lastPageLength = $("#PageContainer_" + GlobalModel.pageBrkValueArr[totalPagesOnCreation - 1]).length;
            var nIndex = GlobalModel.currentPageIndex;
            var stopNavigationWhilePgCreation = (nIndex >= (EPubConfig.totalPageContainers - 5)) && (lastPageLength == 0);
            // log("SWIPELEFT " + nIndex + "  " +stopNavigationWhilePgCreation);
            if (stopNavigationWhilePgCreation != true) {
                GlobalModel.isToAnimate = true;
                var iPageIndex = parseInt($($('[type=PageContainer]')[nIndex]).attr('pagesequence'));
                var totalPages = EPubConfig.totalPages;
                if ((EPubConfig.pageView == 'doublePage') && (EPubConfig.totalPages % 2 != 0)) {
                    totalPages = totalPages - 1;
                }
                if (((iPageIndex < (totalPages - 1)) && (EPubConfig.pageView == 'singlePage')) || (((iPageIndex + 2) <= (totalPages - 2)) && (EPubConfig.pageView == 'doublePage'))) {
                    if (EPubConfig.pageView == 'singlePage') {
                        nIndex++;
                    } else {
                        /* if (GlobalModel.originalPageView == 'singlePage') {
                         nIndex = nIndex + 1;
                         } else {*/
                        nIndex = nIndex + 2;
                        //}
                    }
                    if (nIndex) {
                        GlobalModel.currentPageIndex = nIndex;
                        objScrollviewComp.options.nCurrentPageIndex = nIndex;
                        var iIndex = $($('[type=PageContainer]')[nIndex]).attr('pagesequence');
                        objScrollviewComp.showPageAtIndex(iIndex);
                    }
                    if (nIndex) {
                        GlobalModel.currentPageIndex = nIndex;
                    }
                }
            }
        });

        $(objScrollviewComp.element).unbind("swipepageright").bind("swipepageright", function() {
            var firstPageLength = $("#PageContainer_" + GlobalModel.pageBrkValueArr[0]).length;
            var nIndex = GlobalModel.currentPageIndex;
            var stopNavigationWhilePgCreation = ((nIndex <= (5)) && firstPageLength == 0);
            if (stopNavigationWhilePgCreation != true) {
                // log("SWIPERIGHT " + nIndex + "  " +stopNavigationWhilePgCreation);
                GlobalModel.isToAnimate = true;
                var iPageIndex = parseInt($($('[type=PageContainer]')[nIndex]).attr('pagesequence'));
                if (iPageIndex > 0) {
                    if (EPubConfig.pageView == 'doublePage') {
                        if (GlobalModel.originalPageView == 'singlePage') {
                            nIndex = nIndex - 1;
                        } else {
                            nIndex = nIndex - 2;
                        }
                    } else {
                        nIndex--;
                    }
                    if (nIndex < 0) {
                        nIndex = 0;
                    }

                    if (nIndex >= 0) {
                        GlobalModel.currentPageIndex = nIndex;
                        objScrollviewComp.options.nCurrentPageIndex = nIndex;
                        var iIndex = $($('[type=PageContainer]')[nIndex]).attr('pagesequence');
                        objScrollviewComp.showPageAtIndex(iIndex);
                    }

                }
                if (nIndex >= 0) {
                    GlobalModel.currentPageIndex = nIndex;
                }
            }
        });
    }
}

PageNavigationManager.removeSwipeListner = function(objScrollElement) {
    $(document).unbind("swipepageleft");
    $(document).unbind("swipepageright");
    GlobalModel.isToAnimate = false;
}

PageNavigationManager.resetSinglePageContainerPos = function(nIndex, scrollViewCompRef, isResizing) {
    var objScrollviewComp = scrollViewCompRef;
    var objThis = this;
    if (EPubConfig.pageView == 'doublePage') {
        if (($($('[type="PageContainer"]')[nIndex]).attr('pagesequence') % 2 != 0)) {
            nIndex = nIndex - 1;
        }
        $('[type="PageContainer"]').css('visibility', 'hidden');
        $($('[type="PageContainer"]')[nIndex]).css('visibility', 'visible');
        $($('[type="PageContainer"]')[nIndex + 1]).css('visibility', 'visible');
        $($('[type="PageContainer"]')[nIndex]).css('border', 'none');
        if ((EPubConfig.TwoPageViewDividerLine_isAvailable == true) && (GlobalModel.originalPageView != 'singlePage')) {
            $($('[type="PageContainer"]')[nIndex]).css({
                'border-right' : '1px solid rgb(150, 149, 149)'
            });
            $($('[type="PageContainer"]')[nIndex + 1]).css({
                'border-left' : '1px solid rgb(150, 149, 149)'
            });
        }
    }
    if (EPubConfig.pageView == 'singlePage' || EPubConfig.pageView == 'doublePage') {
        var nLeftPos = $("#bookContentContainer2").offset().left - $($('[type="PageContainer"]')[nIndex]).offset().left;
        if (nIndex == 0) {
            nLeftPos = 0;
        }
        $("#bookContentContainer2").css({
            'transition' : 'left 0s',
            '-webkit-transition' : 'left 0s',
            '-moz-transition' : 'left 0s',
            '-o-transition' : 'left 0s',
            'left' : nLeftPos + 'px'
        });
        GlobalModel.pageLeftValue = nLeftPos;
        objScrollviewComp.options.nCurrentPageIndex = nIndex;
        GlobalModel.currentPageIndex = nIndex;
        $($('[type="PageContainer"]')[nIndex]).css('visibility', 'visible');
    }
    GlobalModel.currentPageIndex = nIndex;
    objThis.newPageIndex = nIndex;
    $(objScrollviewComp).trigger(objScrollviewComp.events.PAGE_INDEX_CHANGE, objScrollviewComp.data);
    changeNextPrevBtnState(parseInt($($('[type="PageContainer"]')[nIndex]).attr('pagesequence')));
}

PageNavigationManager.swipePage = function(nIndex, scrollViewCompRef, isResizing) {
    var objScrollviewComp = scrollViewCompRef;
    var objThis = this;
    if (objThis.timeoutVariableForPageLoad != null) {
        clearTimeout(objThis.timeoutVariableForPageLoad);
    }

    if (EPubConfig.pageView == 'doublePage') {
        if (($($('[type="PageContainer"]')[nIndex]).attr('pagesequence') % 2 != 0)) {
            nIndex = nIndex - 1;
        }
        if (nIndex < EPubConfig.totalPages - 1) {
            $($('[type="PageContainer"]')[nIndex]).css('visibility', 'visible');
            if ($($('[type="PageContainer"]')[nIndex + 1]) != undefined)
                $($('[type="PageContainer"]')[nIndex + 1]).css('visibility', 'visible');
            var element = document.getElementById("scrollMainContainer");
            var nLeftPos = $("#bookContentContainer2").offset().left - $($('[type="PageContainer"]')[nIndex]).offset().left;
            if (nIndex == 0) {
                nLeftPos = 0;
            }
            GlobalModel.pageLeftValue = nLeftPos;
            if (GlobalModel.isToAnimate) {
                $($('[type="PageContainer"]')[nIndex]).css({
                    'border-left' : '1px solid rgb(150, 149, 149)'
                });

                if ((EPubConfig.TwoPageViewDividerLine_isAvailable == true) && (GlobalModel.originalPageView != 'singlePage')) {
                    $($('[type="PageContainer"]')[nIndex]).css({
                        'border-right' : '1px solid rgb(150, 149, 149)'
                    });
                }
                if ($($('[type="PageContainer"]')[nIndex + 1]) != undefined) {
                    $($('[type="PageContainer"]')[nIndex + 1]).css({
                        'border-right' : '1px solid rgb(150, 149, 149)'
                    });

                    if ((EPubConfig.TwoPageViewDividerLine_isAvailable == true) && (GlobalModel.originalPageView != 'singlePage')) {
                        $($('[type="PageContainer"]')[nIndex + 1]).css({
                            'border-left' : '1px solid rgb(150, 149, 149)'
                        });
                    }
                }
                if (objThis.timeoutVariable != null) {
                    clearTimeout(objThis.timeoutVariable);
                }
                GlobalModel.isToAnimate = false;
                $("#bookContentContainer2").css({
                    'transition' : 'left 0.3s',
                    '-webkit-transition' : 'left 0.3s',
                    '-moz-transition' : 'left 0.3s',
                    '-o-transition' : 'left 0.3s',
                    'left' : nLeftPos + 'px'
                });
                objThis.timeoutVariableForPageLoad = setTimeout(function() {
                    if (nIndex != 0) {
                        objScrollviewComp.doPageLoading(nIndex);
                    } else {
                        objScrollviewComp.doPageLoading(nIndex - 1);
                    }
                    objScrollviewComp.doPageLoading(nIndex);
                }, 500);
                objThis.timeoutVariable = setTimeout(function() {
                    $($('[type="PageContainer"]')[nIndex]).css('border', 'none');
                    $($('[type="PageContainer"]')[nIndex + 1]).css('border', 'none');
                    if ((EPubConfig.TwoPageViewDividerLine_isAvailable == true) && (GlobalModel.originalPageView != 'singlePage')) {
                        $($('[type="PageContainer"]')[nIndex]).css({
                            'border-right' : '1px solid rgb(150, 149, 149)'
                        });
                        $($('[type="PageContainer"]')[nIndex + 1]).css({
                            'border-left' : '1px solid rgb(150, 149, 149)'
                        });
                    }
                    $($('[type="PageContainer"]')[nIndex - 1]).css('visibility', 'hidden');
                    $($('[type="PageContainer"]')[nIndex + 2]).css('visibility', 'hidden');
                }, 280);
            } else {
                $("#bookContentContainer2").css({
                    'transition' : 'left 0s',
                    '-webkit-transition' : 'left 0s',
                    '-moz-transition' : 'left 0s',
                    '-o-transition' : 'left 0s',
                    'left' : nLeftPos + 'px'
                });
                $($('[type="PageContainer"]')[nIndex]).css('border', 'none');
                if ((EPubConfig.TwoPageViewDividerLine_isAvailable == true) && (GlobalModel.originalPageView != 'singlePage')) {
                    $($('[type="PageContainer"]')[nIndex]).css({
                        'border-right' : '1px solid rgb(150, 149, 149)'
                    });
                    $($('[type="PageContainer"]')[nIndex + 1]).css({
                        'border-left' : '1px solid rgb(150, 149, 149)'
                    });
                }
                if (nIndex != 0) {
                    objScrollviewComp.doPageLoading(nIndex - 1);
                } else {
                    objScrollviewComp.doPageLoading(nIndex);
                }
                objScrollviewComp.doPageLoading(nIndex);
                //});

                $($('[type="PageContainer"]')[nIndex - 1]).css('visibility', 'hidden');
                $($('[type="PageContainer"]')[nIndex + 2]).css('visibility', 'hidden');
            }
            changeNextPrevBtnState(parseInt($($('[type="PageContainer"]')[nIndex]).attr('pagesequence')));
            GlobalModel.currentPageIndex = nIndex;
        }
    } else if (EPubConfig.pageView == 'singlePage') {
        var nLeftPos = $("#bookContentContainer2").offset().left - $($('[type="PageContainer"]')[nIndex]).offset().left;
        if (nIndex == 0) {
            nLeftPos = 0;
        }
        $($('[type="PageContainer"]')[nIndex]).css('visibility', 'visible');
        if (GlobalModel.isToAnimate) {
            $($('[type="PageContainer"]')[nIndex]).css({
                'border-left' : '1px solid rgb(150, 149, 149)',
                'border-right' : '1px solid rgb(150, 149, 149)'
            });
            if (objThis.timeoutVariable != null) {
                clearTimeout(objThis.timeoutVariable);
            }
            GlobalModel.isToAnimate = false;
            $("#bookContentContainer2").css({
                'transition' : 'left 0.3s',
                '-webkit-transition' : 'left 0.3s',
                '-moz-transition' : 'left 0.3s',
                '-o-transition' : 'left 0.3s',
                'left' : nLeftPos + 'px'
            });
            objThis.timeoutVariableForPageLoad = setTimeout(function() {
                objScrollviewComp.doPageLoading(nIndex);
            }, 500);
            objThis.timeoutVariable = setTimeout(function() {
                $($('[type="PageContainer"]')[nIndex]).css('border', 'none');
                $($('[type="PageContainer"]')[nIndex + 1]).css('border', 'none');
                $($('[type="PageContainer"]')[nIndex - 1]).css('visibility', 'hidden');
                $($('[type="PageContainer"]')[nIndex + 1]).css('visibility', 'hidden');
            }, 280);
        } else {
            $("#bookContentContainer2").css({
                'transition' : 'left 0s',
                '-webkit-transition' : 'left 0s',
                '-moz-transition' : 'left 0s',
                '-o-transition' : 'left 0s',
                'left' : nLeftPos + 'px'
            });
            objScrollviewComp.doPageLoading(nIndex);
            $($('[type="PageContainer"]')[nIndex - 1]).css('visibility', 'hidden');
            $($('[type="PageContainer"]')[nIndex + 1]).css('visibility', 'hidden');
        }
        GlobalModel.pageLeftValue = nLeftPos;
        objScrollviewComp.options.nCurrentPageIndex = nIndex;
        changeNextPrevBtnState(parseInt($($('[type="PageContainer"]')[nIndex]).attr('pagesequence')));
        GlobalModel.currentPageIndex = nIndex;
    }
    if (isResizing == null)
        $(objScrollviewComp).trigger(objScrollviewComp.events.PAGE_INDEX_CHANGE, objScrollviewComp.data);
    objThis.newPageIndex = GlobalModel.currentPageIndex;
}
changeNextPrevBtnState = function(iPageIndex) 
{
	var objThis = this;
	
    if (EPubConfig.pageView == 'doublePage') 
    {
        var totalPages = EPubConfig.totalPages;
        if (EPubConfig.totalPages % 2 != 0) 
        {
            totalPages = totalPages - 1;
        }

        if (iPageIndex == 0) 
        {
            PageNavigationManager.disableNextPreviousButtons ( "left" );
        } 
        else if (iPageIndex >= totalPages - 2) 
        {
            PageNavigationManager.disableNextPreviousButtons ( "right" );
        } 
        else 
        {
            PageNavigationManager.disableNextPreviousButtons ( "none" );
        }
    } 
    else 
    {
        if (iPageIndex == 0) 
        {
            PageNavigationManager.disableNextPreviousButtons ( "left" );
        } 
        else if (iPageIndex >= (EPubConfig.totalPages - 1)) 
        {
            PageNavigationManager.disableNextPreviousButtons ( "right" );
        } 
        else 
        {
            PageNavigationManager.disableNextPreviousButtons ( "none" );
        }
    }
}

PageNavigationManager.disableNextPreviousButtons = function ( type ) 
{
	var _left = $( "#prevBtnLeft" );
	var _right = $( "#nextBtnRight" );
	
	switch ( type )
	{
		case "left":
			_left.addClass('ui-disabled');
            _left.parent().removeClass('nextPreviousPageActive').addClass('nextPreviousPageInactive ui-disabled');
            _right.removeClass('ui-disabled');
            _right.parent().removeClass('nextPreviousPageInactive ui-disabled').addClass('nextPreviousPageActive');
		break;
		
		case "right":
			_left.removeClass('ui-disabled');
            _left.parent().removeClass('nextPreviousPageInactive ui-disabled').addClass('nextPreviousPageActive');
            _right.addClass('ui-disabled');
            _right.parent().removeClass('nextPreviousPageActive').addClass('nextPreviousPageInactive ui-disabled');
		break;
		
		case "none":
			_left.removeClass('ui-disabled');
            _left.parent().removeClass('nextPreviousPageInactive ui-disabled').addClass('nextPreviousPageActive');
            _right.removeClass('ui-disabled');
            _right.parent().removeClass('nextPreviousPageInactive ui-disabled').addClass('nextPreviousPageActive');
		break
	}
}

PageNavigationManager.adjustPageToScreen = function(nVal) {
    if (EPubConfig.pageView == 'doublePage' || EPubConfig.pageView == 'singlePage') {
        $("#bookContentContainer").attr('align', 'center');
        $("#innerScrollContainer").css('margin-left', 0);
    } else {
        /*$("#scrollMainContainer").css('margin-left', 0);*/
        $("#bookContentContainer").attr('align', 'left');
        if (EPubConfig.pageView.toLowerCase() == 'scroll') {
           setTimeout(function(){ var bookContentContainer2Width = $("#bookContentContainer2").width();
            var pageContainerWidth = EPubConfig.pageWidth * nVal;
            var calculatedLeft = (bookContentContainer2Width - pageContainerWidth) / 2;
            if (calculatedLeft < 0) {
                calculatedLeft = 0;
            }
            $("#innerScrollContainer").css('margin-left', calculatedLeft);}, 0 );
        }
    }
}

PageNavigationManager.manageDomPageElements = function(nIndex, scrollViewCompRef) {
    var objThis = this;
    objThis.currentPageIndex = nIndex;
    objThis.newPageIndex = nIndex;
    objThis.currentPageBrkVal = GlobalModel.pageBrkValueArr[nIndex];
    var firstPageLength = $("#PageContainer_" + GlobalModel.pageBrkValueArr[0]).length;
    var totalPagesOnCreation = scrollViewCompRef.member.totalPagesOnCreation;
    var lastPageLength = $("#PageContainer_" + GlobalModel.pageBrkValueArr[totalPagesOnCreation - 1]).length;
    var curPageSeq = $($('[type=PageContainer]')[nIndex]).attr('pageSequence');
    var pgIndexinGlobalModelArr;
    var currentPageindex;
    /**
     * If nIndex is between (0 - maxpages-1), then its pagesequence would be undefined, and curpage will exist.
     * check if index is of the last 5 pages or first 5 pages, and create new pages accordingly.
     */
    if (($($('[type=PageContainer]')[nIndex]).attr('pageSequence') != undefined) && (scrollViewCompRef.member.curPageExists == true)) {
        if (((nIndex >= (EPubConfig.totalPageContainers - 5)) && lastPageLength == 0) || ((nIndex <= (5)) && firstPageLength == 0)) {
            //pagebreakvalue
            var nPageNum = parseInt($($('[type=PageContainer]')[nIndex]).attr('pageSequence'));
            //objThis.stopNavigationWhilePgCreation = true;
            try {

                //if (EPubConfig.pageView.toLowerCase() == "scroll") {
                $('[type=PageContainer]').find('[id=pageNumberBelowBookMark]').css('display', 'none');
                //}

            } catch(e) {

            }
            objThis.newPageIndex = objThis.modifyPageContainerAttributes(nPageNum, scrollViewCompRef);
        }

    } else {
        /**
         * If the current page doen't exist, its index will not lie between 0 - (maxpages-1)
         * The index would be the actual position of the page in GlobalModel.pageBrkArr.
         * The desired pages will be created in  modifyPageContainerAttributes()
         */
        if (scrollViewCompRef.member.curPageExists == false) {
            //objThis.stopNavigationWhilePgCreation = true;
            try {

                //if (EPubConfig.pageView.toLowerCase() == "scroll") {
                $('[type=PageContainer]').find('[id=pageNumberBelowBookMark]').css('display', 'none');
                //}

            } catch(e) {

            }
            objThis.newPageIndex = objThis.modifyPageContainerAttributes(nIndex, scrollViewCompRef);
            scrollViewCompRef.member.curPageExists = true;
        }
    }

    isFrmScrollStop = false;
}
/**
 * This function will add or remove the blank page on switching to or from double_page view.
 */
PageNavigationManager.addRemoveStartingLeftPage = function(bVal, scrollViewCompRef) {
    var objThis = this;
    var strPage = "";
    var objScrollviewComp = scrollViewCompRef;
    var nPageHeight = objScrollviewComp.options.contentHeight;

    var currentPageSequence = parseInt($($(scrollViewCompRef.element).find('[type=PageContainer]')[GlobalModel.currentPageIndex]).attr('pagesequence'));
    if (bVal && (GlobalModel.pageBrkValueArr[0] != "start_blank")) {
        GlobalModel.currentPageIndex = parseInt(GlobalModel.currentPageIndex) + 1;
        GlobalModel.navPageArr.splice(0, 0, -1);
        GlobalModel.pageBrkValueArr.splice(0, 0, "start_blank");
        scrollViewCompRef.options.arrPageElements.splice(0, 0, "BlankPage");
        scrollViewCompRef.member.arrPageNames.splice(0, 0, "BlankPage");
        EPubConfig.totalPages = EPubConfig.totalPages + 1;
        extraPagesAdded = 1;
        if (EPubConfig.totalPages % 2 != 0) {
            extraPagesAdded = 2;
            GlobalModel.navPageArr.push(-1);
            GlobalModel.pageBrkValueArr.push("end_blank");
            scrollViewCompRef.options.arrPageElements.push("BlankPage");
            scrollViewCompRef.member.arrPageNames.push("BlankPage");
            EPubConfig.totalPages = EPubConfig.totalPages + 1;
        }
        objThis.newPageIndex = objThis.modifyPageContainerAttributes((currentPageSequence + 1), scrollViewCompRef);
        return false;

    } else if ((bVal == false) && (GlobalModel.pageBrkValueArr[0] == "start_blank")) {
        if (EPubConfig.pageView.toLowerCase() == "scroll") {
            pageChangeToScrollView = true;
        }
        if (parseInt(GlobalModel.currentPageIndex) != 0)
            GlobalModel.currentPageIndex = parseInt(GlobalModel.currentPageIndex) - 1;
        GlobalModel.navPageArr.splice(0, 1);
        GlobalModel.pageBrkValueArr.splice(0, 1);
        scrollViewCompRef.options.arrPageElements.splice(0, 1);
        scrollViewCompRef.member.arrPageNames.splice(0, 1);
        EPubConfig.totalPages = EPubConfig.totalPages - 1;
        if (extraPagesAdded > 1) {
            GlobalModel.navPageArr.splice((GlobalModel.navPageArr.length - 1), 1);
            GlobalModel.pageBrkValueArr.splice((GlobalModel.pageBrkValueArr.length - 1), 1);
            scrollViewCompRef.options.arrPageElements.splice((scrollViewCompRef.options.arrPageElements.length - 1), 1);
            scrollViewCompRef.member.arrPageNames.splice((scrollViewCompRef.member.arrPageNames.length - 1), 1);
            EPubConfig.totalPages = EPubConfig.totalPages - 1;
        }

        var GlobalPageBrkArrLenght = GlobalModel.pageBrkValueArr.length;
        if (currentPageSequence == 0) {
            currentPageSequence = currentPageSequence + 1;
        }
        objThis.newPageIndex = objThis.modifyPageContainerAttributes((currentPageSequence - 1), scrollViewCompRef);
        return true;
    }
}
/**
 * This function will modify the pages according to the nPageNum provided.
 */
PageNavigationManager.modifyPageContainerAttributes = function(nPageNum, scrollViewCompRef) {
    var objThis = this;
    try {
        if (isFrmScrollStop) {
            currentPgOffset = $($('[type=PageContainer]')[GlobalModel.currentPageIndex]).offset().top;
        } else {
            currentPgOffset = 0;
        }

    } catch(e) {
        currentPgOffset = 0;
    }

    $("#bookContentContainer2").stop(true, false);
    //log("modifyPageContainerAttributes");
    if (EPubConfig.pageView.toLowerCase() == "scroll") {
        PopupManager.addPopup($("#stopScrollFunctionality"), null, {
            isModal : true,
            isCentered : true,
            popupOverlayStyle : "popupMgrMaskStyleSemiTransparent",
            isTapLayoutDisabled : true
        });
    }
    var strPageName;
    var objPgNameReg;
    var pageSeq = 0;
    var totalPages = EPubConfig.totalPageContainers;
    var lowerLimit = (nPageNum - ((totalPages / 2) - 1) >= 0) ? (nPageNum - ((totalPages / 2) - 1)) : 0;
    var upperLimit = ((lowerLimit + totalPages) < EPubConfig.totalPages) ? (lowerLimit + totalPages) : EPubConfig.totalPages;
    if (upperLimit == EPubConfig.totalPages) {
        lowerLimit = upperLimit - totalPages;
    }
    if (lowerLimit < 0) {
        lowerLimit = 0;
    }
    if ((lowerLimit == 0) && !(EPubConfig.totalPages <= EPubConfig.totalPageContainers)) {
        upperLimit = totalPages;
    }
    var tempPgBrkArr = [];
    var currentPageindex;
    for (var k = (lowerLimit); k < (upperLimit); k++) {
        if (k == nPageNum) {
            currentPageindex = pageSeq;
            GlobalModel.currentPageIndex = pageSeq;
        }
        var pglinkListNumber = k;
        if ($('[type=PageContainer]')[pageSeq] && ($($('[type=PageContainer]')[pageSeq]).attr('pagebreakvalue') != "start_blank") && ($($('[type=PageContainer]')[pageSeq]).attr('pagebreakvalue') != "end_blank")) {
            $($('[type=PageContainer]')[pageSeq]).data('pagecomp').resetMe();
        }
        objPgNameReg = /.xhtml/gi;
        if (pglinkListNumber >= 0) {
            strPageName = scrollViewCompRef.options.arrPageLinkList[pglinkListNumber];
            try {
                strPageName = strPageName.replace(objPgNameReg, "");
            } catch(e) {

            }

        } else {
            strPageName = "";
        }

        var pageBrkValue = 0;
        var pageBreakId = "";
        pageBrkValue = GlobalModel.pageBrkValueArr[k];
        var nPageHeight = scrollViewCompRef.options.contentHeight;
        var nPagePos = k * scrollViewCompRef.options.contentHeight;
        var bookmarkText = pageBrkValue;
        if (bookmarkText.length > 8) {
            bookmarkText = bookmarkText.substring(0, 8);
        }
        $($('[type=PageContainer]')[pageSeq]).attr('id', "PageContainer_" + pageBrkValue);
        $($('[type=PageContainer]')[pageSeq]).attr('pagename', strPageName);
        $($('[type=PageContainer]')[pageSeq]).attr('pageBreakValue', pageBrkValue);
        $($('[type=PageContainer]')[pageSeq]).attr('pageSequence', k);
        scrollViewCompRef.member.curPageElementsPgBrk[pageSeq] = pageBrkValue;
        if (EPubConfig.RequiredApplicationComponents.indexOf(AppConst.BOOKMARK) > -1) {
            $($('[type=PageContainer]')[pageSeq]).find('[type="addRemoveBookmarkStrip"]').attr('id', "bookmarkStrip_" + pageBrkValue);
            $($('[type=PageContainer]')[pageSeq]).find('[type="addRemoveBookmarkStrip"]').attr('class', "btnBookmarkComp-off");
            try {
                $($('[type=PageContainer]')[pageSeq]).find('[type="addRemoveBookmarkStrip"]').data('BookmarkStripComp').options.selected = false;
            } catch(e) {

            }
            $($('[type=PageContainer]')[pageSeq]).find('.pageNumberBelowBookMark').html(bookmarkText);
            if (!EPubConfig.PageNumberInBookmark_isAvailable) {
                $($('[type=PageContainer]')[pageSeq]).find('.pageNumberBelowBookMark').css('visibility', 'hidden');
            }
            if (pageBrkValue == "start_blank" || pageBrkValue == "end_blank") {
                $($('[type=PageContainer]')[pageSeq]).find('[type="addRemoveBookmarkStrip"]').css('visibility', 'hidden');
            } else {
                $($('[type=PageContainer]')[pageSeq]).find('[type="addRemoveBookmarkStrip"]').css('visibility', 'visible');
            }
        }
        var objPgContainerPgnum = $($(scrollViewCompRef.element).find('[type=PageContainer]')[pageSeq]).attr('pageSequence');
        var objPgContainer = $(scrollViewCompRef.element).find('[type=PageContainer]')[pageSeq];
        scrollViewCompRef.options.arrPageElements[pageSeq] = objPgContainer;
        pageSeq = pageSeq + 1;
    }
    GlobalModel.currentPageIndex = currentPageindex;
    objThis.newPageIndex = currentPageindex;
    var firstPageLength = $("#PageContainer_" + GlobalModel.pageBrkValueArr[0]).length;
    if (EPubConfig.pageView == 'singlePage' || EPubConfig.pageView == 'doublePage') {
        setTimeout(function() {
            objThis.resetSinglePageContainerPos(currentPageindex, scrollViewCompRef);
            setTimeout(function() {
                if (EPubConfig.pageView == 'doublePage') {
                    if (currentPageindex != 0) {
                        scrollViewCompRef.doPageLoading(currentPageindex - 1);
                    } else {
                        scrollViewCompRef.doPageLoading(currentPageindex);
                    }
                }
                scrollViewCompRef.doPageLoading(currentPageindex);
            }, 500);
        }, 0);
        changeNextPrevBtnState(parseInt($($('[type="PageContainer"]')[currentPageindex]).attr('pagesequence')));
    } else {
        objThis.scrollElementToCurPage(currentPageindex, scrollViewCompRef);
    }
    objThis.updateBookmarks(scrollViewCompRef);
    if ((EPubConfig.totalPages <= EPubConfig.totalPageContainers)) {
        var totalPageLength = $('[type=PageContainer]').length;
        if ((EPubConfig.pageView != "doublePage")) {
            $($('[type=PageContainer]')[totalPageLength - 1]).css('display', 'none');
            if (extraPagesAdded > 1) {
                $($('[type=PageContainer]')[totalPageLength - 2]).css('display', 'none');
            }
        } else {
            $($('[type=PageContainer]')[totalPageLength - 1]).css('display', 'block');
            if (extraPagesAdded > 1) {
                $($('[type=PageContainer]')[totalPageLength - 2]).css('display', 'block');
            }
        }
    }
    //objThis.stopNavigationWhilePgCreation = false;
    objThis.newPageIndex = currentPageindex;
    return currentPageindex;
}
/**
 * This function will scroll the page to the index provided
 */
PageNavigationManager.scrollElementToCurPage = function(moveToIndex, scrollViewCompRef) {
    var nIndex = moveToIndex;
    var nTopPos = -1 * currentPgOffset + scrollViewCompRef.options.nScalePercent * (nIndex * (scrollViewCompRef.options.contentHeight + scrollViewCompRef.options.nPageGap + (2 * scrollViewCompRef.options.pagecompBorder) + (scrollViewCompRef.options.paddingTop))) - scrollViewCompRef.options.pagecompBorder;
    setTimeout(function() {
        $("#bookContentContainer2").scrollTop(nTopPos);
        setTimeout(function() {
            try {

                $('[type=PageContainer]').find('[id=pageNumberBelowBookMark]').css('display', 'block');

            } catch(e) {

            }
            PopupManager.removePopup();
        }, 100);
    }, 0);

    if (pageChangeToScrollView) {
        var iPageIndex = $($('[type=PageContainer]')[nIndex]).attr('pagesequence');
        scrollViewCompRef.showPageAtIndex(iPageIndex);
        pageChangeToScrollView = false;
        if (iPageIndex == 0) {
            setTimeout(function() {
                scrollViewCompRef.doPageLoading(iPageIndex);
            }, 100);
        }

    }
}
/**
 * This function updates the bookmarks after modifying the pages
 */
PageNavigationManager.updateBookmarks = function() {
    var i = 0;
    var bkMarkArrLength = GlobalModel.bookMarks.length;
    for ( i = 0; i < EPubConfig.totalPageContainers; i++) {
        var curPageContainer = $('[type=PageContainer]')[i];
        var curPageBkMarkVal = $(curPageContainer).attr('pagebreakvalue');
        var flag = -1;
        for (var k = 0; k < bkMarkArrLength; k++) {
            if (GlobalModel.bookMarks[k].id.replace('bookmarkStrip_', '') == curPageBkMarkVal) {
                flag = 0;
                break;
            }
        }
        if (flag == 0) {
            $(curPageContainer).find('[type="addRemoveBookmarkStrip"]').attr('class', "btnBookmarkComp-on");
            $(curPageContainer).find('[type="addRemoveBookmarkStrip"]').data('BookmarkStripComp').options.selected = true;
        }
        if (EPubConfig.pageView == 'singlePage' || EPubConfig.pageView == 'doublePage') {
            $(curPageContainer).find('[type="addRemoveBookmarkStrip"]').find('[id=pageNumberBelowBookMark]').css('display', 'block');
        }
    }
}
/**
 * This function is called once on launch. This function creates the initial pages.
 */
PageNavigationManager.addDomPageElements = function(scrollViewCompRef) {
    var nIndex = 0;
    if (EPubConfig.totalPages == EPubConfig.totalPageContainers) {
        EPubConfig.totalPageContainers = EPubConfig.totalPageContainers + 2;
    }
    var maxIndex = EPubConfig.totalPageContainers;
    var objScrollviewComp = scrollViewCompRef;
    var objThis = this;
    var iLength = ((maxIndex) < EPubConfig.totalPages) ? (maxIndex) : (EPubConfig.totalPages);
    var strPage = "";
    var strPageName;
    var objPgNameReg;
    var index = nIndex;
    var tempPgBrkArr = [];
    for (var i = index; i < iLength; i++) {
        objPgNameReg = /.xhtml/gi;
        strPageName = scrollViewCompRef.options.arrPageLinkList[i];
        try {
            strPageName = strPageName.replace(objPgNameReg, "");
        } catch(e) {

        }

        var pageBrkValue = 0;
        var pageBreakId = "";
        pageBrkValue = GlobalModel.pageBrkValueArr[i];
        var nPageHeight = scrollViewCompRef.options.contentHeight;
        var nPagePos = i * scrollViewCompRef.options.contentHeight;
        /**
         * Adding the pagecontainer divs according to the page_view
         */
        var bookmarkText = pageBrkValue;
        if (bookmarkText.length > 8) {
            bookmarkText = bookmarkText.substring(0, 8);
        }
        var pageSeq = i;
        tempPgBrkArr[i] = pageBrkValue;
        if (EPubConfig.pageView == 'singlePage' || EPubConfig.pageView == 'doublePage') {
        	var pageBorder = 0;
        	if(ismobile){
        		pageBorder = (1 + scrollViewCompRef.options.contentWidth * i);
        	}
        	else{
        		pageBorder = ((1 + scrollViewCompRef.options.contentWidth) * i);
        	}
            strPage += '<div pageSequence="' + pageSeq + '" id ="PageContainer_' + pageBrkValue +
            '" pagebreakid="' + pageBreakId + '" pagename="' + strPageName +
            '" hotkeygroupid="PageBody" type="PageContainer" pageBreakValue ="' + pageBrkValue +
            '" class="contentBody" pageNum="' + i + '" data-role="pagecomp" style="overflow:hidden;float:none;position:absolute; left: ' + 
            (pageBorder) + 'px; width:' + scrollViewCompRef.options.contentWidth + 'px; height:' +
            scrollViewCompRef.options.contentHeight + 'px; background-color:#FFFFFF; padding-top:' +
            scrollViewCompRef.options.paddingTop + 'px; visibility:visible;"><div id="pageContainer"/>'
            ///if bookmark feature has to be added;
            if (EPubConfig.RequiredApplicationComponents.indexOf(AppConst.BOOKMARK) > -1 && EPubConfig.PageNumberInBookmark_isAvailable) {
                strPage += '<div data-role = "BookmarkStripComp" class="btnBookmarkComp-off" id="bookmarkStrip_' + pageBrkValue + '" type="addRemoveBookmarkStrip" data-options=\'{"selected":false, "setEnabled":true, "offstyle":"btnBookmarkComp-off", "onstyle":"btnBookmarkComp-on"}\'><div id = "pageNumberBelowBookMark" class = "pageNumberBelowBookMark" > ' + bookmarkText + '</div></div>';
            } else if (!EPubConfig.PageNumberInBookmark_isAvailable) {
                strPage += '<div data-role = "BookmarkStripComp" class="btnBookmarkComp-off" id="bookmarkStrip_' + pageBrkValue + '" type="addRemoveBookmarkStrip" data-options=\'{"selected":false, "setEnabled":true, "offstyle":"btnBookmarkComp-off", "onstyle":"btnBookmarkComp-on"}\'><div id = "pageNumberBelowBookMark" class = "pageNumberBelowBookMark"  style="visibility:hidden"> ' + bookmarkText + '</div></div>';
            }
        } else {
            strPage += '<div pageSequence="' + pageSeq + '" id ="PageContainer_' + pageBrkValue + '" pagebreakid="' + pageBreakId + '" pagename="' + strPageName + '" hotkeygroupid="PageBody" type="PageContainer" pageBreakValue ="' + pageBrkValue + '" class="contentBody" pageNum="' + i + '" data-role="pagecomp" style="overflow:hidden;position:relative; border:1px solid rgb(150, 149, 149); width:' + scrollViewCompRef.options.contentWidth + 'px; height:' + nPageHeight + 'px; background-color:#FFFFFF; padding-top:' + scrollViewCompRef.options.paddingTop + 'px; margin-bottom:' + scrollViewCompRef.options.nPageGap + 'px;"><div id="pageContainer"/>'
            ///if bookmark feature has to be added;
            if (EPubConfig.RequiredApplicationComponents.indexOf(AppConst.BOOKMARK) > -1 && EPubConfig.PageNumberInBookmark_isAvailable) {
                strPage += '<div data-role = "BookmarkStripComp" class="btnBookmarkComp-off" id="bookmarkStrip_' + pageBrkValue + '" type="addRemoveBookmarkStrip" data-options=\'{"selected":false, "setEnabled":true, "offstyle":"btnBookmarkComp-off", "onstyle":"btnBookmarkComp-on"}\'><div id = "pageNumberBelowBookMark" class = "pageNumberBelowBookMark" > ' + bookmarkText + '</div></div>';
            } else if (!EPubConfig.PageNumberInBookmark_isAvailable) {
                strPage += '<div data-role = "BookmarkStripComp" class="btnBookmarkComp-off" id="bookmarkStrip_' + pageBrkValue + '" type="addRemoveBookmarkStrip" data-options=\'{"selected":false, "setEnabled":true, "offstyle":"btnBookmarkComp-off", "onstyle":"btnBookmarkComp-on"}\'><div id = "pageNumberBelowBookMark" class = "pageNumberBelowBookMark"  style="visibility:hidden"> ' + bookmarkText + '</div></div>';
            }
        }
        strPage += '</div>'
    }
    $("#innerScrollContainer").append(strPage);
    var pageLength = $(scrollViewCompRef.element).find('[type=PageContainer]').length;
    for (var k = 0; k < (pageLength); k++) {
        if ($($(scrollViewCompRef.element).find('[type=PageContainer]')[k]).data().pagecomp == undefined) {
            $($(scrollViewCompRef.element).find('[type=PageContainer]')[k]).pagecomp();
            var objPgContainerPgnum = $($(scrollViewCompRef.element).find('[type=PageContainer]')[k]).attr('pagenum');
            var objPgContainer = $(scrollViewCompRef.element).find('[type=PageContainer]')[k];
            scrollViewCompRef.options.arrPageElements[objPgContainerPgnum] = objPgContainer;
        }
    }
    scrollViewCompRef.options.totalPageCount = iLength;
    scrollViewCompRef.options.totalContentHeight = $("#innerScrollContainer").height();
    scrollViewCompRef.member.curPageElementsPgBrk = (tempPgBrkArr);

    if (EPubConfig.totalPages <= EPubConfig.totalPageContainers) {
        var extraPages = "";
        if (EPubConfig.pageView == 'singlePage' || EPubConfig.pageView == 'doublePage') {
            extraPages += '<div pageSequence="-1" id ="PageContainer_start_blank" pagebreakid="" pagename="" hotkeygroupid="PageBody" type="PageContainer" pageBreakValue ="start_blank" class="contentBody" pageNum="-1" data-role="pagecomp" style="float:none;position:absolute; left: ' + (1 + scrollViewCompRef.options.contentWidth * iLength) + 'px; width:' + scrollViewCompRef.options.contentWidth + 'px; height:' + scrollViewCompRef.options.contentHeight + 'px; background-color:#FFFFFF; padding-top:' + scrollViewCompRef.options.paddingTop + 'px; visibility:visible;"><div id="pageContainer"/>'
            ///if bookmark feature has to be added;
            if (EPubConfig.RequiredApplicationComponents.indexOf(AppConst.BOOKMARK) > -1) {
                extraPages += '<div data-role = "BookmarkStripComp" class="btnBookmarkComp-off" id="bookmarkStrip_Blank" type="addRemoveBookmarkStrip" data-options=\'{"selected":false, "setEnabled":true, "offstyle":"btnBookmarkComp-off", "onstyle":"btnBookmarkComp-on"}\'><div id = "pageNumberBelowBookMark" class = "pageNumberBelowBookMark" > Blank </div></div>';
            }
        } else {
            extraPages += '<div pageSequence="-1" id ="PageContainer_end_blank" pagebreakid="" pagename="" hotkeygroupid="PageBody" type="PageContainer" pageBreakValue ="end_blank" class="contentBody" pageNum="-1" data-role="pagecomp" style="position:relative; border:1px solid rgb(150, 149, 149); width:' + scrollViewCompRef.options.contentWidth + 'px; height:' + nPageHeight + 'px; background-color:#FFFFFF; padding-top:' + scrollViewCompRef.options.paddingTop + 'px; margin-bottom:' + scrollViewCompRef.options.nPageGap + 'px;"><div id="pageContainer"/>'
            ///if bookmark feature has to be added;
            if (EPubConfig.RequiredApplicationComponents.indexOf(AppConst.BOOKMARK) > -1) {
                extraPages += '<div data-role = "BookmarkStripComp" class="btnBookmarkComp-off" id="bookmarkStrip_Blank" type="addRemoveBookmarkStrip" data-options=\'{"selected":false, "setEnabled":true, "offstyle":"btnBookmarkComp-off", "onstyle":"btnBookmarkComp-on"}\'><div id = "pageNumberBelowBookMark" class = "pageNumberBelowBookMark" > Blank</div></div>';
            }
        }
        extraPages += '</div>'
        $("#innerScrollContainer").append(extraPages);
        extraPagesAdded = 1;

        var totalpageLength = $(scrollViewCompRef.element).find('[type=PageContainer]').length;
        $($(scrollViewCompRef.element).find('[type=PageContainer]')[iLength]).css('display', 'none');
        $($(scrollViewCompRef.element).find('[type=PageContainer]')[iLength]).pagecomp();
        if (totalpageLength % 2 != 0) {
            var endContainerExtraPage = $(extraPages).clone();
            //console.log("endContainerExtraPage: ",$(endContainerExtraPage));
            $("#innerScrollContainer").append(endContainerExtraPage);
            $(endContainerExtraPage).css('left', ((scrollViewCompRef.options.contentWidth+ 1) * (iLength)));
            extraPagesAdded = 2;
            $($(scrollViewCompRef.element).find('[type=PageContainer]')[iLength + 1]).css('display', 'none');
            $($(scrollViewCompRef.element).find('[type=PageContainer]')[iLength + 1]).pagecomp();
        }
    }

    if (EPubConfig.pageView == 'doublePage') {
        objThis.addRemoveStartingLeftPage(true, scrollViewCompRef);
    }
}