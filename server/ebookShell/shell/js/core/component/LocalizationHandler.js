/**
 * @author Harpreet S. Saluja
 * @description This JS sets the localization value (text) in the respective element
 */

(function($, undefined) {
	$.widget("magic.localizationHandler", $.magic.magicwidget, {
		options : {

		},

		_create : function() {
			$.extend(this.options, this.element.jqmData('options'));

			//if localization ID is missing, stop further processing
			if (this.options.localizationID == null)
				return;
			
			var strLocalizedData = GlobalModel.localizationData[this.options.localizationID];
			
			var objReg = /src='/g;
			strLocalizedData = strLocalizedData.replace(objReg, "src='" + strReaderPath);
			
			objReg = /src="/g;
			strLocalizedData = strLocalizedData.replace(objReg, 'src="' + strReaderPath);
			
			//if required value is not available, stop further processing
			if (strLocalizedData == null)
				return;

			//if all required values are available, then setting the localized data in respective element.
			$(this.element).html(strLocalizedData)
		},

		_init : function() {
			$.magic.magicwidget.prototype._init.call(this);
		},
	});
})(jQuery); 