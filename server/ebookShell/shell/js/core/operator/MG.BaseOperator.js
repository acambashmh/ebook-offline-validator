/**
 * @author Anand Kumar
 */

MG.BaseOperator = function() {
	this.objOperatorManager = null;
	this.arrCompsList = [];
	this.id = null;
}

MG.BaseOperator.prototype.initialize = function(objOperatorManager){
	this.objOperatorManager = objOperatorManager;
}

MG.BaseOperator.prototype.setId = function(strId){
	this.id = strId;
}

MG.BaseOperator.prototype.getId = function(){
	return this.id;
}

MG.BaseOperator.prototype.attachComponent = function(objComp) {
	
	var strType = objComp.element.attr(AppConst.ATTR_TYPE).toString();
	var compArr = [];
	if(this.arrCompsList[strType] != undefined) {
		compArr = this.arrCompsList[strType];
	}
	compArr.push(objComp);
	this.arrCompsList[strType] = compArr;

	return true;
}



/**
 * This function handles the function calls for components.
 */
MG.BaseOperator.prototype.doFunctionCall = function(strType, strFunction){
	var objThis = this,
	thirdArgs = arguments[2],
	fourthArgs = arguments[3],
	returnObj = null;
	arrComp = objThis.getCompArray(strType);
	
	if(arrComp!=undefined){
		$.each(arrComp, function(index, objComp){
			if(objComp){
				returnObj = objComp[strFunction](thirdArgs, fourthArgs);
			}
		});
		return returnObj;
	}
}

MG.BaseOperator.prototype.getCompArray = function(strType){
	return this.arrCompsList[strType];
}

MG.BaseOperator.prototype.destroy = function(){
}
