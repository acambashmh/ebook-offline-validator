var objPathManager = null;
function getPathManager()
{
	if(objPathManager == null)
	{
		objPathManager = new MG.PathManager();
	}
	return objPathManager;
}
/**
 * @author arvind.arora
 */

/**
 * Constructor
 */
MG.PathManager = function()
{
	this.blnUseAbsolutePath = false;
	this.strMediaDomainPath = "";
	this.strContentDomainPath = "";
	this.strMediaFolder = "";
	this.strContentFolder = "";
	//
}

MG.PathManager.prototype.replaceMediaURLs = function(strResponseText)
{
	var strOriginalText = strResponseText;
	
	var objReg = /\<img [A-Za-z0-9.\"=]* src=\"[0-9]*\"/g;
	
	var arrMatchedPatterns = new Array();
	arrMatchedPatterns = strResponseText.match(objReg);
	
	if (arrMatchedPatterns) {
		if (arrMatchedPatterns.length > 0) {
			for (var i = 0; i < arrMatchedPatterns.length; i++) {
				strResponseText = strResponseText.replace(arrMatchedPatterns[i], this.getConvertedValue(arrMatchedPatterns[i]))
			}
		}
	}
	return strResponseText;
}

MG.PathManager.prototype.getConvertedValue = function(strText)
{
	var arrSplit = strText.split('src="');
	var strId = arrSplit[1];
	strId = strId.substr(0, strId.indexOf('"'));
	strText = arrSplit[0]+'src="' +this.getFilePath(strId)+'"';
	return strText;
}

MG.PathManager.prototype.getFilePath = function(strFileUrl,isMediaFile)
{
	if(isNaN(strFileUrl) == false && $.trim(strFileUrl) != "")
	{
		strFileUrl = this.getMediaURLFromConfig(strFileUrl);
	}
	
	// to parse as per new requirement
	return this.appendReaderPath(strFileUrl);
}

MG.PathManager.prototype.appendReaderPath = function(strFileUrl)
{
	return strReaderPath + strFileUrl;
}

MG.PathManager.prototype.getLayoutFilePath = function()
{
    var strFileUrl = "content/html/player.html";
	var objReg = /\.html/gi;
	var strNewPath = ThemeManager.getLayoutType()+".html";
	strFileUrl = strFileUrl.replace(objReg, strNewPath);

    if(strReaderPath == "")
        return strFileUrl;

	return strReaderPath+strFileUrl;
}

MG.PathManager.prototype.getReaderSettingsFilePath = function()
{
    var strFileUrl = "content/settings.txt";
	//var objReg = /\.html/gi;
	//var strNewPath = ThemeManager.getLayoutType()+".html";
	//strFileUrl = strFileUrl.replace(objReg, strNewPath);

    if(strReaderPath == "")
        return strFileUrl;

	return strReaderPath+strFileUrl;
}

MG.PathManager.prototype.getMediaURLFromConfig = function(strMediaID)
{
	var strMediaURL;// = OSSProductSpec.getMediaURL(strMediaID);
    var LocalPathConfig = {};
    LocalPathConfig.getFilePath = function()
    {
        return "";
    }
	if(LocalPathConfig)
	{
		strMediaURL = LocalPathConfig.getFilePath(strMediaID);
	}
	else
	{
		strMediaURL = OSSProductSpec.getMediaURL(strMediaID);
	}
	
	// to parse as per new requirement
	return strMediaURL;
}

/**
* Getter/setter set whether to use a absolute path or not.  
* @param blnTrue
* 
*/		
MG.PathManager.prototype.setUseAbsolutePath = function(blnTrue)
{
	this.blnUseAbsolutePath = blnTrue
}

MG.PathManager.prototype.getUseAbsolutePath = function()
{
	return this.blnUseAbsolutePath;
}
/**
 * set the media domain e.g. http://mediadomain/path" 
 * @param strPath
 * 
 */
MG.PathManager.prototype.setMediaDomainPath = function(strPath)
{
	this.strMediaDomainPath = strPath;
}

MG.PathManager.prototype.getMediaDomainPath = function()
{
	return this.strMediaDomainPath;
}
/**
 * Getter/Setter for the content domain paht e.g. http://content/domin 
 * @return 
 * 
 */		
MG.PathManager.prototype.getContentDomainPath = function()
{
	return this.strContentDomainPath;
}
MG.PathManager.prototype.setContentDomainPath = function(strPath)
{
	this.strContentDomainPath = strPath;
}
/**
 * Getter/setter to specify the media folder. Default is "media"
 *  
 * @param strPath
 * 
 */		
MG.PathManager.prototype.setMediaFolder = function(strPath)
{
	this.strMediaFolder = strPath;
}
MG.PathManager.prototype.getMediaFolder = function()
{
	return this.strMediaFolder;	
}	
/**
 * Getter/setter to specify the content folder name. Default is "content".
 *  
 * @return 
 * 
 */		
MG.PathManager.prototype.getContentFolder = function()
{
	return this.strContentFolder;
}
		
MG.PathManager.prototype.setContentFolder = function(strPath)
{
	this.strContentFolder = strPath;
}

MG.PathManager.prototype.getEPubPagePath = function(strPageName)
{
	var strPath = EPubConfig.baseContentPath + "/" + EPubConfig.bookTitle + "/" + EPubConfig.contentFolderName + "/" + strPageName;
	
	if(strReaderPath == "")
		return strPath;
	
	return EPubConfig.contentFolderName + "/" + strPageName;
}

/**
 * This function returns the SMIL file path corresponding to the given page 
 * @param {String} strPageName
 * @return {String} SMIL file path
 */
MG.PathManager.prototype.getEPubPageSMILPath = function(strPageName)
{
	//replace the extension
	var objExtensionRegExp = /[0-9a-z]+$/i;
	var strPageSMILName = strPageName.replace(objExtensionRegExp, "smil")
	
	var strPath = EPubConfig.baseContentPath + "/" + EPubConfig.bookTitle + "/" + EPubConfig.contentFolderName + "/" + EPubConfig.smilFolderName + "/" + strPageSMILName;
	
	if(strReaderPath == "")
		return strPath;
	
	return EPubConfig.contentFolderName + "/" + EPubConfig.smilFolderName + "/" + strPageSMILName;
}

/**
 * This function returns the audio file path
 * @param {String} strSrc
 * @return {String} SMIL audio file path
 */
MG.PathManager.prototype.getSMILAudioPath = function(strSrc)
{
	var strPath = EPubConfig.baseContentPath + "/" + EPubConfig.bookTitle + "/" + EPubConfig.contentFolderName + "/" + EPubConfig.smilFolderName + "/" + strSrc;
	
	if(strReaderPath == "")
		return strPath;
	
	return EPubConfig.contentFolderName + "/" + EPubConfig.smilFolderName + "/" + strSrc;
}

/**
 * This function returns the SMIL file path corresponding to the given page 
 * @param {String} strPageName
 * @return {String} SMIL file path
 */
MG.PathManager.prototype.getOverlaySMILPath = function(strPageSMILName)
{
	var strPath = EPubConfig.baseContentPath + "/" + EPubConfig.bookTitle + "/" + EPubConfig.contentFolderName + "/" + strPageSMILName;
	
	if(strReaderPath == "")
		return strPath;
	
	return EPubConfig.contentFolderName + "/" + strPageSMILName;
}

MG.PathManager.prototype.getHomeBtnPath = function()
{
	if (_objQueryParams.homeurl) {
		return EPubConfig.HomeButtonURL;
	} else {
		var href = window.location.href.split('/');
		return href[0] + '//' + href[2] + '/';
	}
}

/**
 * This function returns the SMIL file path corresponding to the given page 
 * @param {String} strPageName
 * @return {String} SMIL file path
 */
MG.PathManager.prototype.getEPubPageSlowSMILPath = function(strPageName)
{
	//replace the extension
	var objExtensionRegExp = /[0-9a-z]+$/i;
	var strPageSMILName = strPageName.replace(objExtensionRegExp, "smil")
	
	var strPath = EPubConfig.baseContentPath + "/" + EPubConfig.bookTitle + "/" + EPubConfig.contentFolderName + "/" + EPubConfig.smilFolderName + "/" + strPageSMILName;
	
	if(strReaderPath == "")
		return strPath;
	
	return EPubConfig.contentFolderName + "/" + EPubConfig.smilFolderName + "/" + strPageSMILName;
}

/**
 * This function returns the audio file path
 * @param {String} strSrc
 * @return {String} SMIL audio file path
 */
MG.PathManager.prototype.getSlowSMILAudioPath = function(strSrc)
{
	var strPath = EPubConfig.baseContentPath + "/" + EPubConfig.bookTitle + "/" + EPubConfig.contentFolderName + "/" + EPubConfig.smilFolderName + "/" + strSrc;
	
	if(strReaderPath == "")
		return strPath;
	
	return EPubConfig.contentFolderName + "/" + EPubConfig.smilFolderName + "/" + strSrc;
}

MG.PathManager.prototype.getEPubImagePath = function()
{
	var strPath = EPubConfig.baseContentPath + "/" + EPubConfig.bookTitle + "/" + EPubConfig.contentFolderName + "/" + EPubConfig.imageFolderName + "/";
	
	if(strReaderPath == "")
		return strPath;
	
	return EPubConfig.contentFolderName + "/" + EPubConfig.imageFolderName + "/";
}
MG.PathManager.prototype.getEPubVideoPath = function()
{
	var strPath = EPubConfig.baseContentPath + "/" + EPubConfig.bookTitle + "/" + EPubConfig.contentFolderName + "/" + EPubConfig.videoFolderName + "/";
	
	if(strReaderPath == "")
		return strPath;
	
	return EPubConfig.contentFolderName + "/" + EPubConfig.videoFolderName + "/";
}

MG.PathManager.prototype.getEPubCSSPath = function(strCSSFileName)
{
	var strPath = EPubConfig.baseContentPath + "/" + EPubConfig.bookTitle + "/" + EPubConfig.contentFolderName + "/" + EPubConfig.cssFolderName + "/" + strCSSFileName;
	
	if(strReaderPath == "")
		return strPath;
	
	return EPubConfig.contentFolderName + "/" + EPubConfig.cssFolderName + "/" + strCSSFileName;
}

MG.PathManager.prototype.getFontsPath = function()
{
	var strPath = EPubConfig.baseContentPath + "/" + EPubConfig.bookTitle + "/" + EPubConfig.contentFolderName + "/" + EPubConfig.fontsFolderName;
	
	if(strReaderPath == "")
		return strPath;
	
	return EPubConfig.contentFolderName + "/" + EPubConfig.fontsFolderName;
}

MG.PathManager.prototype.getTOCPath = function(strFileName)
{
    //alert(EPubConfig.baseContentPath);
	var strPath = EPubConfig.baseContentPath + "/" + EPubConfig.bookTitle + "/" + EPubConfig.contentFolderName + "/" + EPubConfig.tocFileName + "?" + $.now();
	
	if(strReaderPath == "")
		return strPath;
	
	return EPubConfig.contentFolderName + "/toc.xhtml?" + $.now();
}

MG.PathManager.prototype.getEPubGlossaryPath = function()
{
	var strPath = EPubConfig.baseContentPath + "/" + EPubConfig.bookTitle + "/" + EPubConfig.contentFolderName + "/"+ EPubConfig.glossaryXMLPath;
	
	if(strReaderPath == "")
		return strPath;
	
	return EPubConfig.contentFolderName + "/"+ EPubConfig.glossaryXMLPath;
}

MG.PathManager.prototype.getEPubJSPath = function(strPath)
{
	if(strReaderPath == "")
	{
		var strPath = EPubConfig.baseContentPath + "/" + EPubConfig.bookTitle + "/" + EPubConfig.contentFolderName + "/" + strPath;
		return strPath;
	}
	
	return EPubConfig.contentFolderName + "/" + strPath;
}

MG.PathManager.prototype.getEPubAudioPath = function(strPath)
{
	//replace the extension
	var objExtensionRegExp = /[0-9a-z]+$/i;
	strPath = strPath.replace(objExtensionRegExp, "mp3")
	
	if(strReaderPath == "")
	{
		var strPath = EPubConfig.baseContentPath + "/" + EPubConfig.bookTitle + "/" + EPubConfig.contentFolderName + "/"+ EPubConfig.audioFolderName +"/" + strPath;
		return strPath;
	}
	
	return EPubConfig.contentFolderName + "/"+ EPubConfig.audioFolderName +"/" + strPath;
}

MG.PathManager.prototype.getEPubPageLevelAudioPath = function(strPath)
{
	//replace the extension
	var objExtensionRegExp = /[0-9a-z]+$/i;
	strPath = strPath.replace(objExtensionRegExp, "mp3")
	
	if(strReaderPath == "")
	{
		var strPath = EPubConfig.baseContentPath + "/" + EPubConfig.bookTitle + "/" + EPubConfig.contentFolderName + "/" + strPath;
		return strPath;
	}
	
	return EPubConfig.contentFolderName + "/" + strPath;
}

MG.PathManager.prototype.getEPubOGGAudioPath = function(strPath)
{
	var objReg = /mp3/gi;
	strPath = strPath.replace(objReg, "ogg");
	
	if(strReaderPath == "")
	{
		var strPath = EPubConfig.baseContentPath + "/" + EPubConfig.bookTitle + "/" + EPubConfig.contentFolderName + "/"+ EPubConfig.audioFolderName +"/" + strPath;
		return strPath;
	}
	
	return EPubConfig.contentFolderName + "/"+ EPubConfig.audioFolderName +"/" + strPath;
}

/**
 * This function returns the Global Settings file path available in ePub;
 * @param none;
 * @return {String} Global Settings file path
 */
MG.PathManager.prototype.getEPubGlobalSettingsPath = function()
{
	var strPath = EPubConfig.baseContentPath + "/" + EPubConfig.bookTitle + "/" + EPubConfig.contentFolderName + "/" + EPubConfig.ePubGlobalSettingsFile;
	
	if(strReaderPath == "")
		return strPath;
	
	return EPubConfig.contentFolderName + "/" + EPubConfig.ePubGlobalSettingsFile;
}

/**
 * This function returns the Content.opf file path available in ePub;
 * @param none;
 * @return {String} Content.opf file path
 */

MG.PathManager.prototype.getEPubContentOpfFilePath = function()
{
    var strPath = EPubConfig.baseContentPath + "/" + EPubConfig.bookTitle + "/" + EPubConfig.contentFolderName + "/" + EPubConfig.ePubContentOpfFile;
    
    if(strReaderPath == "")
        return strPath;
    
    return EPubConfig.contentFolderName + "/" + EPubConfig.ePubContentOpfFile;
}

MG.PathManager.prototype.getIndexPath = function(strFileName)
{
    var strPath = EPubConfig.baseContentPath + "/" + EPubConfig.bookTitle + "/" + EPubConfig.contentFolderName + "/" + strFileName;
    
	if(strReaderPath == "")
		return strPath;
	
	return EPubConfig.contentFolderName + "/" + strFileName;
}
MG.PathManager.prototype.getIntractivitiesXMLPath = function(strFileName)
{
    var strPath = EPubConfig.baseContentPath + "/" + EPubConfig.bookTitle + "/" + EPubConfig.contentFolderName + "/" + EPubConfig.intractivityPath + "/" + strFileName + ".xml";
    
	if(strReaderPath == "")
		return strPath;
	
	return EPubConfig.contentFolderName + "/" + EPubConfig.intractivityPath + "/" + strFileName + ".xml";
}
MG.PathManager.prototype.getIntractivitiesXMLBasePath = function()
{
    var strPath = EPubConfig.baseContentPath + "/" + EPubConfig.bookTitle + "/" + EPubConfig.contentFolderName + "/" + EPubConfig.intractivityPath;
    
	if(strReaderPath == "")
		return strPath;
	
	return EPubConfig.contentFolderName + "/" + EPubConfig.intractivityPath;
}

MG.PathManager.prototype.getStandardsPath = function(strFileName)
{
    var strPath = EPubConfig.baseContentPath + "/" + EPubConfig.bookTitle + "/" + EPubConfig.contentFolderName + "/" + strFileName;
    
	if(strReaderPath == "")
		return strPath;
	
	return EPubConfig.contentFolderName + "/" + strFileName;

}

MG.PathManager.prototype.getProgramBrandingImagePath = function (strFileName)
{
	//var strPath = EPubConfig.baseContentPath + "/" + EPubConfig.bookTitle + "/" + EPubConfig.contentFolderName + "/" + strFileName;
    var strPath = EPubConfig.baseContentPath + "/" + EPubConfig.bookTitle + "/" + EPubConfig.contentFolderName + "/" + EPubConfig.imageFolderName + "/" + strFileName;
	//alert(strReaderPath);
	if(strReaderPath == "")
		return strPath;
	
	return EPubConfig.contentFolderName + "/" + EPubConfig.imageFolderName + "/" + strFileName;
}


MG.PathManager.prototype.getHelpPath = function()
{
	//if(strReaderPath == "")
		return EPubConfig.Help_URL;
		
	//return strReaderPath + EPubConfig.Help_URL;
}

MG.PathManager.prototype.getQAPanelQuestionImagePath = function(strFileName)
{
	var strPath = EPubConfig.baseContentPath + "/" + EPubConfig.bookTitle + "/" + EPubConfig.contentFolderName + "/" + EPubConfig.imageFolderName + "/" + strFileName;
	//alert(strReaderPath);
	if(strReaderPath == "")
		return strPath;
	
	return EPubConfig.contentFolderName + "/" + EPubConfig.imageFolderName + "/" + strFileName;
}

MG.PathManager.prototype.getEPubResourcesPath = function(objPageData)
{
	var strPath = "";
	
	
	if(EPubConfig.LocalResource_isAvailable == false)
	{
		
		//GlobalModel.ePubGlobalSettings.Page[iPageIndex]["L5"];
		//console.log(objPageData);
		var iPageIndex = $($('[type=PageContainer]')[GlobalModel.currentPageIndex]).attr('pagesequence');
		//var objPageData = GlobalModel.pageDataBySequence["pagesequence_" + iPageIndex];
		
		var strLevel = GlobalModel.ePubGlobalSettings.Page[iPageIndex][EPubConfig.TOCStartLevel];
		strLevel = parseInt(strLevel) - parseInt(EPubConfig.MDSStartLevel);
		var strLessonStr = "";//+" + EPubConfig.LevelIdentifier_L5.toLowerCase() + ":" + GlobalModel.ePubGlobalSettings.Page[GlobalModel.currentPageIndex]["L5"];
		
		var strLevelIdentifierL0 = "";
		var strLevelIdentifierL1 = "";
		var strLevelIdentifierL2 = "";
		var strLevelIdentifierL3 = "";
		var strLevelIdentifierL4 = "";
		var strLevelIdentifierL5 = "";
		var strLevelIdentifierL6 = "";
		var strFilterString = "";
		var strSEFacing = "+se_facing:true+freeplay:true";
		var strMDSProgramName = "";
		
		if(EPubConfig.MDSProgramName != undefined && EPubConfig.MDSProgramName != "")
		{
			strMDSProgramName = 'program:"'+EPubConfig.MDSProgramName+'"+';
		}
		
		var nIndentifierVal = 0;
		if(EPubConfig.legacyTOC == false && EPubConfig.MDSVersion >= 3.2)
		{
			strFilterString = "";
			
			for (var str in objPageData.mdsData)
			{
				if(EPubConfig["LevelIdentifier_" + str])
				{
					strFilterString += EPubConfig["LevelIdentifier_" + str].toLowerCase() + ":" + objPageData.mdsData[str] + "+";
				}
				else
				{
					strFilterString += str.toLowerCase() + ":" + objPageData.mdsData[str] + "+";
				}
			}
			
			strFilterString += 'resources_panel_' + ((EPubConfig.Component_type == AppConst.TEACHER_EDITION) ? AppConst.TEACHER_EDITION.toLowerCase() : AppConst.STUDENT_EDITION.toLowerCase()) +':true';
			
			strPath = EPubConfig.Resources_Service_URL + "?q=(" + strMDSProgramName + strFilterString + ")&collection=Resources&options=search-ebook-json&format=json";
			
			return strPath;
		}
		//console.log(objPageData);
		if(EPubConfig.L0 == true || EPubConfig.L0 == "true")
		{
			strLevelIdentifierL0 = EPubConfig.LevelIdentifier_L0.toLowerCase() + ":" + objPageData.L0 + "+";
		}
		
		if(EPubConfig.L1 == true || EPubConfig.L1 == "true")
		{
			var nL1Offset = 0;
			if(EPubConfig.TOCStartLevel == "L2")
			{
				if(parseInt(EPubConfig.MDSStartLevel) >= 0)
				{
					nL1Offset = parseInt(EPubConfig.MDSStartLevel);
				}
			}
			if(parseInt(EPubConfig.L1_Offset) >= 0)
			{
				nL1Offset = EPubConfig.L1_Offset;//(EPubConfig.L2_Offset) ? EPubConfig.L2_Offset : 0;
			}
			
			nIndentifierVal = parseInt(objPageData.L1) - parseInt(nL1Offset);
			if (nIndentifierVal > 0) {
				strLevelIdentifierL1 = EPubConfig.LevelIdentifier_L1.toLowerCase() + ":" + nIndentifierVal;
				strFilterString += strLevelIdentifierL1;
			}
		}
		
		if(EPubConfig.L2 == true || EPubConfig.L2 == "true")
		{
			var nL2Offset = 0;
			if(EPubConfig.TOCStartLevel == "L2")
			{
				if(parseInt(EPubConfig.MDSStartLevel) >= 0)
				{
					nL2Offset = parseInt(EPubConfig.MDSStartLevel);
				}
			}
			if(parseInt(EPubConfig.L2_Offset) >= 0)
			{
				nL2Offset = EPubConfig.L2_Offset;//(EPubConfig.L2_Offset) ? EPubConfig.L2_Offset : 0;
			}
			
			nIndentifierVal = parseInt(objPageData.L2) - parseInt(nL2Offset);
			if (nIndentifierVal > 0) {
				strLevelIdentifierL2 = ((strFilterString != "") ? "+" : "") + EPubConfig.LevelIdentifier_L2.toLowerCase() + ":" + nIndentifierVal;
				strFilterString += strLevelIdentifierL2;
			}
		}
		
		
		if(EPubConfig.L3 == true || EPubConfig.L3 == "true")
		{
			var nL3Offset = 0;
			if(EPubConfig.TOCStartLevel == "L3")
			{
				if(parseInt(EPubConfig.MDSStartLevel) >= 0)
				{
					nL3Offset = parseInt(EPubConfig.MDSStartLevel);
				}
			}
			if(parseInt(EPubConfig.L3_Offset) >= 0)
			{
				nL3Offset = EPubConfig.L3_Offset;//(EPubConfig.L2_Offset) ? EPubConfig.L2_Offset : 0;
			}
			
			nIndentifierVal = parseInt(objPageData.L3) - parseInt(nL3Offset);
			if (nIndentifierVal > 0) {
				strLevelIdentifierL3 = ((strFilterString != "") ? "+" : "") + EPubConfig.LevelIdentifier_L3.toLowerCase() + ":" + nIndentifierVal;
				strFilterString += strLevelIdentifierL3;
			}
		}
		if(EPubConfig.L4 == true || EPubConfig.L4 == "true")
		{
			var nL4Offset = 0;
			if(EPubConfig.TOCStartLevel == "L4")
			{
				if(parseInt(EPubConfig.MDSStartLevel) >= 0)
				{
					nL4Offset = parseInt(EPubConfig.MDSStartLevel);
				}
			}
			if(parseInt(EPubConfig.L4_Offset) >= 0)
			{
				nL4Offset = EPubConfig.L4_Offset;//(EPubConfig.L2_Offset) ? EPubConfig.L2_Offset : 0;
			}
			
			nIndentifierVal = parseInt(objPageData.L4) - parseInt(nL4Offset);
			if (nIndentifierVal > 0) {
				strLevelIdentifierL4 = ((strFilterString != "") ? "+" : "") + EPubConfig.LevelIdentifier_L4.toLowerCase() + ":" + nIndentifierVal;
				strFilterString += strLevelIdentifierL4;
			}
		}
		if(EPubConfig.L5 == true || EPubConfig.L5 == "true")
		{
			var nL5Offset = 0;
			if(EPubConfig.TOCStartLevel == "L5")
			{
				if(parseInt(EPubConfig.MDSStartLevel) >= 0)
				{
					nL5Offset = parseInt(EPubConfig.MDSStartLevel);
				}
			}
			
			if(parseInt(EPubConfig.L5_Offset) >= 0)
			{
				nL5Offset = EPubConfig.L5_Offset;//(EPubConfig.L2_Offset) ? EPubConfig.L2_Offset : 0;
			}
			
			nIndentifierVal = parseInt(objPageData.L5) - parseInt(nL5Offset);
			if (nIndentifierVal > 0) {
				strLevelIdentifierL5 = ((strFilterString != "") ? "+" : "") + EPubConfig.LevelIdentifier_L5.toLowerCase() + ":" + nIndentifierVal;
				strFilterString += strLevelIdentifierL5;
			}
		}
		if(EPubConfig.L6 == true || EPubConfig.L6 == "true")
		{
			var nL6Offset = 0;
			if(EPubConfig.TOCStartLevel == "L6")
			{
				if(parseInt(EPubConfig.MDSStartLevel) >= 0)
				{
					nL6Offset = parseInt(EPubConfig.MDSStartLevel);
				}
			}
			if(parseInt(EPubConfig.L6_Offset) >= 0)
			{
				nL6Offset = EPubConfig.L6_Offset;//(EPubConfig.L2_Offset) ? EPubConfig.L2_Offset : 0;
			}
			
			nIndentifierVal = parseInt(objPageData.L6) - parseInt(nL6Offset);
			if (nIndentifierVal > 0) {
				strLevelIdentifierL6 = ((strFilterString != "") ? "+" : "") + EPubConfig.LevelIdentifier_L6.toLowerCase() + ":" + nIndentifierVal;
				strFilterString += strLevelIdentifierL6;
			}
		}
		
		if(strLevelIdentifierL1 == "" && strLevelIdentifierL2 == "" && strLevelIdentifierL3 == "" && strLevelIdentifierL4 == "" && strLevelIdentifierL5 == "" && strLevelIdentifierL6 == "")
		{
			if(strLevelIdentifierL0 != "")
			{
				strLevelIdentifierL0 = "grade:" + objPageData.L0;
				//strSEFacing = "+se_facing:true+freeplay:true";
			}
		}
		
		if(EPubConfig.MDSVersion != 3.2)
		{
			strSEFacing = "+se_facing:true+freeplay:true";
			if(EPubConfig.Component_type == AppConst.TEACHER_EDITION)
			{
				strSEFacing = "";
			}	
		}
		else
		{
			strSEFacing = '+resources_panel_' + ((EPubConfig.Component_type == AppConst.TEACHER_EDITION) ? AppConst.TEACHER_EDITION.toLowerCase() : AppConst.STUDENT_EDITION.toLowerCase()) +':true';
			/*if(strLevelIdentifierL1 == "")
			{
				if(strLevelIdentifierL0 != "" && strLevelIdentifierL2 == "")
				{
					strLevelIdentifierL0 = "grade:" + objPageData.L0;
				}
				
			}*/
		}
		
		
		
		/*if(EPubConfig.L5 == true || EPubConfig.L5 == "true")
		{
			strLessonLevel = parseInt(strLessonLevel) - parseInt(EPubConfig.L5_Offset);
			if (strLessonLevel > 0) {
				strLessonStr = "+" + EPubConfig.LevelIdentifier_L5.toLowerCase() + ":" + strLessonLevel;//GlobalModel.ePubGlobalSettings.Page[iPageIndex]["L5"];
			}
		}
		
		if(parseInt(strLevel) < 0)
		{
			strLevel = 0;
		}
		if(EPubConfig.Component_type == AppConst.TEACHER_EDITION)
		{
			strPath = EPubConfig.Resources_Service_URL + "?q=(grade:" + EPubConfig.L0_value + "+and+" + EPubConfig["LevelIdentifier_" + EPubConfig.TOCStartLevel].toLowerCase() + ":" + strLevel + strLessonStr + ")&collection=Resources&options=search-levelTransformation-json&format=json";
			return strPath;
		}
		else if(EPubConfig.Component_type == AppConst.STUDENT_EDITION || EPubConfig.Component_type == AppConst.OTHERS_EDITION)
		{
			strPath = EPubConfig.Resources_Service_URL + "?q=(grade:" + EPubConfig.L0_value + "+and+" + EPubConfig["LevelIdentifier_" + EPubConfig.TOCStartLevel].toLowerCase() + ":" + strLevel + strLessonStr + "+and+se_facing:true+freeplay:true)&collection=Resources&options=search-levelTransformation-json&format=json";
			return strPath;
		}
		*/
		//console.log(strLevelIdentifierL0, " ", strLevelIdentifierL1, " ", strLevelIdentifierL2, " ", strLevelIdentifierL3)
		if(EPubConfig.Component_type == AppConst.TEACHER_EDITION)
		{
			strPath = EPubConfig.Resources_Service_URL + "?q=(" + strLevelIdentifierL0 + strLevelIdentifierL1 + strLevelIdentifierL2 + strLevelIdentifierL3 + strLevelIdentifierL4 + strLevelIdentifierL5 + strLevelIdentifierL6 + strSEFacing + ")&collection=Resources&options=search-ebook-json&format=json";
			return strPath;
		}
		else if(EPubConfig.Component_type == AppConst.STUDENT_EDITION || EPubConfig.Component_type == AppConst.OTHERS_EDITION)
		{
			strPath = EPubConfig.Resources_Service_URL + "?q=(" + strLevelIdentifierL0 + strLevelIdentifierL1 + strLevelIdentifierL2 + strLevelIdentifierL3 + strLevelIdentifierL4 + strLevelIdentifierL5 + strLevelIdentifierL6 + strSEFacing + ")&collection=Resources&options=search-ebook-json&format=json";
			return strPath;
		}
		
	}
	
	strPath = EPubConfig.baseContentPath + "/" + EPubConfig.bookTitle + "/" + EPubConfig.contentFolderName + "/"+ EPubConfig.resourcesDataPath;
	
	if(strReaderPath == "")
		return strPath;
	
	return EPubConfig.contentFolderName + "/"+ EPubConfig.resourcesDataPath;
}

MG.PathManager.prototype.getNotesAnnotationPath = function(strFileName)
{
    var strPath = EPubConfig.baseContentPath + "/" + EPubConfig.bookTitle + "/" + EPubConfig.contentFolderName + "/"+ EPubConfig.notesAnnotationDataPath;
    
    if(strReaderPath == "")
        return strPath;
    
    return EPubConfig.contentFolderName + "/"+ strFileName;
}

MG.PathManager.prototype.getQuesAnsDPPath = function(strFileName)
{
    var strPath = EPubConfig.baseContentPath + "/" + EPubConfig.bookTitle + "/" + EPubConfig.contentFolderName + "/"+ EPubConfig.QADPDataPath;
    
    if(strReaderPath == "")
        return strPath;
    
    return EPubConfig.contentFolderName + "/"+ strFileName;
}

MG.PathManager.prototype.getEPubFootNotePath = function()
{
	var strPath = EPubConfig.baseContentPath + "/" + EPubConfig.bookTitle + "/" + EPubConfig.contentFolderName + "/"+ EPubConfig.footNoteXMLPath;
	
	if(strReaderPath == "")
		return strPath;
	
	return EPubConfig.contentFolderName + "/"+ EPubConfig.footNoteXMLPath;
}

MG.PathManager.prototype.getImageGalleryXMLPath = function(strFileName)
{
    var strPath = EPubConfig.baseContentPath + "/" + EPubConfig.bookTitle + "/" + EPubConfig.contentFolderName + "/"+ strFileName;
    
    if(strReaderPath == "")
        return strPath;
    
    return EPubConfig.contentFolderName + "/"+ strFileName;
}

MG.PathManager.prototype.getBookPromptXMLPath = function()
{
	
	var strPath = EPubConfig.baseContentPath + "/" + EPubConfig.bookTitle + "/" + EPubConfig.contentFolderName + "/" + EPubConfig.inBookPromptXMLPath;
    
    if(strReaderPath == "")
        return strPath;
    
    return EPubConfig.contentFolderName + "/"+ EPubConfig.inBookPromptXMLPath;
	
}

MG.PathManager.prototype.getTimeToBlogXMLPath = function()
{
	
	var strPath = EPubConfig.baseContentPath + "/" + EPubConfig.bookTitle + "/" + EPubConfig.contentFolderName + "/" + EPubConfig.timeToBlogXMLPath;
    
    if(strReaderPath == "")
        return strPath;
    
    return EPubConfig.contentFolderName + "/"+ EPubConfig.timeToBlogXMLPath;
	
}

MG.PathManager.prototype.getServiceURL = function(strServiceType)
{
	
	var strPath = EPubConfig.Domain_URL;
	switch(strServiceType)
	{
		case "getannotation":
		strPath += EPubConfig.Annotation_Service_URL + "annotation/getannotation";
		break;
		
		case "addupdate":
		strPath += EPubConfig.Annotation_Service_URL + "annotation/addupdate";
		break;
		
		case "getprompt":
		strPath += EPubConfig.Annotation_Service_URL + "prompt/getpromptresponse";
		break;
		
		case "addupdateprompt":
		strPath += EPubConfig.Annotation_Service_URL + "prompt/addupdate";
		break;
	}
    
    return strPath;
    
}

MG.PathManager.prototype.getKTo2DataPath = function(strPath,id)
{
	
	var strFilePath = EPubConfig.baseContentPath + "/" + EPubConfig.bookTitle + "/" + EPubConfig.contentFolderName + "/data/"+id+"/" + strPath;
    
    if(strReaderPath == "")
        return strFilePath;
    
    return EPubConfig.contentFolderName + "/data/"+id+"/" + strPath;
	
}

MG.PathManager.prototype.getActivitiesDataPath = function(strPath)
{
	
	var strFilePath = EPubConfig.baseContentPath + "/" + EPubConfig.bookTitle + "/" + EPubConfig.contentFolderName + "/data/activities/" + strPath;
    
    if(strReaderPath == "")
        return strFilePath;
    
    return EPubConfig.contentFolderName + "/data/activities/" + strPath;
	
}

MG.PathManager.prototype.getActivitiesLaunchPath = function(strPath)
{
	
	var strFilePath = EPubConfig.baseContentPath + "/" + EPubConfig.bookTitle + "/" + EPubConfig.contentFolderName + "/activities/" + strPath;
    
    if(strReaderPath == "")
        return strFilePath;
    
    return EPubConfig.contentFolderName + "/activities/" + strPath;
	
}


MG.PathManager.prototype.getPagesDataPath = function(strPath)
{
	var strFilePath = EPubConfig.baseContentPath + "/" + EPubConfig.bookTitle + "/" + EPubConfig.contentFolderName + "/data/pages/" + strPath;
    
    if(strReaderPath == "")
        return strFilePath;
    
    return EPubConfig.contentFolderName + "/data/pages/" + strPath;
	
}

MG.PathManager.prototype.getTimeToBlogXMLPath = function()
{
	
	var strPath = EPubConfig.baseContentPath + "/" + EPubConfig.bookTitle + "/" + EPubConfig.contentFolderName + "/" + EPubConfig.timeToBlogXMLPath;
    
    if(strReaderPath == "")
        return strPath;
    
    return EPubConfig.contentFolderName + "/"+ EPubConfig.timeToBlogXMLPath;
	
}

MG.PathManager.prototype.getServiceURL = function(strServiceType)
{
	var strPath = EPubConfig.Domain_URL;
	switch(strServiceType)
	{
		case "getannotation":
		strPath += EPubConfig.Annotation_Service_URL + "annotation/getannotation";
		break;
		
		case "addupdate":
		strPath += EPubConfig.Annotation_Service_URL + "annotation/addupdate";
		break;
		
		case "getprompt":
		strPath += EPubConfig.Annotation_Service_URL + "prompt/getpromptresponse";
		break;
		
		case "addupdateprompt":
		strPath += EPubConfig.Annotation_Service_URL + "prompt/addupdate";
		break;
		
		case "authoredBookPrompts":
		strPath += EPubConfig.Annotation_Service_URL + "prompt/getbookprompts.json";
		break;
		
		case "addupdateAuthoredBookPrompts":
		strPath += EPubConfig.Annotation_Service_URL + "prompt/addupdateprompt.json";
		break;
	}
    
    return strPath;
    
}

MG.PathManager.prototype.getExternalAssetPath = function(strURL, mediaType)
{
	
	var strPath = EPubConfig.baseContentPath + "/" + EPubConfig.bookTitle + "/" + EPubConfig.contentFolderName + "/data/assets/" + strURL;
    
    if(strReaderPath == "")
        return strPath;
    
    return EPubConfig.contentFolderName + "/data/assets/" + strURL;
    
}

MG.PathManager.prototype.getLearnosityURL = function()
{
	//if( strReaderPath == "" )
	//{
		return EPubConfig.LearnosityActivityURL;
	//}
	//return strReaderPath + "/" + EPubConfig.LearnosityActivityURL;
}

MG.PathManager.prototype.getHelpAboutAttributionPath = function()
{
	if(strReaderPath == "") 
	{
		return  "content/pdf/HMH_OpenSourceNoticeAndAttribution.pdf";
	}
	else 
	{
		return  strReaderPath + "content/pdf/HMH_OpenSourceNoticeAndAttribution.pdf";
	}
}

MG.PathManager.prototype.getStudentListPath = function () {
    var strPath = EPubConfig.baseContentPath + "/" + EPubConfig.bookTitle + "/" + EPubConfig.contentFolderName + "/data/studentlist.json";

    if (strReaderPath == "")
        return strPath;

    return EPubConfig.contentFolderName + "/data/studentlist.json";
}

MG.PathManager.prototype.getTeacherNodesPath = function () {
    var strPath = EPubConfig.baseContentPath + "/" + EPubConfig.bookTitle + "/" + EPubConfig.contentFolderName + "/data/teacherNodes.json";

    if (strReaderPath == "")
        return strPath;

    return EPubConfig.contentFolderName + "/data/teacherNodes.json";
}