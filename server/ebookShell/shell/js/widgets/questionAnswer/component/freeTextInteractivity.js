/**
 * Author: Lakshay Ahuja
 */
/**
 * @author lakshay.ahuja
 */
(function($, undefined) {
    $.widget("magic.freeTextInteractivity", $.magic.magicwidget, {

        options : {
            userResponse : null,
            interactivityType : "freeTextInteractivity",
            showanswerCalled : false,
            parentgrpId : null
        },

        member : {
            questionArray : [],
            questionArrData : null,
            questionInnercontent : null

        },

        _init : function() {
            $.magic.magicwidget.prototype._init.call(this);
            var questionManagerRef = $("#questionanswerInnerContent").data('questionmanager');
            this.options.parentgrpId = questionManagerRef.member.lastGrpXMLLoaded;
            if (GlobalModel.questionPanelAnswers[this.options.parentgrpId] == null)
                GlobalModel.questionPanelAnswers[this.options.parentgrpId] = {}
        },

        _create : function() {
        },

        setQAPanelInnercontentLayout : function(strQAContent, questionInnerContent) {
            var objThis = this;
			//console.log('callsdfklsnf sf');
            var numberOfHeading;
            var questionPanelOptions = $(questionInnerContent).find('[id=questionPanelOptions]');
            questionPanelOptions.removeClass("questionPanelMultipleChoiceOptions");
            if (strQAContent.itemBody.div == undefined || strQAContent.itemBody.div[0].p == undefined) {
                numberOfHeading = 0;
                $(questionInnerContent).find("#questionPanelSectionTitle").html('');
                $(questionInnerContent).find("#questionPanelSectionContent").html('');
            } else if (strQAContent.itemBody.div[0].p.length == undefined && typeof strQAContent.itemBody.div[0].p == "object") {
                numberOfHeading = 1;
            } else {
                numberOfHeading = strQAContent.itemBody.div[0].p.length;
            }
            for (var k = 0; k < numberOfHeading; k++) {
                if (numberOfHeading == 1) {
                    switch(strQAContent.itemBody.div.p.label) {
                        case 'heading':
                            $(questionInnerContent).find("#questionPanelSectionTitle").html(strQAContent.itemBody.div[0].p.span);
                            $(questionInnerContent).find("#questionPanelSectionContent").html('');

                            break;
                        case 'instruct':
                            $(questionInnerContent).find("#questionPanelSectionContent").html(strQAContent.itemBody.div[0].p.span);
                            $(questionInnerContent).find("#questionPanelSectionTitle").html('');
                            break;
                    }
                } else {
                    switch(strQAContent.itemBody.div[0].p[k].label) {
                        case 'heading':
                            $(questionInnerContent).find("#questionPanelSectionTitle").html(strQAContent.itemBody.div[0].p[k].span);
                            break;
                        case 'instruct':
                            $(questionInnerContent).find("#questionPanelSectionContent").html(strQAContent.itemBody.div[0].p[k].span);
                            break;
                    }
                }
            }
            if (strQAContent.itemBody.div[1].p) {
                $(questionInnerContent).find('[id=theQuestionAsked]').html(strQAContent.itemBody.div[1].p);
            } else {
                $(questionInnerContent).find('[id=theQuestionAsked]').html('');
            }
            //$(questionInnerContent.element).find('[id=theQuestionAsked]').html(strQAContent.itemBody.div[1].p);
            questionPanelOptions.removeClass("questionPanelOptions").addClass("questionPanelOptionsText").css('width', '96%');
            $(questionInnerContent).find('[id=questionImage]').css("display", "none");
            $(questionInnerContent).find("#questionPanelQuestionContent").css('width', 'auto');
            //QAfreeTextImage
            questionPanelOptions.html('<textarea id="freeTextInteractivityTxtArea" class="freeTextInteractivityTxtArea"></textarea>');
            if( GlobalModel.BookEdition == "TSE" && GlobalModel.selectedStudentData )
            {
            	questionPanelOptions.find("textarea").addClass("contentEditableFalseDiv");
            	questionPanelOptions.find("textarea").attr( "readonly", true );
            }
        },

        populatePrintPanelForInteractivity : function(strQAContent, printableInnerContent,arrObj) {
            var objThis = this;
            objThis.setQAPanelInnercontentLayout(strQAContent, printableInnerContent);
            //console.log("printableInnerContent: ",printableInnerContent);
            $(printableInnerContent).find('[id=questionPanelOptions]').find('[id="freeTextInteractivityTxtArea"]').attr('readonly', true);
            $(printableInnerContent).find("#questionPanelQuestionContent").css('width', 'auto');
            var elemId = arrObj.interid;
            var parentGrpIdforPrint = arrObj.interParentid;
            var objData = GlobalModel.questionPanelAnswers[parentGrpIdforPrint][elemId];
            $(printableInnerContent).find('textarea').replaceWith('<div id="freeTextInteractivityTxtArea" class="freeTextInteractivityTxtArea"></div>');
            //<textarea id="freeTextInteractivityTxtArea" class="freeTextInteractivityTxtArea"></textarea>
            //$(printableInnerContent).find('[id=freeTextInteractivityTxtArea]').val("HERE I AM");
            if (objData && $(objData.response).length > 0) {
                objThis.options.userResponse = $(objData.response).find("response").html();
                $(printableInnerContent).find('[id=freeTextInteractivityTxtArea]').val(objThis.options.userResponse);
                $(printableInnerContent).find('[id=freeTextInteractivityTxtArea]').html(objThis.options.userResponse);
            }
        },

        populateQAPanelInnerContent : function(strQAContent, questionInnerContent) {
            var objThis = this;
            try {
                objThis.member.questionInnercontent = questionInnerContent;
                objThis.options.xmlContent = strQAContent;
                objThis.setQAPanelInnercontentLayout(strQAContent, questionInnerContent.element);
                objThis.setSavedUserResponse();
            } catch(e) {

            }

        },

        saveCurrentResponse : function() {
            this.setUserResponse();
        },

        setUserResponse : function() {
            var objThis = this;
            objThis.options.userResponse = $.trim($(objThis.member.questionInnercontent.element).find('[id=freeTextInteractivityTxtArea]').val());
            $(objThis.member.questionInnercontent.element).parent().parent().trigger('recentResponseSaved');
        },

        setSavedUserResponse : function() {
            var objThis = this;
            var indexofQues = parseInt($(objThis.element).attr('id').replace('questionPanelques','')) - 1;
            var questionManagerRef = $("#questionanswerInnerContent").data('questionmanager');
            var questionsGrp = questionManagerRef.getCurrentGroupQuestionsArray();
            var quesid = questionsGrp[indexofQues];
            var objData = GlobalModel.questionPanelAnswers[objThis.options.parentgrpId][quesid];
            if (objData && $(objData.response).length > 0) {
                objThis.options.userResponse = $.trim($(objData.response).find("response").html());
                $(objThis.member.questionInnercontent.element).find('[id=freeTextInteractivityTxtArea]').val(objThis.options.userResponse);
                $(objThis.member.questionInnercontent.element).find('[id=freeTextInteractivityTxtArea]').html(objThis.options.userResponse);
                if (objThis.options.userResponse != null && objThis.options.userResponse != -1 && objThis.options.userResponse != "") {
                    var indexElem = $(objThis.element).parent().children().index($(objThis.element));
                    $($(objThis.member.questionInnercontent.element).parent().parent().find('[type="questionPanelquesDots"]')[indexElem]).addClass('quesCompleteCircle').removeClass('quesPendingCircle');
                }
            }

        },

        saveCurrentResponseToServices : function() {
        	var objThis = this;
            objThis.options.userResponse = $.trim($(objThis.member.questionInnercontent.element).find('[id=freeTextInteractivityTxtArea]').val());
            var strFormattedQuestion = $(objThis.member.questionInnercontent.element).find('[id=questionPanelQuestion]').html();
            strFormattedQuestion = strFormattedQuestion.replace(objThis.options.userResponse, "");
            var strBodyText = "<qa><question>" + strFormattedQuestion + "</question><response>" + objThis.options.userResponse + "</response></qa>";
            var arrObj = new Object();
            var indexofQues = parseInt($(objThis.element).attr('id').replace('questionPanelques','')) - 1;
            var questionManagerRef = $("#questionanswerInnerContent").data('questionmanager');
            var questionsGrp = questionManagerRef.getCurrentGroupQuestionsArray();
            arrObj.id = questionsGrp[indexofQues];
            arrObj.response = strBodyText;
            arrObj.interactivityType = objThis.options.interactivityType;
            //console.log( $(objThis.member.questionInnercontent.element).parent().parent().find('[id="questionPanelPageNumber"]'), " This is Interactivity");
            arrObj.pageNumber = $(objThis.member.questionInnercontent.element).parent().parent().find('[id="questionPanelPageNumber"]').find("span").html().replace(GlobalModel.localizationData["QA_PANEL_PAGE_PREFIX"], "");
            //arrObj.pageNumber = GlobalModel.questionPanelAnswers[objThis.options.parentgrpId][arrObj.id].pageNumber;
            arrObj.creationDate = getTodaysDate();
            arrObj.parentGrpId = questionManagerRef.member.lastGrpXMLLoaded;
            

            if ($("#questionAnswerPanel .savetoNoteBookBtn").hasClass('savetoNoteBookBtnChk')) {
                arrObj.savetonote = 1;
            } else {
                arrObj.savetonote = 0;
            }
            //var curParentId = ($("#"+arrObj.id).parent().attr('interactiongroupid'));
            var flag = -1;
            //GlobalModel.questionPanelAnswersForAnnoPanel.push(arrObj)
            for (var i = 0; i < GlobalModel.questionPanelAnswersForAnnoPanel.length; i++) {
                var gModelIdParent = GlobalModel.questionPanelAnswersForAnnoPanel[i].parentGrpId;
                if (gModelIdParent == arrObj.parentGrpId) {
                    flag = 1;
                    break;
                }
            }

            var indexElem = indexofQues;
            if (objThis.options.userResponse != null && objThis.options.userResponse != "") {
                arrObj.flag_id = 1;
                $($(objThis.member.questionInnercontent.element).parent().parent().find('[type="questionPanelquesDots"]')[indexElem]).addClass('quesCompleteCircle').removeClass('quesPendingCircle');
            } else {
                arrObj.flag_id = 0;
                $($(objThis.member.questionInnercontent.element).parent().parent().find('[type="questionPanelquesDots"]')[indexElem]).removeClass('quesCompleteCircle').addClass('quesPendingCircle');
            }
            if (GlobalModel.questionPanelAnswers[arrObj.parentGrpId][arrObj.id] == null) {
                
            	if( GlobalModel.BookEdition == "TSE" && GlobalModel.selectedStudentData )
        		{
            		// do nothing
        		}
            	else
            	{
            		ServiceManager.Annotations.create(arrObj, 9, function(data){
            			GlobalModel.questionPanelAnswers[objThis.options.parentgrpId][arrObj.id].annotation_id = data.annotation_id;
            		});
            	}
                
            } else {

            	if( GlobalModel.BookEdition == "TSE" && GlobalModel.selectedStudentData )
        		{
            		// do nothing
        		}
            	else
            	{
            		ServiceManager.Annotations.update(9, arrObj);
            	}
                arrObj.annotation_id = GlobalModel.questionPanelAnswers[arrObj.parentGrpId][arrObj.id].annotation_id;
            }
            if( GlobalModel.questionPanelAnswers[objThis.options.parentgrpId][arrObj.id] &&
            	GlobalModel.questionPanelAnswers[objThis.options.parentgrpId][arrObj.id].comments )
            {
            	arrObj.comments = GlobalModel.questionPanelAnswers[objThis.options.parentgrpId][arrObj.id].comments;
            	arrObj.isNotified = GlobalModel.questionPanelAnswers[objThis.options.parentgrpId][arrObj.id].isNotified;
            }
            
            if( GlobalModel.BookEdition == "TSE" && GlobalModel.selectedStudentData )
            {
            	if( GlobalModel.questionPanelAnswers[objThis.options.parentgrpId][arrObj.id] )
        		{
            		$("#questionanswerInnerContent").data('questionmanager').addCommentToQA( arrObj,
        				GlobalModel.questionPanelAnswers[objThis.options.parentgrpId][arrObj.id].comments
        				,GlobalModel.questionPanelAnswers[objThis.options.parentgrpId][arrObj.id].pageNumber);
        		}
        		else
        		{
        			$("#questionanswerInnerContent").data('questionmanager').addCommentToQA();
        		}
            }
            else if( GlobalModel.BookEdition == "SE" )
            {
            	$("#questionanswerInnerContent").data('questionmanager').resetCommentSection();
            	$("#questionanswerInnerContent").data('questionmanager').addCommentInCommentBox();
            }
            if( GlobalModel.questionPanelAnswers[objThis.options.parentgrpId][arrObj.id] )
            {
            	arrObj.pageNumber = GlobalModel.questionPanelAnswers[objThis.options.parentgrpId][arrObj.id].pageNumber;
            }
            if( GlobalModel.BookEdition == "TSE" && GlobalModel.selectedStudentData )
    		{
        		// do nothing
    		}
        	else
        	{
        		GlobalModel.questionPanelAnswers[objThis.options.parentgrpId][arrObj.id] = arrObj;
        	}

        },
        displayHideCorrectResponse : function(bVal) {
            var objThis = this;
            objThis.options.showanswerCalled = true;
            var xmlContent = objThis.options.xmlContent;
            var correctResponse = objThis.options.xmlContent.responseDeclaration.correctResponse.value;
            //var correctResponseLength = objThis.options.xmlContent.responseDeclaration.correctResponse.value.length;
            if (bVal == 1) {
                objThis.options.userResponse = $(objThis.member.questionInnercontent.element).find('[id=freeTextInteractivityTxtArea]').val();
                $(objThis.member.questionInnercontent.element).find('[id=questionPanelOptions]').find('[id=freeTextInteractivityTxtArea]').css('display', 'none');
                $(objThis.member.questionInnercontent.element).find('[id=questionPanelOptions]').append('<textarea id="freeTextInteractivityShowAnswerTxtArea" class="freeTextInteractivityTxtArea" disabled>' + correctResponse + '</textarea>');
                //$(objThis.member.questionInnercontent.element).find('[id=freeTextInteractivityTxtArea]').val(correctResponse);
            } else {
                $(objThis.member.questionInnercontent.element).find('[id=questionPanelOptions]').find('[id=freeTextInteractivityTxtArea]').css('display', 'block');
                $(objThis.member.questionInnercontent.element).find('[id=questionPanelOptions]').find('[id=freeTextInteractivityShowAnswerTxtArea]').remove();

            }
        }
    });
})(jQuery);
