MG.MediaPlayerOperator = function() 
{
	this.objAudioLauncher = null;
	this.scrollViewRef = null;
	this.hasLeftPageIDPFAudio = false;
	this.hasRightPageIDPFAudio = false;
	this.playRightPageAudio = true;
	this.leftMediaOverlayPath = "";
	this.rightMediaOverlayPath = "";
	this.pageIndexOfCurrentPageAudio = -1;
	this.objAudio = new AudioPlayer();
	this.audiosynclass = "";
	this.currentAudioComp = null;
	this.leftPanelAudioClicked = false;
}

MG.MediaPlayerOperator.prototype = new MG.BaseOperator();

MG.MediaPlayerOperator.prototype.attachComponent = function(objComp) 
{
	if (objComp == null)
		return;
	
	var strType = objComp.element.attr( AppConst.ATTR_TYPE ).toString();
	var objThis = this;
	
	switch (strType) 
	{

		case AppConst.AUDIO_LAUNCHER_BTN:
				 objThis.objAudioLauncher = objComp;
				 $( objComp ).bind( "click" , function ( ) 
				 {
					objThis.hasLeftPageIDPFAudio = false;
					objThis.hasRightPageIDPFAudio = false;
					objThis.playRightPageAudio = true;
					objThis.leftMediaOverlayPath = "";
					objThis.rightMediaOverlayPath = "";
					
					if( $( objThis.objAudioLauncher.element ).hasClass( 'audioLauncherBtn-on' ) )
					{
						objThis.objAudio.closeAudioPanel( );
					}	
					else
					{
						$( objThis.objAudioLauncher.element ).addClass( "ui-disabled" );
						$( objThis.objAudioLauncher.element ).data( 'buttonComp' ).disable();
						objThis.leftPanelAudioClicked = true;
						setTimeout( function( )
						{
							$( '#audioLauncherBtn' ).removeClass( 'audioLauncherBtn-off' ).addClass( 'audioLauncherBtn-on' );
						},0);
						
						objThis.hidingQAPAnel();
						
						var strCurrentPageName = objThis.scrollViewRef.getCurrentPageName();
						var arrPageNames = strCurrentPageName.split(",");
						
						//if global settings exists, check if current page has page-level audio available so that the audio icon in annotations panel could be enabled or disabled accordingly.
						if(GlobalModel.ePubGlobalSettings)
						{
					        if (GlobalModel.ePubGlobalSettings.Page) {
					            var arrPages = GlobalModel.ePubGlobalSettings.Page;
					            
					            for (var i = 0; i < arrPages.length; i++) {
					                var objItem = arrPages[i];
					                if (arrPageNames[0].indexOf(objItem.PageName) > -1 && objItem.mediaOverlayPath) 
					                {
					                	objThis.leftMediaOverlayPath = objItem.mediaOverlayPath;
					                    objThis.hasLeftPageIDPFAudio = true;
					                }
					                else if(arrPageNames[1])
					                {
					                	if (arrPageNames[1].indexOf(objItem.PageName) > -1 && objItem.mediaOverlayPath) 
					                	{
					                		objThis.rightMediaOverlayPath = objItem.mediaOverlayPath;
					                        objThis.hasRightPageIDPFAudio = true;
					                    }
					                }
					            }
					        }
						}
	
						if( objThis.hasLeftPageIDPFAudio )
						{
							objThis.launchAudio( false );
						}
						else if( objThis.hasRightPageIDPFAudio )
						{
							objThis.launchAudio( true );
						}
					}	
				});
				
				$(objThis.objAudio).unbind( 'change_audio_state' ).bind( 'change_audio_state' , function ( )
				{
					this.pageIndexOfCurrentPageAudio = -1;
					setTimeout( function( )
					{
						if( ! $( '#audioLauncherBtn' ).hasClass( 'ui-disabled' ) )
						{
							$( '#audioLauncherBtn' ).removeClass( 'audioLauncherBtn-on' ).addClass( 'audioLauncherBtn-off' );							
						}
					},0);
					if( objThis.currentAudioComp )
					{
						$( objThis.currentAudioComp ).removeClass( "playAudioContent" ).addClass( "audioHotspotContent" );
						objThis.currentAudioComp = null;
					}
				});
				
				$( objThis.objAudio ).unbind('enable_leftpanelbutton').bind( 'enable_leftpanelbutton' ,function ( )
				{
					if( objThis.leftPanelAudioClicked )
					{
						$( objThis.objAudioLauncher.element ).removeClass( "ui-disabled" );
						$( objThis.objAudioLauncher.element ).data( 'buttonComp' ).enable( );
						objThis.leftPanelAudioClicked = false;
					}

				});
				
				$( objThis.objAudio ).unbind( 'smil_error' ).bind( 'smil_error' ,function ( )
				{
           			//For opening the readers alert box
					objThis.objAudio.closeAudioPanel( );
					$( objThis.objAudioLauncher.element ).removeClass( "ui-disabled" );
					$(objThis.objAudioLauncher.element).data( 'buttonComp' ).enable( );
					$( "#alertTxt" ).html( GlobalModel.localizationData[ "SMIL_ERROR" ] );
					setTimeout( function( )
					{
						PopupManager.addPopup( $( "#alertPopUpPanel" ) , null, {
							        isModal: true,
							        isCentered: true,
									popupOverlayStyle: "popupMgrMaskStyleSemiTransparent",
									isTapLayoutDisabled: true
							    });
					},0);
				});
			break;
		case AppConst.SCROLL_VIEW_CONTAINER:
				objThis.scrollViewRef = objComp;
				if(EPubConfig.MediaActiveClass && EPubConfig.MediaActiveClass != "")
				{
					objThis.audiosynclass = EPubConfig.MediaActiveClass
				}
				else
				{
					objThis.audiosynclass = "-epub-media-overlay-active";
				}
				$(objComp).bind(objComp.events.PAGE_RENDER_COMPLETE, function() {
					objThis.scrollViewRenderCompleteHandler(objComp);
				});
				$(objComp).bind(objComp.events.PAGE_INDEX_CHANGE, function() 
				{
					if( $("#audioPopUp").css("display") == "block" )
					{
						if(EPubConfig.pageView == AppConst.DOUBLE_PAGE_CAMEL_CASING || EPubConfig.pageView == AppConst.SINGLE_PAGE_CAMEL_CASING)
						{
							objThis.closeAudioPanel();	
						}
						else
						{
							if( objThis.pageIndexOfCurrentPageAudio != GlobalModel.currentPageIndex )
							{
								objThis.closeAudioPanel();	
							}
						}
					}
				});	
			/////////////////////////////////////////
		    // main visibility API function 
		    // check if current tab is active or not
            var vis = (function () {
                var stateKey,
                    eventKey,
                    keys = {
                        hidden: "visibilitychange",
                        webkitHidden: "webkitvisibilitychange",
                        mozHidden: "mozvisibilitychange",
                        msHidden: "msvisibilitychange"
                    };
                for (stateKey in keys) {
                    if (stateKey in document) {
                        eventKey = keys[stateKey];
                        break;
                    }
                }
                return function (c) {
                    if (c) document.addEventListener(eventKey, c);
                    return !document[stateKey];
                }
            })();


            function pauseAudioPlayer(evt) {
                if ($("#audioPopUp").css("display") == "block" && $("#audioPopUp").find('.audio-play').css("display") == "block") {
                    $("#audioPopUp").find('.audio-play').trigger("click");
                }
            }

		    /////////////////////////////////////////
		    // check if current tab is active or not
            vis(function () {

                if (vis()) {

                    // the setTimeout() is used due to a delay 
                    // before the tab gains focus again, very important!
                    //setTimeout(function () {

                        // resume() code goes here	


                    //}, 300);

                } else {

                    // pause() code goes here	
                    pauseAudioPlayer();
                }
            });

            

		    /////////////////////////////////////////
		    // check if browser window has focus		
            var notIE = (document.documentMode === undefined),
                isChromium = window.chrome;

            if (notIE && !isChromium) {
                
                
                // checks for Firefox and other  NON IE Chrome versions
                //$(window).on("focusin", function () {
                    
                    //setTimeout(function () {

                        // resume() code goes here


                    //}, 300);

                //}).on("focusout", function (e) {
                    
                    // pause() code goes here
                    //pauseAudioPlayer();
                   

                //});

                //window.onpageshow = window.onpagehide = window.onfocus = window.onblur = pauseAudioPlayer;

            } else {

                // checks for IE and Chromium versions
                if (window.addEventListener) {

                    // bind focus event
                    window.addEventListener("focus", function (event) {
                        
                        //setTimeout(function () {

                            // resume() code goes here


                        //}, 300);

                    }, false);

                    // bind blur event
                    window.addEventListener("blur", function (event) {
                        
                        // pause() code goes
                        pauseAudioPlayer();

                    }, false);

                } else {

                    // bind focus event
                    window.attachEvent("focus", function (event) {
                       
                        //setTimeout(function () {

                            // resume() code goes here


                        //}, 300);

                    });

                    // bind focus event
                    window.attachEvent("blur", function (event) {
                       
                        // pause() code goes here
                        pauseAudioPlayer();

                    });
                }
            }
			break;
		default:
			break;
	}
}
MG.MediaPlayerOperator.prototype.scrollViewRenderCompleteHandler = function( objComp ) 
{
	var objThis = this;

	var arrPageComps = $( objComp.element ).find( '[data-role="pagecomp"]' );
	var objPageRole = null;
	for ( var i = 0; i < arrPageComps.length; i++ ) 
	{
		objPageRole = $( arrPageComps[i] ).data( "pagecomp" );

		$( objPageRole ).bind( objPageRole.events.PAGE_CONTENT_LOAD_COMPLETE , function( e ) {
			objThis.initializeAudioItemClick( e.currentTarget );
		});
	}
}
MG.MediaPlayerOperator.prototype.initializeAudioItemClick = function ( objComp)  
{
	var objThis = this;
	$( objComp.element ).find( '[type=pageLevelAudioHotSpot]' ).unbind( 'click' ).bind( 'click' , function ( )
	{
		$( objComp.element ).find( '[type=pageLevelAudioHotSpot]' ).addClass( 'ui-disabled' );
		objThis.objAudio.stopAudio();
		if( ! $( '#audioLauncherBtn' ).hasClass( 'ui-disabled' ) )
		{
			$( '#audioLauncherBtn' ).removeClass( 'audioLauncherBtn-off' ).addClass( 'audioLauncherBtn-on' );							
		}
		objThis.pageLevelAudioPath = getPathManager().getEPubPageLevelAudioPath( $(this).attr("src") );
		var option = {
			pageLevelAudioPath: objThis.pageLevelAudioPath,
			autoplay: true
		};
		objThis.currentAudioComp = this;
		objThis.pageIndexOfCurrentPageAudio = GlobalModel.currentPageIndex;
		$( objThis.currentAudioComp ).removeClass( "audioHotspotContent" ).addClass( "playAudioContent" );
		objThis.objAudio.setMediaElement( option );
         
	});
	
	$( objComp.element ).find( '[type=pageLevelNoIconAudioHotSpot]' ).unbind( 'click' ).bind( 'click' , function ( )
	{
		$( objComp.element ).find( '[type=pageLevelNoIconAudioHotSpot]' ).addClass( 'ui-disabled' );
		objThis.objAudio.stopAudio();
		if( ! $( '#audioLauncherBtn' ).hasClass( 'ui-disabled' ) )
		{
			$( '#audioLauncherBtn' ).removeClass( 'audioLauncherBtn-off' ).addClass( 'audioLauncherBtn-on' );							
		}
		objThis.pageLevelAudioPath = getPathManager().getEPubPageLevelAudioPath( $(this).attr("src") );
		var option = {
			pageLevelAudioPath: objThis.pageLevelAudioPath,
			autoplay: true
		};
		objThis.pageIndexOfCurrentPageAudio = GlobalModel.currentPageIndex;
		objThis.objAudio.setMediaElement( option );
         
	});
	$( objComp.element ).find( '[type=audioHotSpot]' ).unbind( 'click' ).bind( 'click' , function ( )
	{
		$( objComp.element ).find( '[type=audioHotSpot]' ).addClass( 'ui-disabled' );
		objThis.objAudio.stopAudio();
		if( ! $( '#audioLauncherBtn' ).hasClass( 'ui-disabled' ) )
		{
			$( '#audioLauncherBtn' ).removeClass( 'audioLauncherBtn-off' ).addClass( 'audioLauncherBtn-on' );							
		}
		objThis.currentAudioComp = this;
		$( objThis.currentAudioComp ).removeClass( "audioHotspotContent" ).addClass( "playAudioContent" );
		
		var strReadAloudBlock = $(this).attr('read-aloud-block');
		var objPageContainer = $(this).closest('[type="PageContainer"]')[0];
		var smilPagePath = $(objPageContainer).attr("pagename") + ".smil";
		var smilURL = getPathManager().getEPubPageSMILPath(smilPagePath);
		var readAloudBlock = $(objPageContainer).find('[id="' + strReadAloudBlock + '"]');
		objThis.pageIndexOfCurrentPageAudio = GlobalModel.currentPageIndex;
		var option = {
			mediaOverlayPath: smilURL,
			autoplay: true,
			currentPageFrame: readAloudBlock,
			highlightClass: objThis.audiosynclass
		};
		
		objThis.objAudio.setMediaElement( option );
		
	});
	
	$( objThis.objAudio ).unbind('enable_hotspotbutton').bind( 'enable_hotspotbutton' ,function ( )
	{
		$( '[type=pageLevelAudioHotSpot]' ).removeClass( 'ui-disabled' );
		$( '[type=pageLevelNoIconAudioHotSpot]' ).removeClass( 'ui-disabled' );
		$( '[type=audioHotSpot]' ).removeClass( 'ui-disabled' );
	});
	
}

MG.MediaPlayerOperator.prototype.launchAudio = function( isRightPageAudio  ) 
{
	var objThis = this;
	var objPageContainer;
	var mediaOverlayPath = "";
	if( isRightPageAudio )
	{
		objPageContainer = $( $('[type=PageContainer]' )[ GlobalModel.currentPageIndex + 1 ] );
		mediaOverlayPath = objThis.rightMediaOverlayPath;
	}
	else
	{
		objPageContainer = $( $( '[type=PageContainer]' )[ GlobalModel.currentPageIndex ] );
		mediaOverlayPath = objThis.leftMediaOverlayPath;
	}
	objThis.pageIndexOfCurrentPageAudio = GlobalModel.currentPageIndex;
	var smilURL = getPathManager().getOverlaySMILPath( mediaOverlayPath );
	var hideAudioText = false;
	if( !(EPubConfig.MediaActiveClass && EPubConfig.MediaActiveClass != "" ) )
	{
		hideAudioText = true;
	}
	var option = {
		mediaOverlayPath: smilURL,
		autoplay: objThis.playRightPageAudio,
		currentPageFrame: objPageContainer[0],
		highlightClass: objThis.audiosynclass,
		hideAudioText: hideAudioText
	};
	
	objThis.objAudio.setMediaElement( option );
	
	$( objThis.objAudio ).unbind( 'audio_complete' ).bind( 'audio_complete' , function ( )
	{
		if( objThis.hasLeftPageIDPFAudio && objThis.hasRightPageIDPFAudio )
		{
			if( isRightPageAudio == true )
			{
				objThis.playRightPageAudio = false;
				objThis.launchAudio( false );
				
			}
			else
			{
				objThis.playRightPageAudio = true;
				objThis.launchAudio( true );
				
			}
		}
	});
	
}

MG.MediaPlayerOperator.prototype.hidingQAPAnel = function (  ) 
{
    if( $( "#questionAnswerPanel" ).css( 'display' ) == "block" )
    {
        $( "#questionAnswerPanel" ).find( "#questionPanelCloseBtn" ).trigger( 'click' );
    }
    
    if( $( "#imageGalleryPanel" ).css( 'display' ) == "block" )
    {
        $( "#imageGalleryPanel" ).find( '[id="imageGalleryClsBtn"]' ).trigger( 'click' );
    }
}

MG.MediaPlayerOperator.prototype.closeAudioPanel = function ( )
{
	this.pageIndexOfCurrentPageAudio = -1;
	if( objThis.currentAudioComp )
	{
		$( objThis.currentAudioComp ).removeClass( "playAudioContent" ).addClass( "audioHotspotContent" );
		objThis.currentAudioComp = null;
	}
	this.objAudio.closeAudioPanel( );
}