/**
 * @author Magic s/w pvt ltd
 */

MG.GlossaryOperator = function(){
    this.superClass = MG.BaseOperator.prototype;
}

/*----------------------------------------------------------------------------------------------------------------------------------------------
 *
 * 														Prototype functions starts
 *  
 *---------------------------------------------------------------------------------------------------------------------------------------------*/
MG.GlossaryOperator.prototype = new MG.BaseOperator();

/*
 * This function will handle all the events related to Glossary components.
 */
MG.GlossaryOperator.prototype.attachComponent = function(objComp){
    if(objComp == null)
        return;
    
    var strType = objComp.element.attr(AppConst.ATTR_TYPE).toString();
    var objThis = this;
    switch (strType) {

        case AppConst.SCROLL_VIEW_CONTAINER:
        	/* 
             * PAGE_RENDER_COMPLETE ( pagerendercomplete ) is triggerd from pagecomp when the page rendering completes.
             */
            $(objComp).bind(objComp.events.PAGE_RENDER_COMPLETE, function(){
                objThis.doFunctionCall(AppConst.GLOSSARY_PANEL_COMP,'loadGlossaryContent');
                objThis.scrollViewRenderCompleteHandler(objComp);
            });
            break;
            
        case AppConst.GLOSSARY_PANEL_COMP:
            
            break;
            
        default:
            //console.error('### ERROR: ['+ strType +'] -- Either "type" is not defined or Invalid operator is assigned');
            break;
    }
    
    return MG.BaseOperator.prototype.attachComponent.call(this, objComp);
}

MG.GlossaryOperator.prototype.scrollViewRenderCompleteHandler = function(objComp)
{
    var objThis = this;
    var arrPageComps =$(objComp.element).find('[data-role="pagecomp"]');
    var objPageRole = null;
    for(var i = 0; i < arrPageComps.length; i++)
    {
        objPageRole = $(arrPageComps[i]).data("pagecomp");
        /* 
         * PAGE_CONTENT_LOAD_COMPLETE ( pagecontentloadcomplete ) is triggerd from pagecomp when the page completely loaded.
         */
        $(objPageRole).bind(objPageRole.events.PAGE_CONTENT_LOAD_COMPLETE, function(e){
            objThis.initializeGlossaryItemClick(e.currentTarget);
        });
    }

}

/* 
 * initializeGlossaryClick attach the click event on glossary.
 */
MG.GlossaryOperator.prototype.initializeGlossaryItemClick = function(objComp)
{
    if(EPubConfig.RequiredApplicationComponents.indexOf(AppConst.GLOSSARY) > -1)
    {
        var objThis = this;
        //Ensuring that glossary type is a String;
        EPubConfig.glossaryType = EPubConfig.glossaryType + "";
                
        if (EPubConfig.glossaryType.toLowerCase() == 'in-context') {
            var arrPageComps;
            var isXHTML = objThis.doFunctionCall(AppConst.GLOSSARY_PANEL_COMP,"isglossaryXHTML");
            if(!isXHTML)
            	arrPageComps = $(objComp.element).find('[type="glossterm"]');
            else
            	arrPageComps = $(objComp.element).find('[data-term]');	
            var objBtnRole = null;

            for (var i = 0; i < arrPageComps.length; i++) {
            	if(isXHTML)
            	{
	            	$(arrPageComps[i]).removeAttr("pageindex");
	            	$(arrPageComps[i]).css("cursor","pointer");
	            	$(arrPageComps[i]).attr("data-role","buttonComp");
	            }
                var strMethod = $(arrPageComps[i]).attr("data-role");
                try {
                    $(arrPageComps[i])[strMethod]();
                } 
                catch (err) {
                //console.error("Error Unable to instantiate widget ", strMethod, " \n  ", err);
                }
                objBtnRole = $(arrPageComps[i]).data("buttonComp");
                
                /* 
				 * Click event on glossary.
				 */
                $(arrPageComps[i]).unbind("click").bind('click', function(e){
                    e.preventDefault();
                    var strWrd = $(this).attr('data-term');
                    if(strWrd == null || strWrd == undefined){
                        strWrd = $(this).text();
                    }

					if( $("#audioPopUp").css("display") == "block" )
					{
            			$("#audioPlayerCloseBtn").trigger('click');
					}

                    var bNoErrorEncountered = objThis.doFunctionCall(AppConst.GLOSSARY_PANEL_COMP, "setGlossaryDefinition", strWrd);
                    
                    if(bNoErrorEncountered)
                    {
                        
                        PopupManager.removePopup();
                        PopupManager.addPopup($("#glossaryPanel"), $(this).data("buttonComp"), {
                            isModal: true,
                            hasPointer: true,
                            offsetScale: GlobalModel.currentScalePercent
                        });
                        
                        $("#glossaryTextDiv").parent().scrollTop(0);
                        $("#glossaryTextDiv").parent().scrollLeft(0);
                        
                    }
                });
            }
            
            $("#glossaryPanel").bind("popupClosed",function(){
                objThis.doFunctionCall(AppConst.GLOSSARY_PANEL_COMP, "closeGlossaryPopUp");
            });
        }
    }
}

