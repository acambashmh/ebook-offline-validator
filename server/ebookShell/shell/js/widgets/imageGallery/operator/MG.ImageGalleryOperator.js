/**
 * @author Lakshay Ahuja
 */

MG.ImageGalleryOperator = function() {
    //this.objOperatorManager = null;
    this.superClass = MG.BaseOperator.prototype;
    this.imageGalleryRef = null;
}

MG.ImageGalleryOperator.prototype = new MG.BaseOperator();

MG.ImageGalleryOperator.prototype.attachComponent = function(objComp) {
    if (objComp == null)
        return;

    var strType = objComp.element.attr(AppConst.ATTR_TYPE).toString();
    var objThis = this;
    switch (strType) {

        case AppConst.SCROLL_VIEW_CONTAINER:
        	
            $(objComp).bind(objComp.events.PAGE_RENDER_COMPLETE, function() {
                objThis.scrollViewRenderCompleteHandler(objComp);
            });
            break;

        case AppConst.IMAGE_GALLERY_PANEL:
            objThis.imageGalleryRef = objComp;
            $(objComp.element).find('[type="imageGalleryClsBtn"]').unbind('click').bind('click', function() {
                objThis.doFunctionCall(AppConst.IMAGE_GALLERY_PANEL, 'hideImageGalleryPanel');
            });

            $(objComp.element).find('[type="imageInnerContainer"]').each(function() {
                $(this).unbind('click').bind('click', function() {
                    objThis.doFunctionCall(AppConst.IMAGE_GALLERY_PANEL, 'expandImageBox', this);
                });
            });

            $(objComp.element).find('[id=iGZoomInBtn]').unbind('click').bind('click', function() {
                objThis.doFunctionCall(AppConst.IMAGE_GALLERY_PANEL, 'zoomInOutInnerPanelImage', 1);
            });

            $(objComp.element).find('[id=iGZoomOutBtn]').unbind('click').bind('click', function() {
                objThis.doFunctionCall(AppConst.IMAGE_GALLERY_PANEL, 'zoomInOutInnerPanelImage', -1);
            });

            $(objComp.element).find('[id="imageGalleryContractBtn"]').unbind('click').bind('click', function() {
                var collapseBtnRef = this;
                objThis.doFunctionCall(AppConst.IMAGE_GALLERY_PANEL, 'collapseImageBox', this);
            });

            $(objComp.element).find('[id="nextExpandedImg"]').unbind('click').bind('click', function() {
                objComp.showExpandedImage(1);
                //objThis.doFunctionCall(AppConst.IMAGE_GALLERY_PANEL,'showExpandedImage',1);
            });

            $(objComp.element).find('[id="prevExpandedImg"]').unbind('click').bind('click', function() {
                objComp.showExpandedImage(-1);
                //objThis.doFunctionCall(AppConst.IMAGE_GALLERY_PANEL,'showExpandedImage',-1);
            });

            var totalImages = $('.imageInnerContainer').length;
            var imageContainerW = ($('.imageInnerContainer').width() + parseInt($('.imageInnerContainer').css('margin-left')) + parseInt($('.imageInnerContainer').css('margin-right'))) * totalImages;
            $("#imageOuterContainer").width(imageContainerW);
            var moveLeftBy = (imageContainerW / totalImages) * 3;
            var imgPrevComp = $(objComp.element).find('[type="imageGalleryPrev"]').data('buttonComp');
            var imgNextComp = $(objComp.element).find('[type="imageGalleryNext"]').data('buttonComp');
            imgPrevComp.disable();

            $(objComp.element).find('[type="imageGalleryPrev"]').unbind(imgPrevComp.events.CLICK).bind(imgPrevComp.events.CLICK, function() {
                objThis.doFunctionCall(AppConst.IMAGE_GALLERY_PANEL, 'navigateToPrevImages');
            });

            $(objComp.element).find('[type="imageGalleryNext"]').unbind(imgNextComp.events.CLICK).bind(imgNextComp.events.CLICK, function() {
                objThis.doFunctionCall(AppConst.IMAGE_GALLERY_PANEL, 'navigateToNextImages');
            });

            break;
        default:
            //console.error('### ERROR: ['+ strType +'] -- Either "type" is not defined or Invalid operator is assigned');
            break;
    }

    return MG.BaseOperator.prototype.attachComponent.call(this, objComp);
}

MG.ImageGalleryOperator.prototype.scrollViewRenderCompleteHandler = function(objComp) {
    var objThis = this;
    var arrPageComps = $(objComp.element).find('[data-role="pagecomp"]');
    var objPageRole = null;
    for (var i = 0; i < arrPageComps.length; i++) {
        objPageRole = $(arrPageComps[i]).data("pagecomp");
        $(objPageRole).bind(objPageRole.events.PAGE_CONTENT_LOAD_COMPLETE, function(e) {
            $(e.currentTarget.element).find('[data-linktype="imagegallery"]').each(function() {
                $(this).css('cursor', 'pointer');
                $(this).unbind('click').bind('click', function() {
                    objThis.doFunctionCall(AppConst.IMAGE_GALLERY_PANEL, 'currentPageNumber', this);
                    objThis.doFunctionCall(AppConst.IMAGE_GALLERY_PANEL, 'showImageGalleryPanel', $(this).attr("data-galleryid"), objThis.imageGalleryRef);
                    if ($("#questionAnswerPanel").css('display') == "block") {
                        $("#questionAnswerPanel").find('[id="questionPanelCloseBtn"]').trigger('click');
                    }
                    
                    if ($("#learnosityPanel").css('display') == "block") {
                        $("#learnosityPanel").find('[id="learnosityPanelCloseBtn"]').trigger('click');
                    }
                });

            });
        });
    }

    $(window).bind('resize', function() {
    	setTimeout( function (){
    		var imgGalleryHeight = window.height - $( "#imageGalleryPanel #imageGalleryHeader" ).height() - 
							   	   $( "#imageGalleryPanel #imageGalleryThumbnailContainer" ).height() - 
							   	   parseInt( $( "#imageGalleryPanel #imageGalleryImageContainer" ).css( "padding" ) ) * 2 - 
								   $( "#imageGalleryPanel #imageGalleryFooter" ).height();
        $(objThis.imageGalleryRef.element).find("#imageGalleryImageContainer").height( imgGalleryHeight );
        objThis.doFunctionCall(AppConst.IMAGE_GALLERY_PANEL, 'resetImagePos');
        objThis.doFunctionCall(AppConst.IMAGE_GALLERY_PANEL, 'resetLoaderPos');
        var ref = $("#bookContentContainer").data('scrollviewcomp');
        ref.updateDesignForPageView("panelPosition", GlobalModel.currentScalePercent)
    	}, 100);
    });

    $(window).bind('orientationchange', function() {
        if ($(objThis.imageGalleryRef.element).css('display') == "block") {
            setTimeout(function() {
                var imgGalleryHeight = $(objThis.imageGalleryRef.element).height() - $(objThis.imageGalleryRef.element).find("#imageGalleryImageContainer").height();
                $(objThis.imageGalleryRef.element).find("#imageGalleryImageContainer").height(window.height -  $("#navigationBar").height() - imgGalleryHeight - 10);
                setTimeout(function() {
                    objThis.doFunctionCall(AppConst.IMAGE_GALLERY_PANEL, 'resetImagePos');
                }, 100);
                var nLeftPos = parseInt($(objThis.imageGalleryRef.element).find("[id=imageOuterContainer]").css('left'));
                nLeftPos = objThis.doFunctionCall(AppConst.IMAGE_GALLERY_PANEL, 'enableDisableNavBtns', nLeftPos);
                $(objThis.imageGalleryRef.element).find("[id=imageOuterContainer]").css('left', nLeftPos);
                setTimeout(function() {
                            objThis.doFunctionCall(AppConst.IMAGE_GALLERY_PANEL, 'slideThumbnailPanel');
                        },500);
            }, 500);
        }
    });

}

