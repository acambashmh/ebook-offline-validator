( function( $, undefined ) {
$.widget( "magic.helpAboutPopup", $.magic.magicwidget, 
{
	options: 
	{
		
	},
	
	_create: function()
	{
		
	},
	_init: function()
	{
        var objThis = this;
		$( document ).bind( "launchHelpAboutPopup" , function () {
			objThis.showPopup();
		} );
		
		$( "#helpAboutPopupOkBtn" ).bind( "click" , function () {
			PopupManager.removePopup();
		} );
	},
	
	onPanelOpen: function() {
		
	},
	
	onPanelClose: function() {
		 
	},
	
	showPopup : function ()
	{
		$( "#helpAboutPopup" ).find ( ".helpPopupTitle" ).html ( EPubConfig.Title );
		$( "#helpAboutPopup" ).find ( ".helpPopupSubject" ).html ( EPubConfig.Subject );
		$( "#helpAboutPopup" ).find ( ".helpPopupIdetifier" ).html ( EPubConfig.Identifier );
		$( "#helpAboutPopup" ).find ( ".helpPopupRights" ).html ( EPubConfig.Rights );
		$( "#helpAboutPopup" ).find ( ".helpPopupVersion" ).html ( GlobalModel.localizationData["READER_VERSION"] + currentReaderVersion );
		$( "#helpAboutPopup" ).find ( ".helpPopupAttribution" ).attr ( "href", getPathManager().getHelpAboutAttributionPath() );
		$( "#helpLauncher" ).trigger ( "click" );
		PopupManager.addPopup( $( "#helpAboutPopup" ), null, 
			{
				isModal : true,
				isCentered : true,
				popupOverlayStyle : "popupMgrMaskStyleSemiTransparent",
				isTapLayoutDisabled : true
			} );
	}
	
})
})( jQuery );
