(function($, undefined) {
	$.widget("magic.footNotePanelComp", $.magic.magicwidget, {
		isPageLoaded : false,
		options : {
			id : null,
			isXHTML:false,
			isJSONParsed:false
		},

		events : {},

		member : {
			_objPathManager : null,
			footNoteData : null
		},
		
		_create : function() 
		{
			$.extend(this.options, this.element.jqmData('options'));
		},
		
		_init : function() 
		{
			$.magic.magicwidget.prototype._init.call(this);
			var objThis = this;
			
			/*-----------------------------------------Footnote close button click event ---------------------------------------------- */
				
				$(this.element).find('[id=footNoteclsBtn]').unbind('click').bind('click', function() 
				{				  
					PopupManager.removePopup($(objThis.element).attr('id'));
				});
			/*------------------------------------------------------------------------------------------------------------------------ */
		},

		intializeView : function() {

		},

		/**
		 * This function loads the footnote JSON/XML/XHTML file
		 * @param {}
		 * @return {}
		 */
		loadFootNoteContent: function () {		    
			var objThis = this;
			var type = "GET";
			var url = getPathManager().getEPubFootNotePath();

			// If file type is JSON
			if (url.indexOf('.json') != -1) {
				$.getJSON(url, function(objData) {
					
					if (objData.content) {
						objData = objData.content;
					}
					objThis.footNoteDataLoadComplete(objData);
				});
			}
			// If file type is xml
			else if (url.indexOf('.xml') != -1) {
				$.ajax({
					type : "GET",
					url : url,
					success : function(data) {
						var strXML;

						if (window.ActiveXObject) {
							var xmlObject = new ActiveXObject("Microsoft.XMLDOM");
							xmlObject.async = false;
							strXML = data;
						} else {
							strXML = new XMLSerializer().serializeToString(data);
						}
						var footNoteData = $.xml2json(strXML);
						objThis.footNoteDataLoadComplete(footNoteData);
					}
				});
			}
			else if (url.indexOf('.xhtml') != -1) {
				$.ajax({
					type : "GET",
					url : url,
					success : function(data) {
						objThis.options.isXHTML = true;
						objThis.member.footNoteData = data;
					}
				});
			}
		},
		
		/*
		 * This function sets the footnoteData on footnote JSON/XML/XHTML load complete
		 * 
		 */
		footNoteDataLoadComplete : function(objData) {
			var objThis = this;
			objThis.member.footNoteData = objData;
		},
		
		/*
		 * This function returns the (is XHTML) type of footnote Data
		 * 
		 */
		isFootnoteXHTML : function() {
			var objThis = this;
			return objThis.options.isXHTML;
		},
		
		/**
		 * This function finds the footnote/note definition and sets it in required element if no error is encountered.
		 * If any error is encountered, it displays an appropriate error message.
		 * @param {Object} strId
		 * @param {String} type(footnote/note)
		 * @return {Boolean} Whether or not any error was encountered while setting the definition or not
		 */
		setFootNoteText : function(strId , type) {
			var objThis = this;
			var bfootNoteTermFound = false;
			var strFootNoteDef = "";
			var pageBreakId = "page_" + strId.split("#")[0];
			var footNoteId = "note_" + strId.split("#")[1];
							
			var fnDisplayFootNoteErrorAlertMessage = function(strLocalizationID) {
				$("#alertTxt").html(GlobalModel.localizationData[strLocalizationID]);
				setTimeout(function() {
					PopupManager.addPopup($("#alertPopUpPanel"), null, {
						isModal : true,
						isCentered : true,
						popupOverlayStyle : "popupMgrMaskStyleSemiTransparent",
						isTapLayoutDisabled : true
					});
				}, 0);
			}
			var strFootNoteErrorMessage = "FOOT_NOTE_DATA_ERROR";
			try {
				if(objThis.options.isXHTML)
				{
					var matchedData = $(objThis.member.footNoteData).find("#"+strId);
					$(matchedData).find("a").remove(":contains('return')");
					$(matchedData).find("p").css("white-space","normal");
					strFootNoteDef = $(matchedData).html();
				}
				else if(!objThis.options.isJSONParsed) 
				{
					objThis.options.isJSONParsed = true;
					objThis.member.footnoteData = objThis.parseFootnoteData(objThis.member.footNoteData.page);
					strFootNoteDef = objThis.member.footnoteData[pageBreakId][type][footNoteId];
				}
				else {
				    strFootNoteDef = objThis.member.footnoteData[pageBreakId][type][footNoteId];
				}
				if(strFootNoteDef != undefined)
		                	bfootNoteTermFound = true;
		        //if footnote term is missing
				if (bfootNoteTermFound == false) {
					strFootNoteErrorMessage = "FOOT_NOTE_TERM_MISSING";
					throw new Error("");
				}
				
				$(this.element).find('[id=footNoteTextDiv]').html(strFootNoteDef);
				return true;
			} catch (e) {
				fnDisplayFootNoteErrorAlertMessage(strFootNoteErrorMessage);
				return false;
			}
		},
		
		
		/**
		 * This function store and parse the footnote/note definition in required format (footnoteData[pageBreakId][type][footNoteId]).
		 * @param {Object} footnoteData
		 */
		parseFootnoteData : function(footnoteData){
			var data = {};
			if(footnoteData.length)
			{
				//if there are multiple foot note items
				for (var i = 0; i < footnoteData.length; i++) 
				{
					var pageid = "page_" + footnoteData[i].id.toLowerCase();
					data[pageid] = {};
					if (footnoteData[i].data.length) 
					{
						for (var j = 0; j < footnoteData[i].data.length; j++) 
						{
							var type = footnoteData[i].data[j].type.toLowerCase();
							data[pageid][type] = {};
							if (footnoteData[i].data[j].fn.length) 
							{
								for (var k = 0; k < footnoteData[i].data[j].fn.length; k++) 
								{
									var noteid = "note_" + footnoteData[i].data[j].fn[k].id.toLowerCase();
									data[pageid][type][noteid] = footnoteData[i].data[j].fn[k].def;
								}
							}
							else 
							{
								var noteid = "note_" + footnoteData[i].data[j].fn.id.toLowerCase();
								data[pageid][type][noteid] = footnoteData[i].data[j].fn.def;
							}
						}
					}
					else 
					{
							var type = footnoteData[i].data.type.toLowerCase();
							data[pageid][type] = {};
							if (footnoteData[i].data.fn.length) 
							{
								for (var k = 0; k < footnoteData[i].data.fn.length; k++) 
								{
									var noteid = "note_" + footnoteData[i].data.fn[k].id.toLowerCase();
									data[pageid][type][noteid] = footnoteData[i].data.fn[k].def;
								}
							} 
							else 
							{
								var noteid = "note_" + footnoteData[i].data.fn.id.toLowerCase();
								data[pageid][type][noteid] = footnoteData[i].data.fn.def;
							}
					}
				}
			}
			else 
			{
				var pageid = "page_" + footnoteData.id.toLowerCase();
				data[pageid] = {};
				if (footnoteData.data.length) 
				{
					for (var i = 0; i < footnoteData.data.length; i++) 
					{
						var type = footnoteData.data[i].type.toLowerCase();
						data[pageid][type] = {};
						if (footnoteData.data[i].fn.length) 
						{
							for (var j = 0; j < footnoteData.data[i].fn.length; j++) 
							{
								var noteid = "note_" + footnoteData.data[i].fn[j].id.toLowerCase();
								data[pageid][type][noteid] = footnoteData.data[i].fn[j].def;
							}
						} 
						else 
						{
							var noteid = "note_" + footnoteData.data[i].fn.id.toLowerCase();
							data[pageid][type][noteid] = footnoteData.data[i].fn.def;
						}
					}
				} 
				else 
				{
					var type = footnoteData.data.type.toLowerCase();
					data[pageid][type] = {};
					if (footnoteData.data.fn.length) 
					{
						for (var k = 0; k < footnoteData.data.fn.length; k++) 
						{
							var noteid = "note_" + footnoteData.data.fn[k].id.toLowerCase();
							data[pageid][type][noteid] = footnoteData.data.fn[k].def;
						}
					} 
					else 
					{
						var noteid = "note_" + footnoteData.data.fn.id.toLowerCase();
						data[pageid][type][noteid] = footnoteData.data.fn.def;
					}
				}
			}
			return data;
		}
	})
})(jQuery);
