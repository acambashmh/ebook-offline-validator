/**
 * A4 Sheet size is assumed to be 1240px;
 */

var heightRatio = 0;
var widthRatio = 0;
var finalRatio = 1;
var currentPageWidth = 0;
var currentPageHeight = 0;
var frameWidth = 0;
var frameHeight = 0;
(function($, undefined) {
    $.widget("magic.printPanelComp", $.magic.magicwidget, {

        member : {
            pagePrintArr : [],
            isCanceled : false, 
            /* Chrome
             a4SheetWidth: 840,
             a4SheetHeight: 1070
             */
            /* IE WRONG
             a4SheetWidth: 1300,
             a4SheetHeight: 1650
             */
            /* SAFARI
             a4SheetWidth: 840,
             a4SheetHeight: 1000
             */
            a4SheetWidth : 840,
            a4SheetHeight : 1070

        },

        _getPanel : function() {
            return this.getVar('objPanel');
        },

        _create : function() {

        },

        _init : function() {
            $.magic.magicwidget.prototype._init.call(this);
            self = this;
            self.updatePosition(self.element);
            $(window).bind('resize', function() {
                setTimeout(function() {
                    self.updatePosition();
                }, 600);
            });
            $(window).bind('orientationchange', function() {
                setTimeout(function() {
                    self.updatePosition();
                }, 600);
            });
            self.hideZoomButtons();

            $(this.element).find("#printRange").unbind("click").bind("click", function () {
                $(self.element).find("#range")[0].checked = true;
                $(self.element).find("#printRange").trigger("change");
            });
			
            $(this.element).find("#printRange").unbind("change keyup paste").bind("change keyup paste", function () {

                if ($(self.element).find(".printRangeInput").hasClass("invalidRange")) {
                    $(self.element).find(".printRangeInput").removeClass("invalidRange");
                }

                $(self.element).find("#printRange").focus();

            	if($.trim( $(this).val() ) == "")
            	{
            		$(self.element).find( "#printBtn" ).attr( "disabled", true );
    				$(self.element).find( "#printBtn" ).addClass( "ui-disabled" ).attr( "aria-disabled", true );
            	}
            	else
            	{
					$(self.element).find( "#printBtn" ).attr( "disabled", false );
        			$(self.element).find( "#printBtn" ).removeClass( "ui-disabled" ).attr( "aria-disabled", false );            		
            	}
                $(self.element).find( "#range" )[0].checked = true;
                $(self.element).find(".printError").css("display", "none");
               
                self.member.pagePrintArr = [];
                try
                {
	                var rangeTextArr = $.trim( $(this).val() ).split(",");
	                for(var i = 0; i < rangeTextArr.length ; i++)
	                {
	                	var pageArr = rangeTextArr[i].split("-");
	                	if(pageArr.length>2)
	                	{
	                		throw "Range error";
	                	}
	                	if( pageArr.length > 1 )
	                	{
	                		var startPage = self.getPageIndex($.trim(pageArr[0]));
                			var endPage = self.getPageIndex($.trim(pageArr[1]));
	                		if( startPage > endPage )
	                		{
	                			throw "Range error";
	                		}
                			do
                			{
                				if(self.member.pagePrintArr.indexOf(GlobalModel.ePubGlobalSettings.PageBreaks[startPage].pageName) == -1){
                					self.member.pagePrintArr.push(GlobalModel.ePubGlobalSettings.PageBreaks[startPage].pageName);
                				}
                				startPage = startPage + 1;
                				
                			}while( startPage <= endPage )
	                	}
	                	else
	                	{
	                		if(pageArr[0] != "")
	                		{
	                			var pageName = self.getPageName($.trim(pageArr[0]));
	                			if(self.member.pagePrintArr.indexOf(pageName) == -1)
	                				self.member.pagePrintArr.push(pageName);
	                		}
	                	}
	                }
	                if( self.member.pagePrintArr.length > 10 )
	                {
	                	$(self.element).find( ".printError" ).css("display","block");
                		$(self.element).find( ".printError" ).html("Upto 10 pages can be printed at a time.");
	                	$(self.element).find( "#printBtn" ).attr( "disabled", true );
	                	$(self.element).find("#printBtn").addClass("ui-disabled").attr("aria-disabled", true);
	                	$(self.element).find(".printRangeInput").addClass("invalidRange");
	                }
                }
                catch ( error )
                {
                	$(self.element).find( ".printError" ).css("display","block");
                	$(self.element).find( ".printError" ).html("Invalid Range.");
                	$(self.element).find( "#printBtn" ).attr( "disabled", true );
                	$(self.element).find("#printBtn").addClass("ui-disabled").attr("aria-disabled", true);
                	$(self.element).find(".printRangeInput").addClass("invalidRange");
                	
                }
            });
            
            $(this.element).find( "#printBtn" ).unbind( "click" ).bind( "click" , function() {
        		if($(this).hasClass("ui-disabled"))
            	{
            		return;
            	}
            	var selectedOption = $("input[type='radio'][name='printPage']:checked").attr("id");
            	$('#printBtnPanel').css('display','none');
            	$('#printBtn').removeClass('print-off').addClass('print-on');
            	$( $( "#bookContentContainer" ).data( 'scrollviewcomp' ) ).trigger( 'closeOpenedPanel' );
            	if( selectedOption == "currentPage" )
            	{
            		self.member.pagePrintArr = [];
    				var iPageIndex = parseInt($($('[type=PageContainer]')[GlobalModel.currentPageIndex]).attr('pagesequence'));
    				if (GlobalModel.pageBrkValueArr[iPageIndex] != 'start_blank')
    				{
	            		var currPageName = self.getPageName(GlobalModel.pageBrkValueArr[iPageIndex]);
	            		self.member.pagePrintArr.push(currPageName);
            		}
            		if ( GlobalModel.pageBrkValueArr[iPageIndex + 1] && GlobalModel.pageBrkValueArr[iPageIndex + 1] != 'end_blank')
            			var nextPageName = self.getPageName(GlobalModel.pageBrkValueArr[iPageIndex + 1]);
            		if( EPubConfig.pageView == 'doublePage' )
            		{
            			if ( nextPageName )
            				self.member.pagePrintArr.push(nextPageName);
            			if ( self.member.pagePrintArr.length > 1 )
            			{
            				self.printDoublePage(self.member.pagePrintArr);
            				
            				return;
            			}
            		}
            		self.printPages(self.member.pagePrintArr);
            	}
            	else
            	{
            		self.member.isCanceled = false;
            		$("#printPageCounterMsgTxt" ).text( "0/" + self.member.pagePrintArr.length + ".");
            		PopupManager.addPopup( $( "#printCancelPopUpPanel" ), null, {
			            isModal : true,
			            isCentered : true,
			            popupOverlayStyle : "popupMgrMaskStyleSemiTransparent",
			            isTapLayoutDisabled : true
			        } );
            		self.printPages(self.member.pagePrintArr);
            	}
            });
            $('#printCancelBtn').unbind( "click" ).bind( "click" , function() {
            	self.member.isCanceled = true;
            	PopupManager.removePopup();
            });
            $(this.element).find( "#currentPage" ).unbind( "click" ).bind( "click" , function() {
            	$(self.element).find( "#printBtn" ).attr( "disabled", false );
        		$(self.element).find( "#printBtn" ).removeClass( "ui-disabled" ).attr( "aria-disabled", false );
        		$(self.element).find(".printError").css("display", "none");
        		$(self.element).find("#printRange").val("");
        		if ($(self.element).find(".printRangeInput").hasClass("invalidRange")) {
        		    $(self.element).find(".printRangeInput").removeClass("invalidRange");
        		}
        		$(self.element).find("#printRange").blur();
            }); 
            
            $(this.element).find( "#range" ).unbind( "click" ).bind( "click" , function() {
            	$(self.element).find( "#printRange" ).trigger("change");
            });         
        },
		getPageName : function(pgBrkValue)
		{
			for(var i = 0; i < GlobalModel.ePubGlobalSettings.PageBreaks.length ; i++)
	        {
	        	if(GlobalModel.ePubGlobalSettings.PageBreaks[i].pageBreakValue.toLowerCase() == pgBrkValue.toLowerCase())
	        	{
	        		return GlobalModel.ePubGlobalSettings.PageBreaks[i].pageName;
	        		break;
	        	}
	        }
	        throw "Range error";
		},
		getPageIndex : function(strPage) {
		    var pageNumValue = -1;
		
		    for (var i = 0; i < GlobalModel.pageBrkValueArr.length; i++) {
		        if (strPage.toLowerCase() === GlobalModel.pageBrkValueArr[i].toLowerCase()) {
		            pageNumValue = i;
		            break;
		        }
		    }
			if(pageNumValue == -1)
				throw "Range error"
				
			if( EPubConfig.pageView == 'doublePage' )
				pageNumValue = pageNumValue - 1;
				
		    return pageNumValue;
		},
        hideZoomButtons : function() {
            if (ismobile || EPubConfig.Zoom_isAvailable == false) {
                $(".zoomTxtParent").css('display', 'none');
                $(".zoomSliderParent").css('display', 'none');
                $(this.element).css({
                    "top" : "0px"
                })
                $("#printBtnPanel").css({
                    "margin-top" : "-4px",
                    "height" : "81px !important"
                })
                $("#printPanelArrow").css({
                    "margin-top" : "-10px"
                })
            }
        },

        onPanelOpen : function() {
            //this.hideZoomButtons();
        },

        onPanelClose : function() {

        },

        updatePosition : function() {
            var printBtn = $("#printBtn");
            if ($(printBtn).offset().top + $("#printBtnPanel").height() > window.height) {
                if (!ismobile) {
                    $(this.element).parent().css('top', $(printBtn).offset().top);
                } else {
                    $(this.element).parent().css('top', $(printBtn).offset().top - 20);
                }
            } else {
                if (!ismobile) {
                    $(this.element).parent().css('top', $(printBtn).offset().top + 26);
                } else {
                    $(this.element).parent().css('top', $(printBtn).offset().top - 22);
                }
                if (EPubConfig.ReaderType == '7TO12' && EPubConfig.Theme == "default") {
                    //  $("#printBtnPanel").css('margin-top' , '-30px');
                }
            }
            $(this.element).parent().find('[id="zoomPanelArrow"]').css('margin-top', "37px");

            if (EPubConfig.AnnotationPanelPosition == "right") {
                if (EPubConfig.Theme == "blue") {
                    $(this.element).parent().css('right', $(printBtn).width() + 30);
                    $(this.element).parent().find('[id="zoomPanelArrow"]').css('margin-left', "214px");

                }
                if (EPubConfig.PrintForNotes_isAvailable == false) {
                    $(this.element).parent().find('[id="zoomPanelArrow"]').css('margin-left', "106px");
                    $(this.element).parent().css('right', '8px');
                }
            } else {
                $(this.element).parent().css('left', $(printBtn).width());
            }
            $(this.element).css('top', 0);

        },
		setZoom : function() {
			var cssObj = {};
			var heightRatio = (objThis.member.a4SheetHeight / EPubConfig.pageHeight);
            var widthRatio = (objThis.member.a4SheetWidth / EPubConfig.pageWidth);
			heightRatio = heightRatio.toFixed(2);
    		widthRatio = widthRatio.toFixed(2);
    		
            var nVal = (widthRatio > heightRatio) ? heightRatio : widthRatio;
            var scaleFunc = "scale(" + nVal + "," + nVal + ")";
            
            cssObj["-ms-transform"] = scaleFunc;
            cssObj["-moz-transform"] = scaleFunc;
            cssObj["-webkit-transform"] = scaleFunc;
            cssObj["-o-transform"] = scaleFunc;
            cssObj["-ms-transform-origin"] = "0% 0%";
            cssObj["-moz-transform-origin"] = "0% 0%";
            cssObj["-webkit-transform-origin"] = "0% 0%";
            cssObj["-o-transform-origin"] = "0% 0%";
            return cssObj;
		},
		
        printPages : function(elemToPrintArr) {
        	var objThis = this;
        	var pageCount = 0;
        	var cancelText = $( "#printCancelTxt" ).text();
        	$(objThis.element).find( "#printBtn" ).attr( "disabled", true );
			$(objThis.element).find( "#printBtn" ).addClass( "ui-disabled" ).attr( "aria-disabled", true );
			
        	var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
        	var isIE = false || document.documentMode;
        	var resultLength = elemToPrintArr.length ;
        	if( navigator.appVersion.indexOf("Windows NT 6.2") != -1 && navigator.userAgent.toLowerCase().indexOf('chrome') > -1)
        	{
        		objThis.member.a4SheetHeight = objThis.member.a4SheetHeight - 80;
        	}
            if (isSafari) {
                objThis.member.a4SheetHeight = 850;
            }
        	var elemPrint = '<iframe id="printFrame" frameBorder="0" style="width:1000px;height:1000px;display:none;"></iframe>';
        	
        	$('body').append(elemPrint);
        	var printType = '<style type="text/css" media="print">@page{size: portrait;margin-left: 2cm;}</style>';
            var printDocument = '<html><head></head><body><div id="printArea" style="width: '+ (( objThis.member.a4SheetWidth + 20 )) +'px;" >';
            
            if( isIE )
            {
            	resultLength = resultLength * 2;
            }
			printDocument += '</div></body></html>';
			var newWindow = document.getElementById("printFrame").contentWindow;
			var newDocument = newWindow.document;
			var hasError = false;
			var errorCounterArr = [];
			var d = new Date();
			var currentTime = d.getTime();
			newDocument.write(printDocument);
			for(var i=0;i<elemToPrintArr.length;i++)
			{
				$(newDocument).find('#printArea').append('<iframe onload="printFunction()" frameBorder="0" style="width: '+ objThis.member.a4SheetWidth +'px;height: '+objThis.member.a4SheetHeight+'px;float:left;" src="'+  getPathManager().getEPubPagePath(elemToPrintArr[i])+"?time="+ currentTime  +'"></iframe>');
				var object = new Object();
				object.page = elemToPrintArr[pageCount];
				object.errorCount = 0;
				errorCounterArr.push(object);
				if( isIE  )
				{
					$(newDocument).find('#printArea').append('<iframe frameBorder="0" style="width: '+ objThis.member.a4SheetWidth +'px;height: '+objThis.member.a4SheetHeight+'px;float:left;"></iframe>');
				}
			}
			
			newWindow.printFunction = function()
			{
				if( self.member.isCanceled == true )
				{
					PopupManager.removePopup();
					$('#printFrame').css('display','none');
					$("#printFrame").remove();
					$(objThis.element).find( "#printBtn" ).attr( "disabled", false );
					$(objThis.element).find( "#printBtn" ).removeClass( "ui-disabled" ).attr( "aria-disabled", false );
					return;
				}
				pageCount = pageCount + 1;
				$("#printPageCounterMsgTxt" ).text( pageCount + "/" + elemToPrintArr.length + ". ");
				if( pageCount == elemToPrintArr.length)
				{
					var cssObj = {};
					hasError = false;
					for(var i = 0;i< elemToPrintArr.length;i++)
					{
						var value;
						if( isIE )
						{
							value = $(newDocument.getElementsByTagName("iframe")[i*2]).contents().find('body').length	
						}
						else
						{
							value = $(newDocument.getElementsByTagName("iframe")[i]).contents().find('body').length
						}
						if( value == 0 )
						{
							if(errorCounterArr[i].errorCount == 2)
							{
								PopupManager.removePopup();
								$('#printFrame').css('display','none');
								$("#printFrame").remove();
								$(objThis.element).find( "#printBtn" ).attr( "disabled", false );
								$(objThis.element).find( "#printBtn" ).removeClass( "ui-disabled" ).attr( "aria-disabled", false );
								$( "#alertTxt" ).html( "Unable to print. Please try again." );
						        PopupManager.addPopup( $( "#alertPopUpPanel" ), null, {
						            isModal : true,
						            isCentered : true,
						            popupOverlayStyle : "popupMgrMaskStyleSemiTransparent",
						            isTapLayoutDisabled : true
						        } );
								
							}
							errorCounterArr[i].errorCount++;
							var d = new Date();
							var currentTime = d.getTime();
							pageCount = pageCount - 1;
							if( isIE )
							{
								$(newDocument.getElementsByTagName("iframe")[i*2]).replaceWith('<iframe frameBorder="0" onload="printFunction()" style="width: '+ objThis.member.a4SheetWidth +'px;height: '+objThis.member.a4SheetHeight+'px;float:left;" src="'+  getPathManager().getEPubPagePath(elemToPrintArr[i])+"?time="+ currentTime  +'"></iframe>');
							}
							else
							{
								$(newDocument.getElementsByTagName("iframe")[i]).replaceWith('<iframe frameBorder="0" onload="printFunction()" style="width: '+ objThis.member.a4SheetWidth +'px;height: '+objThis.member.a4SheetHeight+'px;float:left;" src="'+  getPathManager().getEPubPagePath(elemToPrintArr[i])+"?time="+ currentTime  +'"></iframe>');
							}
							hasError = true;
						}
					}
					if( !hasError )
					{
						var frameHeight = objThis.member.a4SheetHeight + 120;
						if( navigator.appVersion.indexOf("Windows NT 6.2") == -1 )
			        	{
			        		objThis.member.a4SheetHeight = objThis.member.a4SheetHeight + 50;
			        	}
			        	else if( navigator.appVersion.indexOf("Windows NT 6.2") != -1 && isIE )
			        	{
			        		objThis.member.a4SheetHeight = objThis.member.a4SheetHeight - 180;
			        	}
						$(newDocument).ready(function() {
			                cssObj = objThis.setZoom();
						});
						for(var i = 0;i< resultLength;i++)
						{
							$(newDocument.getElementsByTagName("iframe")[i]).contents().find("body").append('<div style="display: block !important;position: absolute;bottom: -50px;left: 0px;">'+EPubConfig.Rights+'</div>');
							$(newDocument.getElementsByTagName("iframe")[i]).contents().find("html").css("overflow","hidden");
							$(newDocument.getElementsByTagName("iframe")[i]).contents().find("html").css(cssObj);
							$(newDocument.getElementsByTagName("iframe")[i]).height(frameHeight);
						}						
						
						
						setTimeout(function() {
							if( isIE )
				        	{
				        		$('#printFrame').css('display','block');
				        		if( navigator.appVersion.indexOf("Windows NT 6.2") != -1 )
				        		{
				        			$(newDocument).find('#printArea').find('iframe:odd').remove();
				        		}
				        	}
							newWindow.focus();
							PopupManager.removePopup();
							if(!self.member.isCanceled)
							{
								newWindow.print();
							}
							
							if( navigator.appVersion.indexOf("Windows NT 6.2") != -1 && navigator.userAgent.toLowerCase().indexOf('chrome') > -1)
				        	{
				        		objThis.member.a4SheetHeight = objThis.member.a4SheetHeight + 80;
				        	}
				        	else if(navigator.appVersion.indexOf("Windows NT 6.2") == -1)
				        	{
				        		objThis.member.a4SheetHeight = objThis.member.a4SheetHeight -50;
				        	}
				        	else if( navigator.appVersion.indexOf("Windows NT 6.2") != -1 && isIE )
				        	{
				        		objThis.member.a4SheetHeight = objThis.member.a4SheetHeight + 180;
				        	}
							$('#printFrame').css('display','none');
							$("#printFrame").remove();
							$(objThis.element).find( "#printBtn" ).attr( "disabled", false );
							$(objThis.element).find( "#printBtn" ).removeClass( "ui-disabled" ).attr( "aria-disabled", false );
						},500);
					}
					
				}
			}			
			
			newDocument.close();
       	 },
       	 
         printDoublePage : function(elemToPrintArr) {
        	var objThis = this;
        	var hasError = false;
        	var pageCount = 0;
			var errorCounterArr = [];
			
        	$(objThis.element).find( "#printBtn" ).attr( "disabled", true );
			$(objThis.element).find( "#printBtn" ).addClass( "ui-disabled" ).attr( "aria-disabled", true );
			var isIE = false || document.documentMode;
        	var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
            if (isSafari) {
                objThis.member.a4SheetHeight = 900;
                objThis.member.a4SheetWidth = (objThis.member.a4SheetWidth / 2) + 100;
            }
        	var elemPrint = '<iframe id="printFrame" frameBorder="0" style="width:1000px;height:1000px;display:none;"></iframe>';
        	$('body').append(elemPrint);
        	
        	var printType = '<style type="text/css" media="print">@page{size: landscape;}</style>';
            var printDocument = '<html><head>'+ printType +'</head><body><div style="width: '+ (( objThis.member.a4SheetWidth + 160 )) +'px;" >';
			objThis.member.a4SheetWidth = (( objThis.member.a4SheetWidth + 160 ) / 2);
			objThis.member.a4SheetHeight = (objThis.member.a4SheetHeight / 2) + 30;
			for(var i = 0;i<elemToPrintArr.length;i++)
			{
				var d = new Date();
				var currentTime = d.getTime();
				printDocument += '<iframe frameBorder="0" onload="printFunction()" style="width: '+ objThis.member.a4SheetWidth +'px;height: '+objThis.member.a4SheetHeight+'px;float:left;" src="'+ getPathManager().getEPubPagePath(elemToPrintArr[i])+"?time="+ currentTime  +'"></iframe>'
				var object = new Object();
				object.page = elemToPrintArr[pageCount];
				object.errorCount = 0;
				errorCounterArr.push(object);
			}
			printDocument += '</div></body></html>';
			
			var newWindow = document.getElementById("printFrame").contentWindow;
			var newDocument = newWindow.document;			
			newDocument.write(printDocument)
			newDocument.close();
			var cssObj = {};
			
			$(newDocument).ready(function() {
                cssObj = objThis.setZoom();
			});
			
			newWindow.printFunction = function()
			{
				pageCount = pageCount + 1;
				if( pageCount == elemToPrintArr.length)
				{
					hasError = false;
					for(var i = 0;i<elemToPrintArr.length;i++)
					{
					if($(newDocument.getElementsByTagName("iframe")[i]).contents().find('body').length == 0)
					{
						if(errorCounterArr[i].errorCount == 2)
							{
								PopupManager.removePopup();
								$('#printFrame').css('display','none');
								$("#printFrame").remove();
								$(objThis.element).find( "#printBtn" ).attr( "disabled", false );
								$(objThis.element).find( "#printBtn" ).removeClass( "ui-disabled" ).attr( "aria-disabled", false );
								$( "#alertTxt" ).html( "Unable to print. Please try again." );
						        PopupManager.addPopup( $( "#alertPopUpPanel" ), null, {
						            isModal : true,
						            isCentered : true,
						            popupOverlayStyle : "popupMgrMaskStyleSemiTransparent",
						            isTapLayoutDisabled : true
						        } );
								
							}
							errorCounterArr[i].errorCount++;
							var d = new Date();
							var currentTime = d.getTime();
							pageCount = pageCount - 1;
							$(newDocument.getElementsByTagName("iframe")[i]).replaceWith('<iframe frameBorder="0" onload="printFunction()" style="width: '+ objThis.member.a4SheetWidth +'px;height: '+objThis.member.a4SheetHeight+'px;float:left;" src="'+  getPathManager().getEPubPagePath(elemToPrintArr[i])+"?time="+ currentTime  +'"></iframe>');
							hasError = true;
						}
					}
				if( !hasError )
				{	
					for(var i = 0;i<elemToPrintArr.length;i++)
					{
						$(newDocument.getElementsByTagName("iframe")[i]).contents().find("html").css("overflow","hidden");
	 					$(newDocument.getElementsByTagName("iframe")[i]).contents().find("html").css(cssObj);
	 					$(newDocument.getElementsByTagName("iframe")[i]).height(objThis.member.a4SheetHeight + 65);
	 				}
	 				$(newDocument).find("body").css('-moz-transform', 'rotate(90deg)');
					var isIE = false || document.documentMode;
	                if( isIE || isSafari )
	                {
	                	$(newDocument).find("body").css('top', '10%');
	                	$(newDocument).find("body").css('right', '10%');
	                	$(newDocument).find("body").css('left', '10%');
	                	$(newDocument).find("body").css('position','absolute');
	                	$(newDocument).find("body").css('transform', 'rotate(90deg)');
	                }
	                else
	                {
	                	$(newDocument).find("body").css('top', '0%');
						$(newDocument).find("body").css('left', '100%');
	                }
	                if (isSafari) {
	                	$(newDocument).find("body").css('top', '25%');
	                	$(newDocument).find("body").css('-webkit-transform', 'rotate(90deg)');
	                }
	                if ( $("#printFrame").contents().find("body").find('div').length == 1  )
	                {
						$("#printFrame").contents().find("body").append('<div style="display: block !important;top: 0px;position: relative;left: 0px;">'+EPubConfig.Rights+'</div>');
					}
					setTimeout(function() {
						if( isIE )
			        	{
			        		$('#printFrame').css('display','block');
			        	}
						newWindow.focus();
						PopupManager.removePopup();
						if(!self.member.isCanceled)
						{
							newWindow.print();
						}
						self.member.isCanceled = false;
						$('#printFrame').css('display','none');
		    			objThis.member.a4SheetWidth = ((objThis.member.a4SheetWidth * 2) - 160);
		    			objThis.member.a4SheetHeight = ((objThis.member.a4SheetHeight - 30) * 2);
		    			if (isSafari) {
		    				objThis.member.a4SheetWidth = 840;
		    			}
						$("#printFrame").remove();
						$(objThis.element).find( "#printBtn" ).attr( "disabled", false );
	    				$(objThis.element).find( "#printBtn" ).removeClass( "ui-disabled" ).attr( "aria-disabled", false );
					},500);
				}
			}
		}
       }
    });
})(jQuery);
