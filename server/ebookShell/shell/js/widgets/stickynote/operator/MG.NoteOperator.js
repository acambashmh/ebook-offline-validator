MG.NoteOperator = function()
{
	this.notePopup = null;
	this.scrollViewRef = null;
	this.contentWidth = 820;
	this.deleteFromPanel = false;
	this.noteToEdit = -1;
	this.androidLandscapeHeight = 768;
	this.isOpenNoteForEdit = false;
	this._currentSelectionClass = null;
	this.oldObject = null;
	this.editFromPanel = false;
};

MG.NoteOperator.prototype = new MG.BaseOperator();
	
MG.NoteOperator.prototype.attachComponent = function( objComp )
{
	if ( objComp == null )
	{
		return;
	}

    var _self = this, _type = objComp.element.attr( AppConst.ATTR_TYPE ).toString();
    
    switch ( _type )
    {
    	case AppConst.SCROLL_VIEW_CONTAINER:
    	   if( EPubConfig.ReaderType == "6TO12" )
    	   {
    	       
                //$('.mystudentEdition').text("Mrs. Bellows");
                var abc = new TEController( $("#teacherStudentPanelBtn") );
    	   }
	    	if ( navigator.userAgent.match(/(android)/i) ) 
	    	{
	            setTimeout( function ()
	            { 
	            	_self.androidLandscapeHeight = ( window.height > window.width ) ? window.width : window.height;
	            }, 1000 );
	            
	        }
	        
	        if ( _self.notePopup == null )
            {
            	 _self.notePopup = new NotePopup();
            }
	        _self.addListeners();
    		_self.scrollViewRef = objComp;
    		$( objComp ).bind( objComp.events.PAGE_RENDER_COMPLETE, function () 
    		{
                _self.scrollViewRenderCompleteHandler( objComp );
            } );
            
            
    	break;
    	
    	/* Notes click on Annotation bar */
    	case AppConst.NOTES_ANNOTATION_BTN:
    		if( isWinRT )
            {
				var eventListener = function( evt )
				{
				    _self.noteAnnoIconClickHandler( evt );
				};
			    var target = document.getElementById( "notes-anno" );
			    target.addEventListener( WinRTPointerEventType, eventListener );
            }
            else
            {
            	$( objComp.element ).bind( "click tap", function ( event ){
            		_self.noteAnnoIconClickHandler( event  );
            	} );
            }
    	break;
    	
    	/* Sticky note popup */
    	case AppConst.STICKYNOTE_CONTAINER:
    		this.notePopup = new NotePopup ();
    	break;
    	
    	/* Sticky note/highlight left panel */
    	case AppConst.NOTE_SECTION_PANEL:
    	
    	    var addedNotes = $(objComp.element).find('[id = addedStickyNotes ]')[0];
    	    var addedTeacherNotes = $(objComp.element).find('[id = addedTeacherNotes ]')[0];

	    	$( objComp ).bind( 'addDataToStickyNoteSectionPanel' , function ( e , stickyNotesData )
	    	{
	    	    $(addedNotes).html(stickyNotesData);    	    

	    	    _self.bindNotesEvent(addedNotes, "studentnotes", objComp);
	           
	      	});
	      	
	    	$(objComp).bind('addDataToTeacherNoteSectionPanel', function (e, teacherNotesData) {
	    	    
	    	    $(addedTeacherNotes).html(teacherNotesData); 
	    	   
	    	    _self.bindNotesEvent(addedTeacherNotes, "teachernotes", objComp);

	    	    /*-----------------------------------------Left Panel teacher note edit event ---------------------------------------------- */
	    	    $(objComp.element).find('.teacherAnnotationData').unbind('click').bind('click', function (e) {
	    	        _self.editFromPanel = true;
	    	        $(this).parent().removeClass("panelDataUnRead");
	    	        var currentListId = $(this).parent().attr('id').replace('stickynoteComment_', '');	
	    	        var noteObj = _self.findNoteObjectInGlobalModel( currentListId );
	    	        if( noteObj.shared_with )
	    	        {
	    	            if( noteObj.type == "stickynote" )
	    	            {
	    	            	_self.editNote("tseselect", currentListId, $(this).parent().attr('page'));	
	    	            }
	    	            else
	    	            {
	    	            	_self.editNote("selection", currentListId, $(this).parent().attr('page'));
	    	            }
	    	        }
	    	        else
	    	        {
	    	        	 if( noteObj.type == "stickynote" )
	    	        	 {
	    	        	 	_self.editNote("noteselection_", currentListId, $(this).parent().attr('page'));
	    	        	 }
	    	             else
	    	             {
	    	             	_self.editNote("selection", currentListId, $(this).parent().attr('page'));
	    	             }
	    	        }
	    	    });

	    	});

            

	      	var notesTab = $( objComp.element ).find( '[ id = "annotationMyNotesTab" ]' );
	      	var qaTab = $(objComp.element).find('[ id = "annotationMyQuestionsTab" ]');
	      	var teacherNotesTab = $(objComp.element).find('[ id = "annotationTeacherNotesTab" ]'); 
	      	/*-----------------------------------------Left Panel NotesTab click event ---------------------------------------------- */
	      	notesTab.unbind( 'click' ).bind( 'click' , function ( ) 
	      	{   
	      		if( notesTab.hasClass( 'leftPanelTabActive' ) )
	      		{
	      			return;
	      		}
	      	    //annotationAllTab
                notesTab.addClass( 'leftPanelTabActive' ).removeClass( 'leftPanelTab' );
                qaTab.addClass('leftPanelTab').removeClass('leftPanelTabActive');
                teacherNotesTab.addClass('leftPanelTab').removeClass('leftPanelTabActive');
                
                $(objComp.element).find('[id=addedStickyNotesList]').css('display', 'block');
                $(objComp.element).find('[id=addedQuestionsList]').css('display', 'none');
                $(objComp.element).find('[id=addedTeacherNotesList]').css('display', 'none'); 
                $($(objComp.element).find('[id="blankTeacherNotesPanelText"]')).css("display", "none");

                $($(objComp.element).find('[id="blankQuestionAnswerPanelText"]')).css("display", "none");

                $(objComp.element).find('[id=teacherAnnotationSortBy]').css('display', 'none');
                $($(objComp.element).find("#dd-clickable")[0]).css("display", "block");
                $(objComp.element).find('[id=teacher-dd-clickable]').css('display', 'none');

                if ($.trim( $(addedNotes).html() ) == "") 
                {
                    $($(objComp.element).find('[id="blankNotesPanelText"]')).css("display", "block");
                    $(objComp.element).find('[id=annotationSortBy]').css('display', 'none');
                } 
                else 
                {
                    $($(objComp.element).find('[id="blankNotesPanelText"]')).css("display", "none");
                    $(objComp.element).find('[id=annotationSortBy]').css('display', 'block');
                    if (EPubConfig.My_Notebook_isAvailable)
	                {
	                    $(objComp.element).find('[id=externalURLLink]').css('display', 'block');
	                }
	                
                }
                GlobalModel.updateTeacherNotificationIcon('teacherNoteAlert');
                $(objComp.element).data( 'annotationPanelComp' ).onPanelOpen();
                $(objComp.element).data( 'annotationPanelComp' ).updateViewSize();
	      	});    	   
    	    /*------------------------------------------------------------------------------------------------------------------------ */

    	    /*-----------------------------------------Left Panel TeacherNotesTab click event ---------------------------------------------- */
	      	teacherNotesTab.unbind('click').bind('click', function ()
	      	{
	      		if( teacherNotesTab.hasClass( 'leftPanelTabActive' ) )
	      		{
	      			return;
	      		}
	      	    var teacherNoteAlert = $("#teacherNoteAlert");
	      	    teacherNoteAlert.css("display", "none");	      	   
	      	    teacherNotesTab.addClass('leftPanelTabActive').removeClass('leftPanelTab');	      	    
	      	    $(objComp.element).find('[id=annotationSortBy]').css('display', 'none');
	      	    $($(objComp.element).find("#dd-clickable")[0]).css("display", "none");
	      	    

	      	    qaTab.addClass('leftPanelTab').removeClass('leftPanelTabActive');
	      	    notesTab.addClass('leftPanelTab').removeClass('leftPanelTabActive');
             
	      	    $(objComp.element).find('[id=addedStickyNotesList]').css('display', 'none');
	      	    $(objComp.element).find('[id=addedQuestionsList]').css('display', 'none');
	      	    $(objComp.element).find('[id=addedTeacherNotesList]').css('display', 'block');
	      	    $($(objComp.element).find('[id="blankNotesPanelText"]')).css("display", "none");
                
	      	    $($(objComp.element).find('[id="blankQuestionAnswerPanelText"]')).css("display", "none");

	      	    if ( $(addedTeacherNotes).find( 'div' ).length > 0  ) {
	      	        $($(objComp.element).find('[id="blankTeacherNotesPanelText"]')).css("display", "block");	      	        
	      	    }
	      	    else {
	      	        $($(objComp.element).find('[id="blankTeacherNotesPanelText"]')).css("display", "none");	      	       
	      	        if (EPubConfig.My_Notebook_isAvailable) {
	      	            $(objComp.element).find('[id=externalURLLink]').css('display', 'block');
	      	        }

	      	    }	      	    

	      	    $(objComp.element).data('annotationPanelComp').initTeacherNodes();
	      	    $(objComp.element).data('annotationPanelComp').updateViewSize();
	      	    

	      	});

    	    /*------------------------------------------------------------------------------------------------------------------------ */

    	break;
    }
};
	
/*-------------------------------------------------------------- Handlers start--------------------------------------------------------------------------*/

MG.NoteOperator.prototype.addListeners = function ()
{
	var self = this;
	    
    $( window ).bind( 'orientationchange', function() 
    {
    	document.onselectionchange = null;
        $("#annotation-bar").css("display","none");
        $("body").append('<input type="text" id="inputToRemove" style="width:0;height:0"/>');
        $("#inputToRemove").focus();
        $("#inputToRemove").blur();
        $("#inputToRemove").remove();
        
    } );
    
    self.bindEventsOfPopup();
};

MG.NoteOperator.prototype.scrollViewRenderCompleteHandler = function( objComp )
{
	var self = this;
    var _arrPageComps = $( objComp.element ).find( '[ data-role = "pagecomp" ]' );
    var _objPageRole = null;
    var index = 0;
    var selectionClass = "";
    var range = null;
    var selectioncolor = null;
    
    for ( var i = 0; i < _arrPageComps.length; i++ ) 
    {
        _objPageRole = $( _arrPageComps[ i ] ).data( "pagecomp" );

        $( _objPageRole ).bind( "createHighlightOnPage", function( e ) 
        {
            GlobalModel.annotations.sort( function( a, b ) 
			{
				var c = a.annotationID;
				var d = b.annotationID;
				return c - d;
			} );
            for( var i = 0; i < GlobalModel.annotations.length; i++ )
            {
            	if ( GlobalModel.annotations[ i ].type == 'stickynote' ) 
            	{
            	    range = GlobalModel.annotations[ i ].range;
	            	selectionClass = "noteselection_";
	            	if( GlobalModel.annotations[ i ].shared_with )//GlobalModel.BookEdition == "TSE" && GlobalModel.selectedStudentData && GlobalModel.annotations[ i ].title.length == 0 )
	            	{
	            	    selectionClass = "tseselect";
	            	}
	           	}
	           	else
	           	{
	           	    range = GlobalModel.annotations[ i ].rangeObject;
	           	    selectioncolor = GlobalModel.annotations[ i ].selectioncolor;
	           	    selectionClass = "selection";
	           	}
	           	index = self.scrollViewRef.member.curPageElementsPgBrk.indexOf( GlobalModel.annotations[ i ].pageNumber );
                if ( EPubConfig.pageView == 'doublePage' ) 
                {
                    if ( GlobalModel.currentPageIndex == index || index == GlobalModel.currentPageIndex + 1 ) 
                    {
                        self.createNoteHighlight( selectionClass, GlobalModel.annotations[ i ], range, selectioncolor );
                    }
                }
                else if( GlobalModel.currentPageIndex == index )
                {
                    self.createNoteHighlight( selectionClass, GlobalModel.annotations[ i ], range, selectioncolor );
                }
            }
        } );
        
        $( _objPageRole ).bind( "addEventOnStickyNoteIcon", function ()
        {
            self.addEventOnIcon();
        } );
	}
};

MG.NoteOperator.prototype.createNoteHighlight = function ( selectionClass, noteObj, objRange, selectioncolor )
{
	var objThis = this;
    var rangedata;
    var strNoteSelectionID = noteObj.id;
    setTimeout( function() {
        
        var classApplier = null;
        var Obj = $.xml2json( objRange );
        var startC = $( '[id = "' + Obj.startContainer + '"]' ).find( '#mainContent' )[ 0 ];
        var startO = parseInt( Obj.startOffset );
        var endO = parseInt( Obj.endOffset );
        var savedSel = {
            start : startO,
            end : endO
        };
        
        try {
            rangedata = objThis.restoreSelection( $( startC )[ 0 ], savedSel );
            rangy.init();
            var cssClassApplier = rangy.createCssClassApplier( selectionClass + strNoteSelectionID );
            var cssClassAppliercolor = rangy.createCssClassApplier( selectionClass.replace( "_", "" ) );
            if( noteObj.shared_with )
            {
                classApplier = rangy.createCssClassApplier( selectionClass );
            }
            window.getSelection().removeAllRanges();
            window.getSelection().addRange( rangedata );
            if ( $( startC ).find( "." + selectionClass + strNoteSelectionID )[ 0 ] == undefined ) {
                cssClassApplier.toggleSelection();
                if( noteObj.shared_with )
                {
                    classApplier.toggleSelection();
                }
                
                if( selectionClass.match( "noteselection" ) != null )
                {
                    cssClassAppliercolor.toggleSelection();
                }
            }
            $( "." + selectionClass + strNoteSelectionID ).attr( "note-id", strNoteSelectionID );
            if( selectionClass.match( "noteselection" ) == null && selectionClass.match( "tseselect" ) == null )
            {
                $( "." + selectionClass + strNoteSelectionID ).addClass( selectioncolor );
            }
            else
            {
            	selectioncolor = "noteselection"
            }
            objThis.updateAnnotationAttr( selectionClass + strNoteSelectionID , selectioncolor );
            
        } catch(e) {
				console.log( e.message );
        }
        
        objThis.addEventOnHighlightAndIcon( selectionClass, strNoteSelectionID );

    }, 0);
};

MG.NoteOperator.prototype.addEventOnHighlightAndIcon = function ( selectionClass, id )
{
	var self = this;
	setTimeout( function() {
		/*
		 	Removing the click event of note selection
		 */
		//if( !$( "." + selectionClass + id ).hasClass('noteselection') )
		$( "anno" ).not( "[class='']" ).unbind( 'click' ).bind( 'click', function( e ) 
	    {
	    	if( $(this).attr("annoArr") == undefined )
	    	{
	    		return;
	    	}
	    	var isNotClickable = true;
	    	var annoArr = $(this).attr("annoArr").split(",");
	    	var len = annoArr.length;
	    	for( var index = len - 1; index >=0; index -- )
	    	{
	    		var idColorArr = annoArr[ index ].split(":");
	    		if( idColorArr[1].indexOf( 'selectioncolor' ) != -1 )
	    		{
	    			isNotClickable = false;
	    			id = idColorArr[0].replace( "selection" , "" );
	    			selectionClass = "selection";
	    			break;
	    		}
	    	}
	    	if ( self.scrollViewRef.options.rangedata || isNotClickable )
	    	{
	    		//creating new highlight
	    		return;
	    	}
	    	var element = $( $( e.target )[ 0 ] );
	    	if( ( ( element.closest( "[type = glossterm]" ).length > 0 ) || ( element.closest( "[type = footnote]" ).length > 0 ) ||
	    	  ( element.closest( "[type = internal-link]" ).length > 0 ) || ( element.closest( "[data-ajax = false]" ).length > 0 ) ) && 
	    	  self.editFromPanel == false )
	    	{
	    		return;
	    	}
	    	
	    	if( GlobalModel.hasDPError( "notes" ) )
	    	{
	    		return;
	    	}
	    	e.preventDefault();
	        e.stopPropagation();
	        self._currentSelectionClass = selectionClass + id;
	        
	        self.noteHighlightClickHandler( "#btnStickyNote_" + id, self._currentSelectionClass  );
	        self.editFromPanel = false;
		} );
		
		self.checkIfIconIsToBeRepositioned( selectionClass, id );
		self.addEventOnIcon();
        if ( self.noteToEdit != -1 && self.noteToEdit == id) 
        {      
            if( $( "." + selectionClass + self.noteToEdit ).length > 0 )
            {
                $($( "#btnStickyNote_" + self.noteToEdit )[ 0 ] ).trigger( "click" );
                self.noteToEdit = -1;
            }
        }
	}, 100);
	
	
    window.getSelection().removeAllRanges();
};

MG.NoteOperator.prototype.checkIfIconIsToBeRepositioned = function ( selectionClass, id )
{
	var self = this;
	if( selectionClass == "selection" )
	{
		var obj = self.findNoteObjectInGlobalModel( id );
		
		if( obj.icontop == 0 && obj.iconleft == 0 && ( obj.comments || obj.teachersSharedWith ) )
		{
			self.repositionNoteIcon( obj, "." + selectionClass );
		}
	}
};

MG.NoteOperator.prototype.addEventOnIcon = function ()
{
    var self = this;

    $( ".btnStickyNoteHotspot" ).unbind( 'click' ).bind( 'click', function( evt ) 
	{
	    var id = $( evt.target ).attr( "id" );
	    id = id.replace( "btnStickyNote_", "" );
	    var obj = self.findNoteObjectInGlobalModel( id );
	    var str = "";
	    if( obj.type == "stickynote" )
	    {
	    	if( !obj.shared_with )
		    {
		        str = "noteselection_";
		    }
		    else
		    {
		        str = "tseselect";
		    }
	    }
	    else
	    {
	    	str = "selection";
	    }
		self.noteHighlightClickHandler( evt.target, str + id );
	} );
};

MG.NoteOperator.prototype.noteHighlightClickHandler = function ( btnStickyNote, selectionClass )
{
    var self = this;
    self.notePopup.showPopup( false, $( btnStickyNote ).attr( "id" ), selectionClass, self.isOpenNoteForEdit, self.editFromPanel );
    self.isOpenNoteForEdit = false;
};

MG.NoteOperator.prototype.restoreSelection = function( containerEl, savedSel ) 
{
    var charIndex = 0, range = document.createRange();
    range.setStart( containerEl, 0 );
    range.collapse( true );
    var nodeStack = [ containerEl ], node, foundStart = false, stop = false;
    while ( !stop && ( node = nodeStack.pop() ) ) 
    {
        if ( node.nodeType == 3 ) 
        {
            var nextCharIndex = charIndex + node.length;

            if ( !foundStart && savedSel.start >= charIndex && savedSel.start <= nextCharIndex ) 
            {
                range.setStart( node, savedSel.start - charIndex );
                foundStart = true;

            }
            if ( foundStart && savedSel.end >= charIndex && savedSel.end <= nextCharIndex ) 
            {
                range.setEnd( node, savedSel.end - charIndex );
                stop = true;
            }
            charIndex = nextCharIndex;
        } 
        else 
        {
            var i = node.childNodes.length;
            while ( i-- ) 
            {
                nodeStack.push( node.childNodes[ i ] );
            }
        }
    }

    var sel = window.getSelection();
    sel.removeAllRanges();
    sel.addRange( range );
    return range;
};

MG.NoteOperator.prototype.noteAnnoIconClickHandler = function( event )
{
	var self = this;
	addNoteHighlight = function ()
	{
		if ( self.scrollViewRef.options.rangedata ) 
        {
            window.getSelection().removeAllRanges();
            window.getSelection().addRange( self.scrollViewRef.options.rangedata );
        }
        rangy.init();
       /* if( GlobalModel.BookEdition == "TSE" && GlobalModel.selectedStudentData )
        {
            self._currentSelectionClass = "tseselect" + GlobalModel.totalStickyNotes;
            var cssClassAppliercolor = rangy.createCssClassApplier( self._currentSelectionClass );
            var cssClassApplier = rangy.createCssClassApplier( "tseselect" );   
            window.getSelection().addRange(self.scrollViewRef.options.rangedata);
            cssClassAppliercolor.toggleSelection();
            cssClassApplier.toggleSelection();
            
        }  
        else
        { */
            if( !EPubConfig.DEFAULT_HIGHLIGHT || EPubConfig.ReaderType != "6TO12"  )
            {
                 self._currentSelectionClass = "noteselection_" + GlobalModel.totalStickyNotes;
                 rangy.createCssClassApplier( "noteselection_" + GlobalModel.totalStickyNotes ).toggleSelection();
                 rangy.createCssClassApplier( "noteselection" ).toggleSelection();	
            }
            else
            {
                self._currentSelectionClass = "selection" + GlobalModel.totalStickyNotes;
                var cssClassApplier = rangy.createCssClassApplier( self._currentSelectionClass );
                window.getSelection().addRange(self.scrollViewRef.options.rangedata);
                cssClassApplier.toggleSelection();
				
				var selectionArr = $( "." +self._currentSelectionClass );
				for(var i = 0;i < selectionArr.length; i++)
				{
					var classArr = $( selectionArr[i] ).attr('class').split(' ')
					for(var j = 0;j < classArr.length; j++)
					{
						if( classArr[j].indexOf('selectioncolor') != -1 )
						{
							$( selectionArr[i] ).removeClass( classArr[j] );
						}
					}
				}
                $( "." +self._currentSelectionClass ).addClass( "selectioncolor" + 
                                      ( EPubConfig.Highlight_colors.indexOf( EPubConfig.Highlights_default_color ) + 1 ) );
            }
       // }      
	};
	
	if( GlobalModel.hasDPError( "notes" ) )
	{
		return;
	}

	var iPageNum = self.scrollViewRef.options.pageIndexForAnnotation;
	var _arrStickyNotesOnPage = $( "#PageContainer_" + iPageNum ).find( '.btnStickyNoteHotspot' );

    if ( _arrStickyNotesOnPage.length < EPubConfig.Notes_Per_Page || EPubConfig.Notes_Per_Page < 1 ) 
    {    
    	var offset = $( event.currentTarget ).offset();
    	var arrPos = new Object();
    	var lastAddedIcon = null;
    	
        arrPos.top = this.scrollViewRef.options.Icontop;
        if ( arrPos.top < 0 ) 
        {
           arrPos.top = event.pageY;
        }
        arrPos.left = this.scrollViewRef.options.Iconleft;
        arrPos.contentWidth = this.scrollViewRef.options.contentWidth;
        this.setElementPosition( offset, arrPos );
        addNoteHighlight();
     /*   if( GlobalModel.BookEdition == "TSE" && GlobalModel.selectedStudentData )
        {
            var className = ' class="btnStickyNoteHotspot btnStickyNoteStandaloneTeacherComment" ';
        }
        else
        { */
            var className = ' class="btnStickyNoteHotspot ';
       // }
        var newComponentDiv = '<div id="btnStickyNote" type="stickyNoteComp" style="position:absolute;"' + 
        					  className + ' moveable="true" authorable="true" ' + 
        					  ' data-options={"expandedWidth":"51","expandedHeight":"50","width":"43","height":"41"}>' + '</div>';
    	var pageNum = this.scrollViewRef.options.pageIndexForAnnotation;
		$( "#bookContentContainer2" ).find( '[id = "PageContainer_' + pageNum + '"]' ).append( newComponentDiv );
		
		lastAddedIcon = $( "#btnStickyNote" );
		if( EPubConfig.DEFAULT_HIGHLIGHT && EPubConfig.ReaderType == "6TO12" )// && !( GlobalModel.BookEdition == "TSE" && GlobalModel.selectedStudentData ) )
        {
            lastAddedIcon.css( "display" , "none" );
        }
		lastAddedIcon.css( "top", CompModel.yPos );
		lastAddedIcon.css( "left", CompModel.xPos );
		
		var isContainerScroll = self.adjustPageBeforeNoteAdd( lastAddedIcon, lastAddedIcon.css( 'left' ), lastAddedIcon.css( 'top' ) );
		if ( isContainerScroll == true ) 
		{
            objThis.member.isContainerScrollBool = true;
        } 
        else 
        {
            objThis.member.isContainerScrollBool = false;
            
            if ( ismobile )
            {
            	document.onselectionchange = null;
                $("#annotation-bar").css("display","none");
                $("body").append('<input type="text" id="inputToRemove" style="width:0;height:0"/>');
                $("#inputToRemove").focus();
                $("#inputToRemove").blur();
                $("#inputToRemove").remove();   
            }
        }
        setTimeout( function () { 
                self.notePopup.showPopup( true, lastAddedIcon.attr( "id" ), self._currentSelectionClass, null );
            }, 0 );
    }
    else
    {
    	PopupManager.removePopup();
    	window.getSelection().removeAllRanges();
    	$("body").append('<input type="text" id="inputToRemove" style="width:0;height:0"/>');
        $("#inputToRemove").focus();
        $("#inputToRemove").blur();
        $("#inputToRemove").remove();
    	$( "#alertTxt" ).html( GlobalModel.localizationData[ "MAX_NOTE_ANNOTATION_ALLOWED_PER_PAGE" ] );
        setTimeout( function() {
        	PopupManager.addPopup( $( "#alertPopUpPanel" ), null, {
            isModal : true,
            isCentered : true,
            popupOverlayStyle : "popupMgrMaskStyleSemiTransparent",
            isTapLayoutDisabled : true
        } );
        }, 0 );
    }
};

MG.NoteOperator.prototype.bindEventsOfPopup = function ()
{
	var self = this;
	$( self.notePopup ).bind( "showDeleteConfirmationPopup", function ( event, selectionClass )
	{
		self.openDeleteConfirmPanel( selectionClass );
	} );
	
	$( self.notePopup ).bind( "showHighlightDeleteConfirmationPopup", function ( event, selectionClass )
	{
		self.openDeleteConfirmPanel( selectionClass, "highlight" );
	} );
	
	$( self.notePopup ).bind( "showCommentDeletionConfirmationPopup", function ( event, selectionClass )
    {
        self.openCommentDeletionPanel( selectionClass );
    } );
	
	/* Save new note. This event is dispatched from NotePopup */
	$( self.notePopup ).bind( "saveNewNote", function ( event, data )
	{
		self.createNewNote( data );
		if( !data.isSwapping )
		{
		    GlobalModel.totalStickyNotes++;
		}
		
	} );
	
	$( self.notePopup ).bind( "repositionIcon", function ( event, data )
	{
		self.repositionNoteIcon( data, ".selection" );
	} );
	
	$( self.notePopup ).bind( "saveNewHighlight", function ( event, data )
    {
        self.createNewHighlight( data );
        if( !data.isSwapping )
        {
            GlobalModel.totalStickyNotes++;
        }
    } );
	
	/* Modify already existing note. This event is dispatched from NotePopup */
	$( self.notePopup ).bind( "updateExistingNote", function ( event, data )
	{
		self.modifyNote( data );
	} );
	
	$( self.notePopup ).bind( "addCommentToNote", function ( event, data )
    {
        self.addCommentToNote( data );
    } );
    
    $( self.notePopup ).bind( "updateCommentToNote", function ( event, data )
    {
        self.updateCommentToNote( data );
    } );
    
    $( self.notePopup ).bind( "addCommentToHighlight", function ( event, data )
    {
    	self.repositionNoteIcon( data, ".selection" );
        self.addCommentToNote( data );
    } );
    
    $( self.notePopup ).bind( "updateCommentToHighlight", function ( event, data )
    {
        self.updateCommentToNote( data );
    } );
    
	$( self.notePopup ).bind( "deleteAnnotation", function ( event, data )
    {
        self.deleteAnnotation( data.selectionClass, true );
    } );
	
	/* Modify already existing highlight. This event is dispatched from NotePopup */
    $( self.notePopup ).bind( "updateExistingHighlight", function ( event, data )
    {
        self.modifyHighlight( data );
    } );
	
	
	$( "#dltConfirmPanel.noteDeleteConfimPanel #dltConfirmationSuccess" ).bind( "click", function ( event )
	{
		self.deleteAnnotation( self._currentSelectionClass, false );
	} );
	
	$( "#dltConfirmPanel.noteDeleteConfimPanel #dltConfirmationFailure" ).bind( "click", function ( event )
	{
		PopupManager.removePopup();
		if( $( "." + self._currentSelectionClass ).length > 0 && self.deleteFromPanel != true )
		{
	        self.notePopup.launchPopup( "." + self._currentSelectionClass );
		}
		self.deleteFromPanel = false;
	} );
	
	$( "#dltCommentConfirmPanel #dltConfirmationSuccess" ).bind( "click", function ( event )
    {
        self.deleteComment();
    } );
    
    $( "#dltCommentConfirmPanel #dltConfirmationFailure" ).bind( "click", function ( event )
    {
        PopupManager.removePopup();
        if( $( "." + self._currentSelectionClass ).length > 0 && self.deleteFromPanel != true )
        {
            self.notePopup.launchPopup( "." + self._currentSelectionClass );
        }
        self.deleteFromPanel = false;
    } );
    
    $( "#dltConfirmPanel.highlightDeleteConfimPanel #dltConfirmationSuccess" ).bind( "click", function ( event )
	{
		self.deleteAnnotation( self._currentSelectionClass, false );
	} );
	
	$( "#dltConfirmPanel.highlightDeleteConfimPanel #dltConfirmationFailure" ).bind( "click", function ( event )
	{
		PopupManager.removePopup();
	} );
	
	$( self.notePopup ).bind( "resetEditFromPanel", function ( event )
    {
        self.editFromPanel = false;
    } );
        	
};

MG.NoteOperator.prototype.restoreExistingAnnoClass = function ( )
{
	var self = this;
	var addClassToAnnoTag = function ( annoTag )
	{
		annoTag.removeClass()
		var annoArr = annoTag.attr( "annoArr" ).split( "," );
		var len = annoArr.length;
		var isFirstHighlightEncountered = false;
		var isFirstUnderlineHighlightEncountered = false;
		
		for( var index = len - 1; index >=0; index -- )
		{
			var annoItem = annoArr[ index ];
			var idColorArr = annoItem.split( ":" );
			//if( !annoTag.hasClass( idColorArr[ 0 ] ) )
			{
				if( idColorArr[ 1 ].indexOf('underline') != -1 )
				{
					if( isFirstUnderlineHighlightEncountered == false )
					{
						annoTag.addClass( idColorArr[ 0 ] );
						annoTag.addClass( idColorArr[ 1 ] );
					}
					else
					{
						annoTag.addClass( idColorArr[ 0 ] );
					}
					isFirstUnderlineHighlightEncountered = true;
				}
				else if( idColorArr[ 1 ].indexOf('selectioncolor') != -1 )
				{
					if( isFirstHighlightEncountered == false )
					{
						annoTag.addClass( idColorArr[ 0 ] );
						annoTag.addClass( idColorArr[ 1 ] );
					}
					else
					{
						annoTag.addClass( idColorArr[ 0 ] );
					}
					isFirstHighlightEncountered = true;
				}
				else
				{
					annoTag.addClass( idColorArr[ 0 ] );
				}
			}
		}
	}
	
	var pageNum = self.scrollViewRef.options.pageIndexForAnnotation;
	var currentNoteSelectionId;
	for( var i = 0; i < GlobalModel.annotations.length; i++ )
	{
		if( GlobalModel.annotations[ i ].pageNumber == pageNum )
		{
			if ( GlobalModel.annotations[ i ].type == 'stickynote' ) 
	    	{
				currentNoteSelectionId = ".noteselection_" + GlobalModel.annotations[ i ].id;
	        	if( GlobalModel.annotations[ i ].shared_with )//GlobalModel.BookEdition == "TSE" && GlobalModel.selectedStudentData && GlobalModel.annotations[ i ].title.length == 0 )
	        	{
	        		currentNoteSelectionId = ".tseselect" + GlobalModel.annotations[ i ].id;
	        	}
	       	}
	       	else
	       	{
	       		currentNoteSelectionId = ".selection" + GlobalModel.annotations[ i ].id;
	       	}
			
			var _currentNoteSelectionArr = $( currentNoteSelectionId );
		    for ( var j = 0; j < _currentNoteSelectionArr.length ; j++ )
		    {
		    	var existingAnnoInfo = $( _currentNoteSelectionArr[j] ).attr( "annoArr" );
		    	console.log("existingAnnoInfo",existingAnnoInfo)
		    	if( existingAnnoInfo != undefined )
		    	{
		    		existingAnnoArr = existingAnnoInfo.split(",");
		    		
		    		addClassToAnnoTag( $( _currentNoteSelectionArr[j] ) );
		    	}
		    }
		}
	}	
	
}

MG.NoteOperator.prototype.repositionNoteIcon = function ( data, selectionStr )
{
	var self = this;
	var newIcon = $( "#btnStickyNote_" + data.id );
    var arrPos = {};
    arrPos.left = "770";
    arrPos.top = $( $( selectionStr + data.id )[0] ).offset().top;
    arrPos.contentWidth = EPubConfig.pageWidth;
    newIcon.css( "display", "block" );
    self.setElementPosition ( newIcon.offset(), arrPos, data );
    newIcon.css( "top", CompModel.yPos );
    newIcon.css( "left", CompModel.xPos );
    data.icontop = CompModel.yPos;
    data.iconleft = CompModel.xPos;
    return newIcon;
};

MG.NoteOperator.prototype.deleteComment = function ()
{
    var self = this;
    var id = self.getIDFromCurrentSelectionClass();
    var noteObj = self.findNoteObjectInGlobalModel( id );
    if( noteObj.shared_with )
    {
        self.deleteAnnotation( self._currentSelectionClass, false );
        return;
    }
    var annID = noteObj.comments.annotation_id;
    PopupManager.removePopup();
    delete noteObj.comments;
    if( noteObj.type == "stickynote" )
    {
    	$( "#btnStickyNote_" + noteObj.id ).removeClass( "btnStickyNoteTeacherComment" );
    }
    else
    {
    	noteObj.icontop = noteObj.iconleft = 0;
    	$( "#btnStickyNote_" + noteObj.id ).css( "top", noteObj.icontop );
    	$( "#btnStickyNote_" + noteObj.id ).css( "left", noteObj.icontop );
    	$( "#btnStickyNote_" + noteObj.id ).css( "display", "none" );
    }
    
    ServiceManager.Annotations.remove( annID, 7 );
    GlobalModel.updateTeacherNotificationIcon( "teacherNoteAlertleft" );
    if( self.deleteFromPanel == true )
    {
        $( $("#bookContentContainer" ).data( 'scrollviewcomp' ) ).trigger( 'closeOpenedPanel' );
    }
    self.deleteFromPanel = false;
};

MG.NoteOperator.prototype.addCommentToNote = function ( data )
{
	var self = this;
    var obj = self.findNoteObjectInGlobalModel( data.id );
    data.savetoNote = obj.savetonote = $( "#stickyNoteContainer" ).find( '[type = savetoNoteBookButton]' ).hasClass( 'savetoNoteBookBtnChk' ) ? 0 : 1;
    obj.tagsLists = data.tagsLists;
    obj.comments = {};
    obj.comments.body_text = data.title;
    obj.comments.timeMilliseconds = new Date();
    obj.comments.date = getTodaysDate();
    obj.isNotified = true;
    data.annotationId = obj.annotationID;
    data.pageNumber = obj.pageNumber;
    if( obj.type == "stickynote" )
    {
    	$( "#btnStickyNote_" + data.id ).addClass( "btnStickyNoteTeacherComment" );
    }
    else
    {
    	$( "#btnStickyNote_" + data.id ).addClass( "btnStickyNoteTeacherCommentHighlight" );
    }
    GlobalModel.updateTeacherNotificationIcon( "teacherNoteAlertleft" );
    ServiceManager.Annotations.create( data, 7, function ( objData )
    {
       var tempObj = self.findNoteObjectInGlobalModel( data.id );
       tempObj.comments = objData.length ? objData[ 0 ] : objData;
       tempObj.comments.date = getTodaysDate();
       tempObj.comments.timeMilliseconds = new Date();
    } );
};

MG.NoteOperator.prototype.updateCommentToNote = function ( data )
{
	var self = this;
    var obj = self.findNoteObjectInGlobalModel( data.id );
    var objTemp = {};
    obj.tagsLists = data.tagsLists;
    obj.savetonote = $( "#stickyNoteContainer" ).find( '[type = savetoNoteBookButton]' ).hasClass( 'savetoNoteBookBtnChk' ) ? 0 : 1;
    obj.isNotified = true;
    if( obj.comments )
    {
        objTemp.title = obj.comments.body_text = data.title;
        objTemp.annotation_id = obj.comments.annotation_id;
        obj.comments.timeMilliseconds = new Date();
        obj.comments.date = getTodaysDate();
        var type = 7;
    }
    else
    {
    	if( data.type && data.type == 2 )
    	{
    		var index = $( "#stickyNoteContainer" ).find( ".colorContainer .colorButton" ).index( $( ".colorButtonSelected" ) ) + 1;
    		var tempType = $( $( "#stickyNoteContainer" ).find( ".styleContainer span" )[ 0 ] ).hasClass( "styleButtonSelected" ) ? "_underline" : "";
		    data.selectioncolor = "selectioncolor" + index + tempType;
		    data.style = ( tempType == "_underline" ) ? 1 : 0;
    		data.annotation_id = obj.annotationID;
    		objTemp = data;
    		var type = 2;
    	}
    	else
    	{
    		objTemp.title = obj.title = data.title;
	        objTemp.annotation_id = obj.annotationID;
	        var type = 3;
    	}
        
    }
    GlobalModel.updateTeacherNotificationIcon( "teacherNoteAlertleft" );
    PopupManager.removePopup();
    if ( ( ( obj.annotationID != undefined ) && ( obj.annotationID != 0 ) ) || 
    ( ( obj.comments.annotation_id != undefined ) && ( obj.comments.annotation_id != 0 ) ) ) 
    {
        ServiceManager.Annotations.update( type, objTemp );
    }
 };

MG.NoteOperator.prototype.deleteAnnotation = function ( selectionClass, isSwapping )
{
    var self = this;
    self._currentSelectionClass = selectionClass;
    var len = GlobalModel.annotations.length;
    var id = self._currentSelectionClass.replace( ".", "" );
    var annotation = null;
       
    PopupManager.removePopup();
    for( var i = 0; i < len; i++ )
    {
        annotation = GlobalModel.annotations[ i ];
        if( self.getIDFromCurrentSelectionClass() == annotation.id )//&& annotation.type == "stickynote" )
        {
            if ( ( annotation.annotationID != undefined ) &&
               ( annotation.annotationID != 0 ) ) 
            {
                if( annotation.shared_with && GlobalModel.BookEdition == "SE" )
                {
                    annService.AnnotationSharedWith.remove( annotation.shared_with.results[0].id ).done( function( data ) {
                       console.log("DELETED BY STUDENT");
                   } ).fail( function( err ) 
                   {
                       console.log( err );
                   } )
                   .run();
                }
                else
                {
                    ServiceManager.Annotations.remove( annotation.annotationID, 3 );
                }
            }
            // must be set only and only here
            if( !isSwapping )
		    {
		        $( "#btnStickyNote_" + self.getIDFromCurrentSelectionClass() ).remove();
		        self.removeFromAnnotationArr( id );	
		        if ( GlobalModel.annotations[ i ].selectioncolor )
		        {
		        	$( "." + id ).removeClass( GlobalModel.annotations[ i ].selectioncolor );	
		        }
		        $( "." + id ).removeClass( id );	        
		    }
            self.oldObject = GlobalModel.annotations[ i ];
            GlobalModel.annotations.splice(i, 1);
            self.reCreatehighlights();     
            GlobalModel.updateTeacherNotificationIcon( "teacherNoteAlertleft" );
            $('[annoarr=""]').removeAttr('class');
            $('[annoarr=""]').removeAttr('annoarr');
            break;
        }
    }
    if( self.deleteFromPanel == true )
    {
        $( $("#bookContentContainer" ).data( 'scrollviewcomp' ) ).trigger( 'closeOpenedPanel' );
    }
    self.deleteFromPanel = false;
};

MG.NoteOperator.prototype.removeFromAnnotationArr = function ( currentNoteSelectionId )
{
	var _currentNoteSelectionArr = $( "." + currentNoteSelectionId );
    for ( var i = 0; i < _currentNoteSelectionArr.length ; i++ )
    {
    	var existingAnnoInfo = $( _currentNoteSelectionArr[i] ).attr( "annoArr" );
    	if( existingAnnoInfo != undefined )
    	{
    		existingAnnoArr = existingAnnoInfo.split(",");
    		var indexToRemove = -1;
    		for ( var j = 0; j < existingAnnoArr.length ; j++ )
    		{
    			if( existingAnnoArr[j].split(":")[0] == currentNoteSelectionId )
    			{
    				indexToRemove = j;
    				break;
    			}
    		}
    		if ( indexToRemove != -1 )
    		{
    			existingAnnoArr.splice( indexToRemove, 1 );
    			$( _currentNoteSelectionArr[i] ).attr( "annoArr" , existingAnnoArr.toString() );
    		}
    	}
    }
}

MG.NoteOperator.prototype.reCreatehighlights = function ( )
{
	var self = this;
	if (navigator.userAgent.match(/(MSIE 10.0)/i) || isWinRT || $.browser.version == 11)
	{
		self.restoreExistingAnnoClass();
		return;
	}
	var range = "";
	var selectioncolor,selectionClass;
	$( "anno" ).removeAttr('annoArr');
	$( "anno" ).removeAttr('class');
	
	GlobalModel.annotations.sort( function( a, b ) 
	{
		var c = a.annotationID;
		var d = b.annotationID;
		return c - d;
	} );
	for( var i = 0; i < GlobalModel.annotations.length; i++ )
    {
   	    if ( GlobalModel.annotations[ i ].type == 'stickynote' ) 
    	{
    	    range = GlobalModel.annotations[ i ].range;
        	selectionClass = "noteselection_";
        	if( GlobalModel.annotations[ i ].shared_with )//GlobalModel.BookEdition == "TSE" && GlobalModel.selectedStudentData && GlobalModel.annotations[ i ].title.length == 0 )
        	{
        	    selectionClass = "tseselect";
        	}
       	}
       	else
       	{
       	    range = GlobalModel.annotations[ i ].rangeObject;
       	    selectioncolor = GlobalModel.annotations[ i ].selectioncolor;
       	    selectionClass = "selection";
       	}
       	index = self.scrollViewRef.member.curPageElementsPgBrk.indexOf( GlobalModel.annotations[ i ].pageNumber );
        if ( EPubConfig.pageView == 'doublePage' ) 
        {
            if ( GlobalModel.currentPageIndex == index || index == GlobalModel.currentPageIndex + 1 ) 
            {
                self.createNoteHighlight( selectionClass, GlobalModel.annotations[ i ], range, selectioncolor );
            }
        }
        else if( GlobalModel.currentPageIndex == index )
        {
            self.createNoteHighlight( selectionClass, GlobalModel.annotations[ i ], range, selectioncolor );
        }
    }
};

MG.NoteOperator.prototype.getIDFromCurrentSelectionClass = function ()
{
    var self = this;
    var retVal = self._currentSelectionClass;
    retVal = retVal.replace("noteselection_", "");
    retVal = retVal.replace("selection", "");
    retVal = retVal.replace(".noteselection_", "");
    retVal = retVal.replace(".selection", "");
    retVal = retVal.replace("tseselect", "");
    retVal = retVal.replace(".tseselect", "");
    retVal = retVal.replace(".", "");
    return retVal;
};

MG.NoteOperator.prototype.editNote = function ( selectionClass ,noteId , pageNum )
{
	var self = this;
	var scrollviewCompRefnce = $( "#bookContentContainer" ).data( 'scrollviewcomp' );
	var pageIndex = scrollviewCompRefnce.member.curPageElementsPgBrk.indexOf( String( pageNum ) );
	var isNoteVisible = false;
	if ( EPubConfig.pageView == 'doublePage' )
	{
        isNoteVisible = ( pageIndex == GlobalModel.currentPageIndex || pageIndex == GlobalModel.currentPageIndex + 1 ) ? true : false;
    }
    else
    {
        isNoteVisible = ( pageIndex == GlobalModel.currentPageIndex ) ? true : false;
	}

    if ( isNoteVisible ) 
    {
    	self.noteToEdit = -1;
    	$($( "#btnStickyNote_" + noteId )[ 0 ] ).trigger( "click" );
    }
    else
    {
    	self.noteToEdit = noteId;
    	scrollviewCompRefnce.showPageAtIndex( GlobalModel.pageBrkValueArr.indexOf( String ( pageNum ) ) );
    }
	
};

MG.NoteOperator.prototype.modifyHighlight = function ( data )
{
    var self = this;
    var objData = {};
    var obj = {};
    var index = $( "#stickyNoteContainer" ).find( ".colorContainer .colorButton" ).index( $( ".colorButtonSelected" ) ) + 1;
    var type = $( $( "#stickyNoteContainer" ).find( ".styleContainer span" )[ 0 ] ).hasClass( "styleButtonSelected" ) ? "_underline" : "";
    
    obj = self.findNoteObjectInGlobalModel( data.id );
    objData.title = obj.title;
    objData.id = data.id;
    objData.date = obj.date = self.getDate();
    objData.type = "highlight";
    objData.savetonote = obj.savetonote = $( "#stickyNoteContainer" ).find( '[type = savetoNoteBookButton]' ).hasClass( 'savetoNoteBookBtnChk' ) ? 0 : 1;
    objData.tagsLists = obj.tagsLists = data.tagsLists;
    objData.rangeObject = obj.range;
    objData.selectioncolor = obj.selectioncolor = "selectioncolor" + index + type;
    objData.selectionID = obj.selectionID;
    objData.style = obj.style = ( type == "_underline" ) ? 1 : 0;
    objData.timeMilliseconds = obj.timeMilliseconds = new Date();
    objData.top =  obj.top;
    objData.pageNumber = obj.pageNumber;
    objData.annotation_id = obj.annotationID;
    self.updateAnnotationAttr( obj.selectionID.replace( ".","" ) , obj.selectioncolor );
    
    if( ( objData.annotation_id != undefined ) && ( objData.annotation_id != 0 ) )
    {
        ServiceManager.Annotations.update( 2, objData );
    }

    if( obj.userComment && obj.userComment.annotation_id )
    {
    	
    	var objToPost = {};
    	objToPost.title = data.userComment.body_text;
		objToPost.annotation_id = obj.userComment.annotation_id;
    	if( data.userComment.body_text == "" )
    	{
    		var annoID = obj.userComment.annotation_id;
    		delete obj.userComment;
    		ServiceManager.Annotations.remove( annoID );
    	}
    	else
    	{
    		obj.userComment.body_text = data.userComment.body_text;
    		ServiceManager.Annotations.update( 7, objToPost );
    	}
    }
    else if ( data.userComment )
    {
    	// create
    	var objToPost = {};
    	objToPost.pageNumber = obj.pageNumber;
    	objToPost.annotationId = obj.annotationID;
    	objToPost.title = data.userComment.body_text;
    	obj.userComment = data.userComment;
    	ServiceManager.Annotations.create( objToPost, 7 , function ( data )
		{
    		 for( var i = 0; i < GlobalModel.annotations.length; i++ )
    	     {
    	         if( GlobalModel.annotations[ i ].annotationID == data.parent_id )
    	         {
    	        	 GlobalModel.annotations[ i ].userComment = data;
    	        	 return;
    	         }
    	     }
		});
    }
    
};

MG.NoteOperator.prototype.modifyNote = function ( data )
{
	var self = this;
	var obj = {};
	var objData = {};
	obj = self.findNoteObjectInGlobalModel( data.id );
	objData.stickyContent = obj.title = data.title;
    objData.savetonote = obj.savetonote = $( "#stickyNoteContainer" ).find( '[type = savetoNoteBookButton]' ).hasClass( 'savetoNoteBookBtnChk' ) ? 0 : 1;
    obj.date = self.getDate();
    obj.timeMilliseconds = new Date();
    objData.tagsLists = obj.tagsLists = data.tagsLists;
    objData.contentID = obj.id;
    objData.title = "";
    objData.pagenum = objData.objectID = obj.pageNumber;
    objData.rangeObject = obj.range;
    objData.annotation_id = obj.annotationID;
    self.updateAnnotationAttr( "noteselection_" + objData.id , "noteselection" );
    //objData.comment = obj.comment = $( "#stickyNoteContainer" ).find( "#commentTextArea" ).html();
    
    if ( ( objData.annotation_id != undefined ) && ( objData.annotation_id != 0 ) ) 
    {
        ServiceManager.Annotations.update( 3, objData );
    }
	
};

MG.NoteOperator.prototype.findNoteObjectInGlobalModel = function ( id )
{
	for( var i = 0; i < GlobalModel.annotations.length; i++ )
	{
		if( GlobalModel.annotations[ i ].id == id )//&& ( GlobalModel.annotations[ i ].type == "stickynote" ) )
		{
			return GlobalModel.annotations[ i ];
		}
	}
};

MG.NoteOperator.prototype.createNewHighlightModel = function ( data )
{
    var self = this;
    var index = $( "#stickyNoteContainer" ).find( ".colorContainer .colorButton" ).index( $( ".colorButtonSelected" ) ) + 1;
    var type = $( $( "#stickyNoteContainer" ).find( ".styleContainer span" )[ 0 ] ).hasClass( "styleButtonSelected" ) ? "_underline" : "";
    var container = document.createElement("span");
    var objData = {};
    var str = data.id;
    var rangeData = null;
    
    if( data.isSwapping ) 
    {
        str = self.oldObject.id;
        var Obj = $.xml2json( self.oldObject.range );
        var startC = $( '[id = "' + Obj.startContainer + '"]' ).find( '#mainContent' )[ 0 ];
        var startO = parseInt( Obj.startOffset );
        var endO = parseInt( Obj.endOffset );
        var savedSel = {
            start : startO,
            end : endO
        };
        
        rangeData = self.restoreSelection( $( startC )[ 0 ], savedSel );
        container.appendChild( rangeData.cloneContents() ).innerHTML;
        $( container ).addClass( $( rangeData.commonAncestorContainer.parentElement ).attr('class') );
    } 
    else
    {
        container.appendChild( self.scrollViewRef.options.rangedata.cloneContents() ).innerHTML;
        $( container ).addClass( $( self.scrollViewRef.options.rangedata.commonAncestorContainer.parentElement ).attr('class') );
    }
    objData.title = container.outerHTML;
    objData.id = str;
    objData.date = self.getDate();
    objData.type = "highlight";
    objData.savetonote = $( "#stickyNoteContainer" ).find( '[type = savetoNoteBookButton]' ).hasClass( 'savetoNoteBookBtnChk' ) ? 0 : 1;
    objData.tagsLists = data.tagsLists;
    objData.range = ( data.isSwapping ) ? rangeData : self.scrollViewRef.options.rangedata;
    objData.rangeObject = ( data.isSwapping ) ? self.oldObject.range : self.scrollViewRef.options.rangeObject;
    objData.selectioncolor = "selectioncolor" + index + type;
    objData.selectionID = data.selectionClass;
    objData.style = ( type == "_underline" ) ? 1 : 0;
    objData.timeMilliseconds = new Date();
    objData.top = 0;//self.scrollViewRef.options.rangedata.getBoundingClientRect().top;
    objData.pageNumber = ( self.scrollViewRef.options.pageIndexForAnnotation );
    if( GlobalModel.BookEdition == "TSE" && GlobalModel.selectedStudentData )
    {
    	objData.isNotified = true;
        objData.shared_with = {};
    }
    
    if( GlobalModel.BookEdition == "TSE" && !GlobalModel.selectedStudentData && ( GlobalModel.NewNoteSelectedStudents.length > 0 || 
    	data.isSwapping && self.oldObject && self.oldObject.tempTeachersSharedWith && self.oldObject.tempTeachersSharedWith.results ) )
    {
    	objData.teachersSharedWith = {};
		objData.teachersSharedWith.results = [];
		var tempArr = [];
		if( data.isSwapping && self.oldObject && self.oldObject.tempTeachersSharedWith && self.oldObject.tempTeachersSharedWith.results )
		{
			tempArr = self.oldObject.tempTeachersSharedWith.results;
		}
		else
		{
			tempArr = GlobalModel.NewNoteSelectedStudents;
		}
		for( var i = 0; i < tempArr.length; i++ )
		{
			var tempObj = {};
			tempObj.platformid = $( tempArr[ i ] ).attr( "platformid" );
			objData.teachersSharedWith.results.push( tempObj );
		}
    }
    GlobalModel.annotations.push( objData );
    return objData;
};

MG.NoteOperator.prototype.createNewHighlight = function ( data )
{
    var self = this;
    var objData =  self.createNewHighlightModel( data );
    
    if( !data.isSwapping )
    {
        var newIcon = $( "#btnStickyNote" );    
        newIcon.attr( 'id', 'btnStickyNote_' + GlobalModel.totalStickyNotes );
        newIcon.attr( 'associated-page-number', objData.pageNumber );    
    }
    else
    {
        var newIcon = $( "#btnStickyNote_" + data.id );
        newIcon.css( "display", "none" );
    }
    
    if( GlobalModel.BookEdition == "TSE" && !GlobalModel.selectedStudentData && objData.teachersSharedWith )
    {
    	newIcon.addClass( "btnHighlightGroupSharing" );
        newIcon.css( "display", "block" );
        self.repositionNoteIcon( objData, ".selection" );
    }
    self.addEventOnHighlightAndIcon( "selection", data.id );  
    self.updateAnnotationAttr( objData.selectionID.replace( ".","" ) , objData.selectioncolor );  
    self.saveNewHighlightOnServer( objData );
};

MG.NoteOperator.prototype.saveNewHighlightOnServer = function ( objData )
{
	var obj = {};
	var annoId = 0;
   GlobalModel.updateTeacherNotificationIcon( "teacherNoteAlertleft" );
   ServiceManager.Annotations.create( objData, 2, function( data )
   {
        for( var i = 0; i < GlobalModel.annotations.length; i++ )
        {
            if( ( objData.id == GlobalModel.annotations[ i ].id ) && ( objData.type == GlobalModel.annotations[ i ].type ) )
            {
                if( data.length == undefined )
                {
                    GlobalModel.annotations[ i ].annotationID = data.annotation_id;  
                }
                else
                {
                    GlobalModel.annotations[ i ].annotationID = data[ 0 ].annotation_id;
                }
                obj = GlobalModel.annotations[ i ];
                annoId = GlobalModel.annotations[ i ].annotationID;
                break;
            }
        }
        if( obj.teachersSharedWith && obj.teachersSharedWith.results && obj.teachersSharedWith.results.length > 0 )
        {
        	ServiceManager.getStudentDataForNewAnnotation( annoId, function ( data )
	        {
	        	obj.teachersSharedWith = data.shared_with;
	        } );
        }
   } );
};

MG.NoteOperator.prototype.createNewNoteModel = function ( data, newIcon )
{
    var self = this;
    var objData = {};
    var obj = {};
    var str = data.id;
    
    if( data.isSwapping ) 
    {
    	//console.log( self.oldObject, "create new note model" );
        str = self.oldObject.id;
    } 
    
    objData.pagenum = obj.pageNumber = self.scrollViewRef.options.pageIndexForAnnotation;
    objData.tagsLists = obj.tagsLists = data.tagsLists;
    objData.savetoNote = obj.savetonote = $( "#stickyNoteContainer" ).find( '[type = savetoNoteBookButton]' ).hasClass( 'savetoNoteBookBtnChk' ) ? 0 : 1;
    objData.rangeObject = obj.range = ( data.isSwapping ) ? self.oldObject.rangeObject : self.scrollViewRef.options.rangeObject;
    objData.icontop = obj.icontop = parseInt( newIcon.css( "top" ) );
    objData.iconleft = obj.iconleft = parseInt( newIcon.css( "left" ) );
    objData.title = "";
    obj.date = self.getDate();
    obj.type = "stickynote";
    obj.timeMilliseconds = new Date();
    objData.id = obj.id = str;
    objData.stickyContent = obj.title = data.title;
    if( GlobalModel.BookEdition == "TSE" && GlobalModel.selectedStudentData )
    {
    	obj.isNotified = true;
        obj.shared_with = {};
    }
    
    if( GlobalModel.BookEdition == "TSE" && !GlobalModel.selectedStudentData && ( GlobalModel.NewNoteSelectedStudents.length > 0 || 
    	data.isSwapping && self.oldObject && self.oldObject.tempTeachersSharedWith && self.oldObject.tempTeachersSharedWith.results ) )
    {
    	obj.teachersSharedWith = {};
		obj.teachersSharedWith.results = [];
		var tempArr = [];
		if( data.isSwapping && self.oldObject && self.oldObject.tempTeachersSharedWith && self.oldObject.tempTeachersSharedWith.results )
		{
			tempArr = self.oldObject.tempTeachersSharedWith.results;
		}
		else
		{
			tempArr = GlobalModel.NewNoteSelectedStudents;
		}
		for( var i = 0; i < tempArr.length; i++ )
		{
			var tempObj = {};
			tempObj.platformid = $( tempArr[ i ] ).attr( "platformid" );
			obj.teachersSharedWith.results.push( tempObj );
		}
		objData.teachersSharedWith = obj.teachersSharedWith; 
    }
    //objData.comment = obj.comment = $( "#stickyNoteContainer" ).find( "#commentTextArea" ).html();
    GlobalModel.annotations[ GlobalModel.annotations.length ] = obj;
    return objData;
};

MG.NoteOperator.prototype.createNewNote = function ( data )
{
	var self = this;
	var newIcon = $( "#btnStickyNote" );
	var selection = "";
	
	if( GlobalModel.BookEdition == "TSE" && GlobalModel.selectedStudentData )
	{
	    selection = "tseselect";
	}
	else
	{
	    selection = "noteselection_";
	}
	if( data.isSwapping )
	{
		if( self.oldObject.comments )
		{
			var newIcon = $( "#btnStickyNote_" + data.id );
		}
		else
		{
			var newIcon = self.repositionNoteIcon( data, "." + selection );
		}
		if( selection == "tseselect" )
		{
			newIcon.addClass( "btnStickyNoteStandaloneTeacherComment" );
		}
	    var objData = self.createNewNoteModel( data, newIcon );       
	}
	else
    {
        var objData = self.createNewNoteModel( data, newIcon );
        newIcon.attr( 'id', 'btnStickyNote_' + GlobalModel.totalStickyNotes );
        self.repositionNoteIcon( data, "." + selection );
        newIcon.attr( 'associated-page-number', objData.pageNumber );
        newIcon.css( "display", "block" );
        if( selection == "tseselect" )
		{
			newIcon.addClass( "btnStickyNoteStandaloneTeacherComment" );
		}
    }
    if( objData.teachersSharedWith )
    {
    	newIcon.addClass( "btnStickyNoteGroupSharing" );
    }
	self.addEventOnHighlightAndIcon ( selection, objData.id  );
	self.updateAnnotationAttr( selection + objData.id , "noteselection" );
	self.saveNewNoteOnServer( objData );
};

MG.NoteOperator.prototype.saveNewNoteOnServer = function ( objData )
{
    var self = this;
    var annoId = 0;
    var obj = {};
    GlobalModel.updateTeacherNotificationIcon( "teacherNoteAlertleft" );
    /* Send call to Service Manager to save the data on Server */
    ServiceManager.Annotations.create( objData, 3, function( data ) 
    {
        for ( var i = 0; i < GlobalModel.annotations.length; i++ ) 
        {
            if ( ( objData.id == GlobalModel.annotations[ i ].id ) && ( GlobalModel.annotations[ i ].type == "stickynote" ) ) 
            {
                if( data.length == undefined )
                {
                    GlobalModel.annotations[ i ].annotationID = data.annotation_id;
                }   
                else
                {
                    GlobalModel.annotations[ i ].annotationID = data[0].annotation_id;
                } 
                obj = GlobalModel.annotations[ i ];
                annoId = GlobalModel.annotations[ i ].annotationID;
                break;
            }
        }
        if( obj.teachersSharedWith && obj.teachersSharedWith.results && obj.teachersSharedWith.results.length > 0 )
        {
        	ServiceManager.getStudentDataForNewAnnotation( annoId, function ( data )
	        {
	        	obj.teachersSharedWith = data.shared_with;
	        } );
        }
        
    } );
    
    //objData.comment = $( "#stickyNoteContainer" ).find( "#commentTextArea" ).html();
};

MG.NoteOperator.prototype.getDate = function ()
{
	var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var year = d.getFullYear();
    var dateOfCreation = ( ( '' + month ).length < 2 ? '0' : '' ) + month + '/' + ( ( '' + day ).length < 2 ? '0' : '' ) + 
    					 day + '/' + year.toString().substr( 2, 2 );
    return dateOfCreation;
};

MG.NoteOperator.prototype.openDeleteConfirmPanel = function ( selectionClass , type )
{
	var self = this;
	self._currentSelectionClass = selectionClass;
	
	PopupManager.removePopup();
	if( type != "highlight" )
	{
		$( "#dltConfirmPanel.noteDeleteConfimPanel #dltConfirmationTxt" ).html( GlobalModel.localizationData[ "STICKY_NOTE_DELETE_CONFIRMATION_TEXT_NOTEBOOK" ] );
		PopupManager.addPopup( $( "#dltConfirmPanel.noteDeleteConfimPanel" ), null, {
	        isModal : true,
	        isTapLayoutDisabled : true,
	        popupOverlayStyle : "popupMgrMaskStyleSemiTransparent",
	        isCentered : true
	
	    } );
	}
	else
	{
		$( "#dltConfirmPanel.highlightDeleteConfimPanel #dltConfirmationTxt" ).html( GlobalModel.localizationData[ "HIGHLIGHT_DELETE_CONFIRMATION_TEXT" ] );
		PopupManager.addPopup( $( "#dltConfirmPanel.highlightDeleteConfimPanel" ), selectionClass, {
	        isModal : true,
	        hasPointer : true,
	        offsetScale : GlobalModel.currentScalePercent,
	        isTapLayoutDisabled : true,
	        popupOverlayStyle : "popupMgrMaskStyleSemiTransparent"
	        	
	    } );
	    //self.adjustHighlightDeleteConfirmPopup( $( "#dltConfirmPanel.highlightDeleteConfimPanel" ) );
	}
};

MG.NoteOperator.prototype.adjustHighlightDeleteConfirmPopup = function( deleteConfirmationPanel )
{
	var self = this;
	if($(deleteConfirmationPanel).offset().left < $("#bookContentContainer").css('margin-left').slice(0,-2))
    {
    	$(deleteConfirmationPanel).css("left",$("#bookContentContainer").css('margin-left').slice(0,-2)+"px");
    }
    if(($(deleteConfirmationPanel).offset().left + $(deleteConfirmationPanel).width()) > $(window).width() - 75)
    {
    	$(deleteConfirmationPanel).css("left",$(window).width() - $(deleteConfirmationPanel).width() - 75+"px");
    }
    if($(deleteConfirmationPanel).offset().top < 0)
    {
    	$(deleteConfirmationPanel).css("top","10px");
    } 
    if(($(deleteConfirmationPanel).offset().top + $(deleteConfirmationPanel).height()) > $(window).height() - 50)
    {
    	$(deleteConfirmationPanel).css("top",$(window).height() - $(deleteConfirmationPanel).height() - 50+"px");
    }
    
    if(EPubConfig.pageView.toUpperCase() == 'SCROLL')
    {
        if($(deleteConfirmationPanel).offset().top < $(self._currentSelectionClass).offset().top)
            $(deleteConfirmationPanel).css('margin-top',"-5px");
        else
            $(deleteConfirmationPanel).css('margin-top',"8px");
    }
    else
            $(deleteConfirmationPanel).css('margin-top',"-5px");
}

/*---------------------------------------------------------- Handlers end ------------------------------------------------------------------------------*/

MG.NoteOperator.prototype.setElementPosition = function( offset, arrPos, data )
{
	var self = this;
    this.contentWidth = arrPos.contentWidth;
    var scrollTopElement = $( "#bookContentContainer" );
    var offsetTopParent = 0;
    
    var pageBrkValue = ( data && data.pageNumber ) ? data.pageNumber : self.scrollViewRef.options.pageIndexForAnnotation;//$( "#bookContentContainer" ).data( 'scrollviewcomp' ).options.pageIndexForAnnotation;
    CompModel.xPos = $( "#innerScrollContainer" ).width() - AppConst.NOTE_MARGIN_FROM_RIGHT;
    if ( EPubConfig.pageView == 'doublePage' || EPubConfig.pageView == 'singlePage' ) 
    {
        CompModel.xPos = arrPos.contentWidth - AppConst.NOTE_MARGIN_FROM_RIGHT;
        if( EPubConfig.ReaderType.toUpperCase() == '6TO12' )
        {
        	offsetTopParent = 50;
        }
        CompModel.yPos = ( arrPos.top / GlobalModel.currentScalePercent ) + ( $( scrollTopElement ).scrollTop() / GlobalModel.currentScalePercent ) - 
        				 offsetTopParent / GlobalModel.currentScalePercent;
    } 
    else 
    {      
        offsetTopParent = $( "#bookContentContainer2" ).find( '[id = "PageContainer_' + pageBrkValue + '"]' ).offset().top / GlobalModel.currentScalePercent;
        CompModel.yPos = ( arrPos.top / GlobalModel.currentScalePercent ) + 
        				 ( $( scrollTopElement ).scrollTop() / GlobalModel.currentScalePercent ) - offsetTopParent;
    }
    var arrStickyNotePositionsInCurrentPage = [];
    var sortedTopPos = [];
    var objStickyNotesInCurrentPage = $( '[id = "PageContainer_' + pageBrkValue + '"]' ).find( '[type = "stickyNoteComp"]' );
    var elementHeight = $( objStickyNotesInCurrentPage[ 0 ] ).height();
   // console.log( data,objStickyNotesInCurrentPage, " objStickyNotesInCurrentPageobjStickyNotesInCurrentPage")
    if ( objStickyNotesInCurrentPage && objStickyNotesInCurrentPage.length > 0 ) 
    {
        for ( var iCount = 0; iCount < objStickyNotesInCurrentPage.length; iCount++ ) 
        {
        	var id = $( objStickyNotesInCurrentPage[ iCount ] ).attr( "id" );
            if( ( $( objStickyNotesInCurrentPage[ iCount ] ).css( "display" ) == "block" ) && !( data &&  data.id == id.replace( "btnStickyNote_", "") ) )
            {
                arrStickyNotePositionsInCurrentPage.push( parseInt( $( objStickyNotesInCurrentPage[ iCount ] ).css( "top" ) ) );
            }
        }
        sortedTopPos = ( arrStickyNotePositionsInCurrentPage ).sort( function( a, b ) 
        {
            return a - b;
        } );
    }

	var bFirstTime = true;
    for ( var i = 0; i < sortedTopPos.length; i++ ) 
    {
        if ( CompModel.yPos  <= ( sortedTopPos[ i ] + elementHeight + 2 ) ) 
        {
        	if ( bFirstTime == true ) 
        	{
                if ( (sortedTopPos[i] - CompModel.yPos ) > ( elementHeight + 2 ) ) 
                {
                    CompModel.yPos = CompModel.yPos;
                    break;
                }
                bFirstTime = false;
            }
            
            if ( sortedTopPos[ i + 1 ] != undefined ) 
            {
                if ( ( sortedTopPos[ i + 1 ] - sortedTopPos[ i ] ) > 2 * elementHeight ) 
                {
                    CompModel.yPos = sortedTopPos[ i ] + elementHeight + 2;
                    break;
                }
            } 
            else 
            {
                CompModel.yPos = sortedTopPos[ i ] + elementHeight + 2;
            }
        }
    }  
    CompModel.yPos = parseInt( CompModel.yPos ); 
};

MG.NoteOperator.prototype.adjustPageBeforeNoteAdd = function( launcher, xPos, yPos ) 
{
    var isContainerScroll = false;
    var container = null;
    if ( EPubConfig.pageView != 'doublePage' ) 
    {
        yPos = parseInt( yPos );
        var stickyNotes = $( $( "#innerScrollContainer" )[ 0 ] ).find( '[type = stickyNoteComp]' )[ 0 ];
        var scrollby = $( launcher ).offset().top + $( "#bookContentContainer2" ).scrollTop() - 
        			   $( window ).height() + $( "#header" ).height() + $( stickyNotes ).height();
        
        var isScroll = $( launcher ).offset().top - $( window ).height();
        var isContainerScroll = false;
        if ( EPubConfig.pageView == 'singlePage' )
        {
            container = $( "#bookContentContainer" );
        }
        else
        {
            container = $( "#bookContentContainer2" );
        }
        // if stickynote is note in the visible screen area (i.e isScroll < 0), and scrollby value is greater than 0, the screen will shift upwards.
        if ( $( launcher ).offset().top < 0) 
        {
            var curScrollTopVal = $( "#bookContentContainer2" ).scrollTop();
            container.scrollTop( curScrollTopVal + $( launcher ).offset().top );
            isContainerScroll = true;
        } 
        else 
        {
            if ( scrollby > 0 && ( isScroll > 0 ) ) 
            {
            	container.scrollTop( scrollby );
                isContainerScroll = true;
            } 
            else 
            {
                isContainerScroll = false;
            }
        }
    }
    return isContainerScroll;
};

MG.NoteOperator.prototype.openCommentDeletionPanel = function ( selectionClass )
{
    var self = this;
    self._currentSelectionClass = selectionClass;
    PopupManager.removePopup();
    $( "#dltCommentConfirmPanel #dltConfirmationTxt" ).html( GlobalModel.localizationData[ "COMMENT_DELETE_CONFIRMATION" ] );
    PopupManager.addPopup( $( "#dltCommentConfirmPanel" ), null, {
        isModal : true,
        isTapLayoutDisabled : true,
        popupOverlayStyle : "popupMgrMaskStyleSemiTransparent",
        isCentered : true

    } );
};

MG.NoteOperator.prototype.addClassToAnnoTag = function ( annoTag )
{
	annoTag.removeClass()
	var annoArr = annoTag.attr( "annoArr" ).split( "," );
	var len = annoArr.length;
	var isFirstHighlightEncountered = false;
	var isFirstUnderlineHighlightEncountered = false;
	for( var index = len - 1; index >=0; index -- )
	{
		var annoItem = annoArr[ index ];
		var idColorArr = annoItem.split( ":" );
		
		if( idColorArr[ 1 ].indexOf('underline') != -1 )
		{
			if( isFirstUnderlineHighlightEncountered == false )
			{
				annoTag.addClass( idColorArr[ 0 ] );
				annoTag.addClass( idColorArr[ 1 ] );
			}
			else
			{
				annoTag.addClass( idColorArr[ 0 ] );
			}
			isFirstUnderlineHighlightEncountered = true;
		}
		else if( idColorArr[ 1 ].indexOf('selectioncolor') != -1 )
		{
			if( isFirstHighlightEncountered == false )
			{
				annoTag.addClass( idColorArr[ 0 ] );
				annoTag.addClass( idColorArr[ 1 ] );
			}
			else
			{
				annoTag.addClass( idColorArr[ 0 ] );
			}
			isFirstHighlightEncountered = true;
		}
		else
		{
			annoTag.addClass( idColorArr[ 0 ] );
		}
	}
};

MG.NoteOperator.prototype.updateAnnotationAttr = function ( selectionClassID , selectioncolor )
{
    var self = this;
    var _currentNoteSelectionArr = $( "." + selectionClassID );
    for ( var i = 0; i < _currentNoteSelectionArr.length ; i++ )
    {
    	var str = selectionClassID + ":" + selectioncolor;
    	var existingAnnoInfo = $( _currentNoteSelectionArr[i] ).attr( "annoArr" );
    	var existingAnnoArr = [];
    	if( existingAnnoInfo == undefined )
    	{
    		existingAnnoArr.push( str );
    	}
    	else
    	{
    		existingAnnoArr = existingAnnoInfo.split(",");
    		var alreadyExisting = false;
    		if( existingAnnoArr.indexOf( str ) == -1 )
    		{
    			for ( var j = 0; j < existingAnnoArr.length ; j++ )
    			{
    				//console.log("selectionClassID",selectionClassID)
    				if( existingAnnoArr[j].indexOf( selectionClassID ) != -1 )
    				{
    					//console.log("updating the exsiting")
    					existingAnnoArr[j] = str;	//Updated the existing class
    					alreadyExisting = true;
    					break;
    				}
    			}
    			if ( !alreadyExisting )
    			{
    				existingAnnoArr.push( str );
    			}
    		}
    	}
    	$( _currentNoteSelectionArr[i] ).attr( "annoArr" , existingAnnoArr.toString() );
    	self.addClassToAnnoTag( $( _currentNoteSelectionArr[i] ) );
    	//console.log("after------->",$( _currentNoteSelectionArr[i] ).attr( "annoArr" ))
    }
}

MG.NoteOperator.prototype.bindNotesEvent = function (objToBind, strPanelType, objComp) 
{
    var self = this;
    /*-----------------------------------------Left Panel annotation edit event ---------------------------------------------- */
    $(objToBind).find('.annotationData').unbind('click').bind('click', function (e) {        
        //Edit Sticky Note
        switch ($(this).parent().attr('type')) {
            case "highlightData":
				
                var currentListId = $(this).parent().attr('id').replace('highlightData_', '');
                $($("#bookContentContainer").data('scrollviewcomp')).trigger('closeOpenedPanel');
                self.editFromPanel = true;
                self.editNote("selection", currentListId, $(this).parent().attr('page'));

                break;

            case "stickynoteData":
                
                var currentListId = $(this).parent().attr('id').replace('stickynoteData_', '');
                self.editFromPanel = true;
                self.editNote("noteselection_", currentListId, $(this).parent().attr('page'));

                break;
            default:
        }
    });

    /*------------------------------------Left Panel annotation Expand/Collapse event ----------------------------------------- */
    $(objToBind).find('.expandCollapseNoteList').unbind('click').bind('click', function (e) {
        
        //Expand/collapse Sticky Note	
        if ($(this).hasClass('showDetail')) {
            var str = "";
            $(objToBind).find('.expandCollapseNoteList:not(.showDetail)').trigger("click");
            $(this).html(GlobalModel.localizationData["STICKY_NOTE_PANEL_HIDE_DETAILS_BUTTON"]);
            $(this).removeClass('showDetail');
            $(this).parent().find("#stickynoteDataTitle").hide();
            $(this).parent().find("#stickynoteDataTitle_detail").show();
            $(this).parent().find('.dataTitle').css({
                'height': 'auto',
                'overflow': 'auto',
                'white-space': 'normal'
            });

            var userRole = ServiceManager.getUserRole();
            if( userRole == AppConst.USERTYPE_TEACHER && GlobalModel.selectedStudentData )
            {
            	if(strPanelType != "studentnotes")
            	{
            		str = '<div class="annotaionEditDiv" ></div><div class="annotaionDeleteDiv"></div>';
            	}
            }
            else if( userRole == AppConst.USERTYPE_TEACHER )
            {
            	if(strPanelType == "studentnotes")
            	{
            		str = '<div class="annotaionEditDiv" ></div><div class="annotaionDeleteDiv"></div>';
            	}
            }
            else if( userRole == AppConst.USERTYPE_STUDENT )
            {
	            if(strPanelType != "studentnotes")
	            {
	                var id = $( this ).attr( "id" );
	                id = id.replace( "expandCollapseNoteList_", "" );
	                var noteObj = self.findNoteObjectInGlobalModel( id );
	                if( noteObj.shared_with )
	                {
	                    str = '<div class="annotaionDeleteDiv"></div>';
	                }
	            }
	            else
	            {
	            	str = '<div class="annotaionEditDiv" ></div><div class="annotaionDeleteDiv"></div>';
	            }
	        }

            $(this).parent().find("#editDeleteAnnotation").append('<div class="editDeleteAnnotaionPanelContainer">' + str +'</div>');

            $(objToBind).find('.annotaionEditDiv').unbind('click').bind('click', function (e) {
                //Edit Sticky Note
                self.isOpenNoteForEdit = true;
                self.editFromPanel = true;
                var type = $(this).parent().parent().parent().attr("type");
                if( type == "highlightData" || type == "stickynoteData" )
                {
                	$(this).parent().parent().parent().find('.annotationData').trigger("click", true);
                }
                else
                {
                	$(this).parent().parent().parent().find('.teacherAnnotationData').trigger("click", true);
                }
                
            });
            /*------------------------------------------------------------------------------------------------------------------------ */

            /*---------------------------------------- Left Panel annotation delete event -------------------------------------------- */
            $(objToBind).find('.annotaionDeleteDiv').unbind('click').bind('click', function (e) {
                //Delete Sticky Note
                switch ($(this).parent().parent().parent().attr('type')) {
                    case "highlightData":
                        var currentListId = $(this).parent().parent().parent().attr('id').replace('highlightData_', '');
                        self.deleteFromPanel = true;
                        self.openDeleteConfirmPanel("selection" + currentListId);
                        //$( objComp.element ).trigger( 'deleteHighlightData' , currentListId );
                        break;
                    case "stickynoteData":
                        var currentListId = $(this).parent().parent().parent().attr('id').replace('stickynoteData_', '');
                        self.deleteFromPanel = true;
                        self.openDeleteConfirmPanel("noteselection_" + currentListId);
                        break;
                    case "stickynoteComment":
                        var currentListId = $(this).parent().parent().parent().attr('id').replace('stickynoteComment_', '');
                        self.deleteFromPanel = true;
                        self.openCommentDeletionPanel( "tseselect" + currentListId );
                        break;
                    case "stickynoteCommentHighlight":
                    	var currentListId = $(this).parent().parent().parent().attr('id').replace('stickynoteComment_', '');
                        self.deleteFromPanel = true;
                        self.openCommentDeletionPanel( "selection" + currentListId );
                    	break;
                    default:
                }
            });
            /*------------------------------------------------------------------------------------------------------------------------ */
        }
        else {
            
            $(this).parent().find("#stickynoteDataTitle").show();
            $(this).parent().find("#stickynoteDataTitle_detail").hide();
            $(this).parent().find('.editDeleteAnnotaionPanelContainer').remove();
            $(this).html(GlobalModel.localizationData["STICKY_NOTE_PANEL_SHOW_DETAILS_BUTTON"]);
            $(this).addClass('showDetail');
            $(this).parent().find('[class=dataTitle]').css({
                'height': '18px',
                'overflow': 'hidden',
                'white-space': 'nowrap'
            });
        }
       
        
        $(objComp.element).data('annotationPanelComp').updateViewSize(); 
    });
    /*------------------------------------------------------------------------------------------------------------------------ */
};