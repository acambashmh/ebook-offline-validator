/**
 * @author anand.kumar
 */
var PopupManager = {
    arrManualPopup: [],
    compViewList: [],
    lastCompView: null,
    objMaskDiv: null,
    objArrowPointer: null,
    _nMaskDivTopPos: 0,
    isOrientationChangeEventAdded: false,
    isPopupShowing: false,
    isTapLayoutEnabled: true,
    isControlToBeDisabled: null,
    objPopupViewComp: null, //param objPanelViewComp in addPopup
    objLauncherHotspot: null, //param objLauncherComp in addPopup
    objOptionsData: null, //param options in addPopup
    iPopupZIndex: 0,
    /**
     * This function is responsible to add/show popup
     * @param PopupPanel
     * @param hotspot/button which launches popup (i.e. calls addPopup function of PopupManager)
     * @param options
     * 			isModal 				true/false	: 	to show transparent tapable overlay behind popup (compulsory)
     *  		isCentered 				true/false	: 	if true popup appears in center of the screen otherwise it will appear somewhere relative to hotspot/button (param2)
     *  		hasPointer				true/false	: 	show pointer with popup
     *  		offsetScale			 	number	 	:	scale percentage of the launcher hotspot
     *  		callbackFunc			Function	: 	call back
     *  		caller					object whose callback function is passed as param
     *  		isTapLayoutDisabled		true/false	: 	close popup on click on tap layout
     *  		popupOverlayStyle		stylename 	:	optional style with less style
     *  		isManuallyRemoveable 	true/false	: 	if popup needs to be closed only on user click (e.g close button of popup)
     *  		isToShowBelowTapLayout 	true/false	: 	if popup needs to be closed only on user click (e.g close button of popup)
     *  		isFloatable				true/false	: 	if popup needs to be placed permanently at any position
     *  		isToCallbackOnTapOutside	true/false: call back on tap outside
     */
    addPopup: function(objPanelViewComp, objLauncherComp, options){
       HotkeyManager.setPopupFocus(objPanelViewComp);
	   //$("#bookContentContainer2").css({"-webkit-overflow-scrolling":"auto"})
	    var self = this;
	    
        //$($.find('[data-role="zoomwrapper"]')).removeClass("ui-header-fixed");
        this.objPopupViewComp = objPanelViewComp;
        this.objLauncherHotspot = objLauncherComp;
        this.objOptionsData = options;
        if (this.isOrientationChangeEventAdded == false) {
            $(window).bind("orientationchange", function(event){
                //self.callCallbackFunction();
                event.preventDefault();
                self.orientationChangeHandler();
                
            });
            
            $(window).bind("resize", function(event){
                if (self.isPopupShowing) {
                    if ($(self.objPopupViewComp).attr("isMinimize")) {
                        if ($(self.objPopupViewComp).attr("isMinimize") == "false") {
                            self.positionPopup(true);
                        }
                    }
                    else {
                        self.positionPopup(true);
                    }
                    
                }
                
            });
            
            this.isOrientationChangeEventAdded = true;
        }
        var isModal = options.isModal;
        this.isControlToBeDisabled = options.isControlToBeDisabled;
        if (this.isControlToBeDisabled) {
            var controlsTobeDisabled = $("#controlsTobeDisabled");
            var itemsToBeDisabledLength = controlsTobeDisabled.children().length;
            for (var i = 0; i < itemsToBeDisabledLength; i++) {
                var itemID = $(controlsTobeDisabled.children()[i]).html().trim();
                $("#" + itemID).addClass('ui-disabled');
            }
        }
        
        this.isTapLayoutEnabled = true;
        if (options.isTapLayoutDisabled) {
            this.isTapLayoutEnabled = false;
        }
        //objCompView = document.getElementById(objCompViewId);
        //add mask div at runtime and then the comp view...
        if (this.objMaskDiv == null) {
            this.objMaskDiv = document.createElement('div');
            this.objMaskDiv.setAttribute('id', 'popupMaskDiv');
            this.objMaskDiv.setAttribute('class', 'popupMgrMaskStyle');
            $(this.objMaskDiv).css('z-index', AppConst.ZI_POPUP_MGR_MASK);
			$(this.objMaskDiv).css('user-select', 'none');
			$(this.objMaskDiv).css('-webkit-user-select', 'none');
			$(this.objMaskDiv).css('-moz-user-select', 'none');
			$(this.objMaskDiv).bind("touchmove", function(e){
			    e.preventDefault();
			})
            document.getElementById('pg').appendChild(this.objMaskDiv);
        }
        
        if (options.popupOverlayStyle != undefined) {
            $(this.objMaskDiv).attr('class', options.popupOverlayStyle);
        }
        else {
            $(this.objMaskDiv).attr('class', "popupMgrMaskStyle");
        }
        
	    if (navigator.userAgent.match(/(android)/i)) {
	    	  $(this.objMaskDiv).css('opacity','0.1');
	    }
        
        
        $(this.objMaskDiv).unbind('click').bind('click', function(event){
            if (self.isTapLayoutEnabled == true) {
				self.removePopup();
            }
			//console.log
			if(self.objOptionsData.callbackFunc && self.objOptionsData.isToCallbackOnTapOutside)
			{
				self.objOptionsData.caller[self.objOptionsData.callbackFunc]();
			}
        });
        
        if (this.objArrowPointer == null) {
            this.objArrowPointer = document.createElement('div');
            this.objArrowPointer.setAttribute('id', 'popupArrowDiv');
            //this.objArrowPointer.setAttribute('class', 'popupMgrMaskStyle');
            $(this.objArrowPointer).css('z-index', AppConst.ZI_POPUP_ARROW_POINTER);
            document.getElementById('pg').appendChild(this.objArrowPointer);
        }
        
        // if Modal...
        if (isModal) {
            //...
            // to-do...
            $(this.objMaskDiv).css('display', 'block');
        }
        // else...
        else {
            //...
        
        }
        
        var iZIndex;// = AppConst.ZI_PANEL_VIEW_COMP + self.iPopupZIndex;
        var isManuallyRemovable = false;
        
        if (this.objOptionsData) {
            if (this.objOptionsData.isManuallyRemoveable) {
                iZIndex = AppConst.ZI_PANEL_VIEW_COMP_MANUALLY_REMOVABLE;
                isManuallyRemovable = true;
            }
            if (this.objOptionsData.isToShowBelowTapLayout != undefined) {
                if (this.objOptionsData.isToShowBelowTapLayout == true) {
                    iZIndex = AppConst.ZI_PANEL_VIEW_COMP_MANUALLY_REMOVABLE + 1;
                    isManuallyRemovable = true;
                }
                else {
                    isManuallyRemovable = false;
                }
            }
            
        }
        if (isManuallyRemovable == true) {
            this.arrManualPopup.push(objPanelViewComp);
        }
        if (isManuallyRemovable == false) {
            iZIndex = AppConst.ZI_PANEL_VIEW_COMP + self.iPopupZIndex;
            self.iPopupZIndex += 1;
        }
        $(objPanelViewComp).css('z-index', iZIndex);
        
        // store the view
        this.lastCompView = objPanelViewComp;
        this.compViewList[objPanelViewComp.attr('id')] = objPanelViewComp;
        $(objPanelViewComp).trigger('popupOpened');
        this.isPopupShowing = true;
        this.positionPopup();
        $(objPanelViewComp).fadeIn(600);
        $(objPanelViewComp).css('display', 'block');
        
        //$("#dummyHeader").css("display", "none");
    },
    
    positionPopup: function(isToCalculateOffsetScale){
    	if($("#annotation-bar").css("display") == "block")
    		return;
        var nLeftPos;
        var nTopPos;
        var objPanelViewComp = this.objPopupViewComp;
        var objLauncherComp = this.objLauncherHotspot;
        var options = this.objOptionsData;
		if(options.isFloatable == undefined)
		{
			options.isFloatable = true;
		}
		
        if (options.isFloatable) {
			$(this.objArrowPointer).css("display", 'none');
			
			var nMaskDivTopPos;
			if ($(this.objMaskDiv).css('display') == "block") {
				nMaskDivTopPos = $(this.objMaskDiv).offset().top;
				this._nMaskDivTopPos = nMaskDivTopPos
			}
			else {
				nMaskDivTopPos = this._nMaskDivTopPos;
			}
			
			var nPopupHeight = $(objPanelViewComp).height() + nMaskDivTopPos;
			var nPopupWidth = $(objPanelViewComp).width();
			
			if (options.isCentered) {
				nLeftPos = (window.innerWidth - nPopupWidth) / 2;
				nTopPos = ((window.innerHeight - nPopupHeight) / 2) - (this._nMaskDivTopPos / 2);
			}
			else {
				var nArrowHCenterPos = 8;
				var nArrowVCenterPos = 8;
				var nPercentScale = 1;
				if (options.offsetScale) {
					nPercentScale = options.offsetScale;//$($.find('[data-role="zoomwrapper"]')).data('zoomwrapper').member._nPercentScale;
					if (isToCalculateOffsetScale == true) {
						nPercentScale = GlobalModel.currentScalePercent;
					}
				}
				if (ismobile && (EPubConfig.pageView != 'singlePage' && EPubConfig.pageView != 'doublePage')) {
					var nBookContentContainerWidth = $("#bookContentContainer2").width() - $("#annotationPanel").width();
					nPercentScale = nBookContentContainerWidth / EPubConfig.pageWidth;
				}
				
				var nHotspotWidth = 0;
				var nHotspotHeight = 0;
				var nHotspotLeftPos = 0;
				var nHotSpotTopPos = 0;
				var nHotSpotRightPos = 0;
				var nHotSpotBottomPos = 0;
				if (objLauncherComp != null) {
					if (objLauncherComp.element) {
						nHotspotWidth = $(objLauncherComp.element).width() * nPercentScale;
						nHotspotHeight = $(objLauncherComp.element).height() * nPercentScale;
						nHotspotLeftPos = $(objLauncherComp.element).offset().left;
						nHotSpotTopPos = $(objLauncherComp.element).offset().top;
					}
					else {
						nHotspotWidth = $(objLauncherComp).width() * nPercentScale;
						nHotspotHeight = $(objLauncherComp).height() * nPercentScale;
						nHotspotLeftPos = $(objLauncherComp).offset().left;
						nHotSpotTopPos = $(objLauncherComp).offset().top;
					}
					nHotSpotRightPos = nHotspotLeftPos + nHotspotWidth;
					nHotSpotBottomPos = nHotSpotTopPos + nHotspotHeight;
				}
				
				var nPanelParentLeftPos = $(objPanelViewComp).parent().offset().left;
				var nPanelParentTopPos = $(objPanelViewComp).height();
				var nMaskDivLeftPos = $(this.objMaskDiv).offset().left;
				
				
				var nMaskDivHeight = $(window).height();//$(this.objMaskDiv).height();
				var nMaskDivWidth = $(this.objMaskDiv).width();
				
				nHotspotLeftPos = nHotspotLeftPos - nPanelParentLeftPos;
				
				var adjustpopuptoLeft = 0;
				if( ( $(objPanelViewComp).attr("id") == "annotation-bar" || $(objPanelViewComp).attr("id") == "stickyNoteContainer" ) && EPubConfig.ReaderType == '6TO12')
				{
					adjustpopuptoLeft = 30;
				}
				
				var nArrowGap = 0;
				var nPopupPaddingLeft = 10;
				if (options.hasPointer) {
					nArrowGap = 13;
					nPopupHeight += nArrowGap;
					if ((nHotspotHeight / 2) > (nPopupHeight / 2) || (nHotspotWidth / 2) > (nPopupWidth / 2)) {
						//this.objOptionsData
						this.objOptionsData.hasPointer = false;
						this.objOptionsData.isCentered = true;
						$(this.objArrowPointer).css("display", 'none');
						this.positionPopup(isToCalculateOffsetScale);
						return;
					}
				}
				if (options.positionAt) {
					if (options.positionAt == 'top') {
						nTopPos = nHotSpotTopPos - (nPopupHeight);
						
						if ((nHotspotLeftPos + nPopupWidth) > (nMaskDivWidth + nMaskDivLeftPos)) {
							nLeftPos = nHotspotLeftPos - ((nHotspotLeftPos + nPopupWidth) - (nMaskDivWidth + nMaskDivLeftPos - nPopupPaddingLeft));
						}
						else {
							nLeftPos = nHotspotLeftPos - nPopupPaddingLeft;
						}
						if (options.hasPointer) {
							$(this.objArrowPointer).attr("class", 'pointerBottom');
							$(this.objArrowPointer).css("display", 'block');
							$(this.objArrowPointer).css("position", 'absolute');
							$(this.objArrowPointer).css('left', (nHotspotLeftPos + ((nHotspotWidth / 2) - nArrowHCenterPos))).css('top', nHotSpotTopPos - (nArrowGap + nMaskDivTopPos));
						}
						if( this.objPopupViewComp[0].id == "tagsCantBeSavedWithSharedAnnotationPopup" )
						{
							$(this.objArrowPointer).addClass('pointerTagBottom');
						}
					}
					else 
						if (options.positionAt == 'bottom') {
							nTopPos = (nHotSpotTopPos + nHotspotHeight + nArrowGap) - nMaskDivTopPos;
							//nLeftPos = nHotspotLeftPos - nPanelParentLeftPos;
							//console.log('else: '+nHotSpotTopPos, nHotspotHeight);
							if ((nHotspotLeftPos + nPopupWidth) > (nMaskDivWidth + nMaskDivLeftPos)) {
								nLeftPos = nHotspotLeftPos - ((nHotspotLeftPos + nPopupWidth) - (nMaskDivWidth + nMaskDivLeftPos - nPopupPaddingLeft));
							}
							else {
								nLeftPos = nHotspotLeftPos - nPopupPaddingLeft;
							}
							if (options.hasPointer) {
								$(this.objArrowPointer).attr("class", 'pointerTop');
								$(this.objArrowPointer).css("display", 'block');
								$(this.objArrowPointer).css("position", 'absolute');
								
								$(this.objArrowPointer).css('left', (nHotspotLeftPos + ((nHotspotWidth / 2) - nArrowHCenterPos))).css('top', ((nHotSpotTopPos + nHotspotHeight) - nMaskDivTopPos));
							}
							if( this.objPopupViewComp[0].id == "tagsCantBeSavedWithSharedAnnotationPopup" )
							{
								$(this.objArrowPointer).addClass('pointerTagTop');
							}
						}
						else 
							if (options.positionAt == 'left') {
							
							}
							else 
								if (options.positionAt == 'right') {
								
								}
				}
				else {
					if (nHotSpotTopPos - nPopupHeight > 0) //FOR POSITIONING POPUP AT TOP
					{
						nTopPos = nHotSpotTopPos - (nPopupHeight);
						
						if ((nHotspotLeftPos + nPopupWidth) > (nMaskDivWidth + nMaskDivLeftPos)) {
							nLeftPos = nHotspotLeftPos - ((nHotspotLeftPos + nPopupWidth) - (nMaskDivWidth + nMaskDivLeftPos - nPopupPaddingLeft))  - adjustpopuptoLeft - adjustpopuptoLeft;
						}
						else {
							nLeftPos = nHotspotLeftPos - nPopupPaddingLeft - adjustpopuptoLeft;
						}
						if (options.hasPointer) {
							$(this.objArrowPointer).attr("class", 'pointerBottom');
							$(this.objArrowPointer).css("display", 'block');
							$(this.objArrowPointer).css("position", 'absolute');
							if (ismobile && EPubConfig.pageView == 'doublePage') {
							$(this.objArrowPointer).css('left', (nHotspotLeftPos + ((nHotspotWidth/2) - nArrowHCenterPos-5))).css('top', nHotSpotTopPos - (nArrowGap + nMaskDivTopPos));
							} else {
							$(this.objArrowPointer).css('left', (nHotspotLeftPos + ((nHotspotWidth / 2) - nArrowHCenterPos))).css('top', nHotSpotTopPos - (nArrowGap + nMaskDivTopPos));
							}
						}
						if( this.objPopupViewComp[0].id == "tagsCantBeSavedWithSharedAnnotationPopup" )
						{
							$(this.objArrowPointer).addClass('pointerTagBottom');
						}
					}
					else if ((nHotSpotTopPos + nHotspotHeight + nPopupHeight + $(this.objArrowPointer).height()) < (nMaskDivTopPos + nMaskDivHeight)) //FOR POSITIONING POPUP AT BOTTOM
						{
							nTopPos = (nHotSpotTopPos + nHotspotHeight + nArrowGap) - nMaskDivTopPos;
							//nLeftPos = nHotspotLeftPos - nPanelParentLeftPos;
							//console.log('else: '+nHotSpotTopPos, nHotspotHeight);
							if ((nHotspotLeftPos + nPopupWidth) > (nMaskDivWidth + nMaskDivLeftPos)) {
								nLeftPos = nHotspotLeftPos - ((nHotspotLeftPos + nPopupWidth) - (nMaskDivWidth + nMaskDivLeftPos - nPopupPaddingLeft)) - adjustpopuptoLeft;
							}
							else {
								nLeftPos = nHotspotLeftPos - nPopupPaddingLeft - adjustpopuptoLeft;
							}
							if (options.hasPointer) {
								$(this.objArrowPointer).attr("class", 'pointerTop');
								$(this.objArrowPointer).css("display", 'block');
								$(this.objArrowPointer).css("position", 'absolute');
								
								$(this.objArrowPointer).css('left', (nHotspotLeftPos + ((nHotspotWidth / 2) - nArrowHCenterPos))).css('top', ((nHotSpotTopPos + nHotspotHeight) - nMaskDivTopPos));
							}
							if( this.objPopupViewComp[0].id == "tagsCantBeSavedWithSharedAnnotationPopup" )
							{
								$(this.objArrowPointer).addClass('pointerTagTop');
							}
						}
						else //FOR POSITIONING POPUP AT SIDES BASED ON AVAILABLE SPACE
						{
							nTopPos = (nHotSpotTopPos) - (nPopupHeight / 2);//0;
							if ((nHotspotLeftPos + nPopupWidth + nHotspotWidth) > (nMaskDivWidth + nMaskDivLeftPos)) {
								nLeftPos = nHotspotLeftPos - (nPopupWidth + nArrowGap);
								if (options.hasPointer) {
									$(this.objArrowPointer).attr("class", 'pointerRight');
									$(this.objArrowPointer).css("display", 'block');
									$(this.objArrowPointer).css('left', nHotspotLeftPos - nArrowGap).css('top', ((nHotSpotTopPos + ((nHotspotHeight / 2) - nArrowVCenterPos)) - nMaskDivTopPos));
								}
								if( this.objPopupViewComp[0].id == "tagsCantBeSavedWithSharedAnnotationPopup" )
								{
									$(this.objArrowPointer).addClass('pointerTagRight');
								}
							}
							else {
								nLeftPos = nHotspotLeftPos + nHotspotWidth + nArrowGap;
								if (options.hasPointer) {
									$(this.objArrowPointer).attr("class", 'pointerLeft');
									$(this.objArrowPointer).css("display", 'block');
									$(this.objArrowPointer).css('left', nHotspotLeftPos + nHotspotWidth).css('top', ((nHotSpotTopPos + ((nHotspotHeight / 2) - nArrowVCenterPos)) - nMaskDivTopPos));
								}
								if( this.objPopupViewComp[0].id == "tagsCantBeSavedWithSharedAnnotationPopup" )
								{
									$(this.objArrowPointer).addClass('pointerTagLeft');
								}
							}
							
							//Adjusting the pop up position based on window height
							if((nPopupHeight / 2) + parseInt($(this.objArrowPointer).css('top'))  > $(window).height())
							{
								var movetop = (nPopupHeight / 2) + parseInt($(this.objArrowPointer).css('top'))  - $(window).height();
								var newTopPos = nTopPos - movetop + 20;
							}
							if(newTopPos != undefined)
							{
								nTopPos = $(window).height() - nPopupHeight + 10;//nHotSpotTopPos + nHotspotHeight + 10 - nPopupHeight;
							}
							else if( nTopPos < 0 )
							{
								nTopPos = 0;
							}
						}
				}
				
			}
			
			$(objPanelViewComp).css('left', nLeftPos).css('top', nTopPos);
		}
        //console.log('height: '+$(objPanelViewComp).height())
    
    },
    
    positionArrow: function(side){
        var nArrowVCenterPos = 8;
        var nMaskDivTopPos = $(this.objMaskDiv).offset().top;
        var nPercentScale = this.objOptionsData.offsetScale;
        var objLauncherComp = this.objLauncherHotspot;
        var nHotspotLeftPos = $(objLauncherComp.element).offset().left;
        var nHotSpotTopPos = $(objLauncherComp.element).offset().top;
        nHotspotHeight = $(objLauncherComp.element).height() * nPercentScale;
        var nArrowGap = 14;
        $(this.objArrowPointer).attr("class", 'pointerRight');
        $(this.objArrowPointer).css("display", 'block');
        $(this.objArrowPointer).css('left', nHotspotLeftPos - nArrowGap).css('top', ((nHotSpotTopPos + ((nHotspotHeight / 2) - nArrowVCenterPos)) - nMaskDivTopPos));
        if( this.objPopupViewComp[0].id == "tagsCantBeSavedWithSharedAnnotationPopup" )
		{
			$(this.objArrowPointer).addClass('pointerTagRight');
		}
    },
    
    removePopup: function(objCompViewId){
    	//console.log('popupclosed: ',arguments.callee.caller.toString());
    	//$("#bookContentContainer2").css({"-webkit-overflow-scrolling":"touch"})
        var isToRemove = true;
        if (this.isControlToBeDisabled) {
            var controlsTobeDisabled = $("#controlsTobeDisabled");
            var itemsToBeDisabledLength = controlsTobeDisabled.children().length;
            for (var i = 0; i < itemsToBeDisabledLength; i++) {
                var itemID = $(controlsTobeDisabled.children()[i]).html().trim();
                $("#" + itemID).removeClass('ui-disabled');
            }
        }
        if (this.objOptionsData) {
            if (this.objOptionsData.isManuallyRemoveable) {
                isToRemove = false;
            }
        }
        
        var objCompView;
        if (objCompViewId || objCompViewId != null) {
            objCompView = this.compViewList[objCompViewId];
        }
        else {
            objCompView = this.lastCompView;
        }
        $(objCompView).trigger('popupClosed');
        if (isToRemove) {
            $(this.objMaskDiv).css('display', 'none');
            $(this.objArrowPointer).css('display', 'none');
            if(this.objOptionsData)
            	this.objOptionsData.hasPointer = false;
            $(objCompView).css('display', 'none');
            
            this.isPopupShowing = false;
            //$($.find('[data-role="zoomwrapper"]')).addClass("ui-header-fixed");
            //$("#dummyHeader").addClass("ui-header-fixed");
            $("#dummyHeader").css("display", "block");
            if (self.iPopupZIndex > 0) {
                self.iPopupZIndex -= 1;
            }
        }
		$(this.objArrowPointer).addClass('pointerTagTop');
		$(this.objArrowPointer).addClass('pointerTagBottom');
		$(this.objArrowPointer).addClass('pointerTagRight');
		$(this.objArrowPointer).addClass('pointerTagLeft');
        HotkeyManager.nullifyActiveElements()
        
    },
    
    closePopup: function(){
        if (this.objOptionsData) {
            if (this.objOptionsData.isModal == false) {
                this.removePopup();
            }
        }
        
    },
    
    orientationChangeHandler: function(){
    	var self = this;
        if (this.objMaskDiv != null) {
            if ($(this.objMaskDiv).attr('class') == "popupMgrMaskStyle" || $(this.objMaskDiv).attr('class') == "popupMgrMaskStyleVideoPanel") {
                //this.removePopup();
                setTimeout(function() {
                	self.positionPopup(true);
                },500)
                
            }
            else {
                //this.positionPopup(true);
                if ($(this.objPopupViewComp).attr("isMinimize")) {
                    if ($(this.objPopupViewComp).attr("isMinimize") == "false") {
                        self.positionPopup(true);
                    }
                }
                else {
                    self.positionPopup(true);
                }
            }
            
        }
    },
    
    removePopupManually: function(objCompViewId){
    	$(this.objMaskDiv).css('display', 'none');
        $(this.objArrowPointer).css('display', 'none');
        var objCompView;
        if (objCompViewId || objCompViewId != null) {
            objCompView = this.compViewList[objCompViewId];
        }
        else {
            objCompView = this.lastCompView;
        }
        
        $(objCompView).css('display', 'none');
        //$(objCompView).trigger('popupClosed');
        this.isPopupShowing = false;
        //$($.find('[data-role="zoomwrapper"]')).addClass("ui-header-fixed");
        //$("#dummyHeader").addClass("ui-header-fixed");
        //$("#dummyHeader").css("display", "block");
        
        for (var i = 0; i < this.arrManualPopup.length; i++) {
            if (this.arrManualPopup[i] == this) {
                this.arrManualPopup.splice(i, 1);
                break;
            }
        }
		HotkeyManager.nullifyActiveElements()
    },
    
    removeAllManualPopup: function(){
        var arrPopup = this.arrManualPopup;
		
        for (var i in arrPopup) {
			try{
				var id = arrPopup[i].attr("id");
				this.removePopupManually(this.arrManualPopup[i].attr("id"));
			}
			catch (e){
				//console.error("Error description: " + e.message);
			}
        }
    },
	
	removePopupmanagerOverlay: function()
	{
		$(this.objMaskDiv).css('display', 'none');
        
	}
}
