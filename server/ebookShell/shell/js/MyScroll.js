/**
 * @author sachin.manchanda
 * This function handles the functionality of clicking on scrollbar in iscroll
 */
function iScrollClickHandler(element,scrollObj,scrollParent) {
	$(element).css("pointer-events","");
	$(element).unbind("click").bind("click",function(e) {
	  var percent = parseInt($("#"+scrollParent).height())/parseInt($(element).find("div").height());
	  var ypos;
	  if(e.offsetX==undefined) // this works for Firefox
	    ypos = e.pageY-$(element).offset().top;
	  else                     // works in Google Chrome/safari
	    ypos = e.offsetY;
	  //console.log(parseInt(ypos),parseInt(Math.abs(scrollObj.y)/percent),parseInt($(element).find("div").height()),parseInt($(element).height()));
	  if(parseInt(Math.abs(scrollObj.y)/percent)+ parseInt($(element).find("div").height()) < parseInt(ypos)-2)
	  {
	  	scrollObj.scrollTo(0,15,0, true);
	  }
	  else if(parseInt(Math.abs(scrollObj.y)/percent) > parseInt(ypos)-2)
	  {
	  	if(parseInt(Math.abs(scrollObj.y)/percent) != 0)
	  		scrollObj.scrollTo(0,-15,0, true);
	  }
	});
	var preYPos = null;
	
	if($(element).find("div")[0] != undefined)
	{
		$($(element).find("div")[0]).unbind("drag").bind("drag",function(event) {
			$(window).unbind("mouseup").bind("mouseup",function(event) {
				preYPos = null;
			});
			var percent = Math.floor(Number($("#"+scrollParent).height())/Number($(element).find("div").height()+2));
			var ypos = event.offsetY;
			var deltaY;
			if(ypos)
			{
			 	  if(preYPos == null)
			 	  {
			 	  		preYPos = ypos;
			 	  }
			 	  else
			 	  {
	 	  		  		deltaY = ypos - preYPos;
	 	  		  		if(scrollObj.y>1)
	 	  		  			deltaY = 0;
	 	  		  		if(scrollObj.y<scrollObj.maxScrollY-1)
	 	  		  			deltaY = 0;
 	  		  			scrollObj.scrollTo(0,(deltaY*percent),0, true);
	 	  	  			preYPos = ypos;
			 	  }
	        }      	    
	        });
	    $($(element).find("div")[0]).css("pointer-events","");
	}
}

function removeiScrollClickHandler(element)
{
	if(element == null)
		return;
	
	$(element).unbind("click");
	
	if($(element).find("div")[0] != undefined)
		$($(element).find("div")[0]).unbind("drag")
}
