/**
 * Created by DugganJ on 14/10/2015.
 */
var fs = require('fs');
var q = require('q');
var exec = require('child_process').exec;
var path = require('path');
var untarFolderPath = path.join(__dirname, './untar');

function listPackageDetails(folderPath){
    var mappingManifestPath = path.resolve(path.join(folderPath, 'mapping_manifest.json'));
    var manifest = JSON.parse(fs.readFileSync(mappingManifestPath, 'utf-8'));
    var sections = [];
    manifest.forEach(function (file) {
        var filePath = path.resolve(path.join(folderPath, file.path));
        var sectionManifest = JSON.parse(fs.readFileSync(filePath, 'utf-8'));

        sectionManifest.download_packages.downloads.forEach(function (download, index) {
            if(download.mime_type == "application/x-tar" && index != 0){
                var section = {
                    "description": download.description,
                    "package_size": download.package_size,
                    "tarFileName": path.basename(download.package_details.download_uri.relative_url),
                    "loaded": false,
                    "failed": false
                };
                sections.push(section);
            }
        });
    });
    return sections
}

function removeUntarContent(){
    var defer = q.defer();
    var untarFolderExists = fs.existsSync(untarFolderPath);
    if (!untarFolderExists) {
        fs.mkdirSync(untarFolderPath);
        return true;
    } else {
        exec('RMDIR ' + untarFolderPath + '  /S /Q', function (err, stdout, stderr) {
            if (err) {
                return defer.reject(err);
            }
            defer.resolve(true);
        });
    }

    return defer.promise;
}

module.exports.listPackageDetails = listPackageDetails;
module.exports.removeUntarContent = removeUntarContent;
