/**
 * Created by DugganJ on 14/10/2015.
 */
var fs = require('fs');
var tarball = require('tarball-extract');
var q = require('q');
var path = require('path');
var untarFolderPath = path.join(__dirname, './untar');

function changeConfigSync() {
    try{
        var configPath = path.resolve(path.join(untarFolderPath, '/Ops/config.txt'));
        var config = JSON.parse(fs.readFileSync(configPath, 'utf-8'));
        config.Settings.isDemo = true;
        config.Settings.readerPath = '/shell/';
        fs.writeFileSync(path.join(untarFolderPath, '/Ops/config.txt'), JSON.stringify(config));
    }catch(e){
        throw e;
    }

};

function untar(path) {
    var defer = q.defer();
    if(!fs.existsSync(path)){
        throw "File does not exist: " + path;
    }

    tarball.extractTarball(path, untarFolderPath, function (err) {
        if (err) {
            return defer.reject(err);
        }
        defer.resolve(true);
    });
    return defer.promise;
};

module.exports.changeConfigSync = changeConfigSync;
module.exports.untar = untar;