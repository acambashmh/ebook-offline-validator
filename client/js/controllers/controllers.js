/**
 * Created by DugganJ on 13/10/2015.
 */
var controllers = angular.module('ValidatorApp.Controllers', []);

controllers.controller('mainCtrl', function ($scope, packageContentService, CONFIG) {
    var self = this;
    self.loadEbook = false;
    self.data = null;
    self.loading = false;
    self.ebookPath = CONFIG.ebookPath;
    self.folderPath = CONFIG.folderPath;
    self.sections = [];
    self.errors = [];
    self.selectedTab = 'SECTIONS';

    self.selectTab = function(tab){
        self.selectedTab = tab;
    };

    self.listPackageDetails = function () {
        startProcessing();
        self.sections = [];

        packageContentService.removeUntarContent().then(function(response){
            packageContentService.loadMasterPackageContent(self.folderPath).then(function(){
                packageContentService.listPackageDetails(self.folderPath).then(function(response){
                    self.sections = response.data;
                    self.loading = false;
                }).catch(function(e){
                    handleError(e);
                    self.loading = false;
                });
            }).catch(function(e){
                handleError(e);
                self.loading = false;
            });
        }).catch(function(e){
            handleError(e);
            self.loading = false;
        });
    };

    function handleError(e){
        console.log(e.data);
        self.errors.push(e.data);
    };

    function startProcessing(){
        self.loading = true;
        self.loadEbook = false;
        self.errors = [];
    };

    self.loadTarContent = function (i) {
        startProcessing();

        packageContentService.loadPackageContent(self.folderPath, self.sections[i].tarFileName).then(function(){
            self.loadEbook = true;
            self.sections[i].loaded = true;
            self.sections[i].failed = false;
        }).catch(function(e){
            handleError(e);
            self.loading = false;
            self.sections[i].failed = true;
            self.loadEbook = false;
        }).finally(function(){
            self.loading = false;
        });
    };

    self.loadAllTarContent = function () {
        startProcessing();

        //reset all content to unloaded and with no error
        angular.forEach(self.sections, function(section) {
            section.loaded = false;
            section.failed = false;
        });

        i = 0;

        var success = function(){
            self.sections[i].loaded = true;
            self.sections[i].failed = false;
            loadNextTar();
        }

        var failure = function(e){
            handleError(e);
            self.sections[i].failed = true;
            loadNextTar();
        }

        var loadNextTar = function(){
            i++;
            if(self.sections[i]){
                packageContentService.loadPackageContent(self.folderPath, self.sections[i].tarFileName).then(success).catch(failure);
            }else{
                self.loadEbook = true;
                self.loading = false;
            }
        };

        packageContentService.loadPackageContent(self.folderPath, self.sections[i].tarFileName).then(success).catch(failure);
    };
});