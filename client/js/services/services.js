/**
 * Created by DugganJ on 13/10/2015.
 */
var services = angular.module('ValidatorApp.Services', []);

services.service('packageContentService', function($http, CONFIG) {

    this.loadPackageContent = function(folderPath, fileName){
        return $http.get(CONFIG.ebookPath +'/loadPackageContent?folderPath='+folderPath+'&fileName='+fileName);
    };

    this.loadMasterPackageContent = function(folderPath){
        return this.loadPackageContent(folderPath, "MASTER.TAR");
    };

    this.listPackageDetails = function(folderPath){
        return $http.get(CONFIG.ebookPath +'/listPackageDetails?folderPath='+folderPath);
    };

    this.removeUntarContent = function(folderPath){
        return $http.get(CONFIG.ebookPath +'/removeUntarContent');
    };
});