var gulp = require('gulp');
var reworkNpm = require('rework-npm');
var fs = require('fs');
var path = require('path');
var rework = require('gulp-rework');
var rename = require('gulp-rename');
var electron = require('gulp-electron');

gulp.task('css', function () {
    var entry = path.join(__dirname, './client/css/build.css');
    var outFile = path.join(__dirname, './client/css/');

    return gulp.src(entry)
        .pipe(rework(reworkNpm(), { sourcemap: false }))
        .pipe(rename('compiled.css'))
        .pipe(gulp.dest(outFile));
});


